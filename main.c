#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "app_timer.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp_btn_ble.h"
#include "nrf_pwr_mgmt.h"
#include "temperature.h"
#include "spo2.h"
#include "accel.h"
#include "battery.h"



//#include "nrf_dfu_ble_svci_bond_sharing.h"
//#include "nrf_svci_async_function.h
//#include "nrf_svci_async_handler.h"
//#include "app_error.h"
//#include "ble.h"
//#include "ble_srv_common.h"
//#include "peer_manager.h"
//#include "peer_manager_handler.h"
//#include "ble_conn_state.h"
//#include "ble_dfu.h"
//#include "fds.h"
//#include "nrf_drv_clock.h"
//#include "nrf_power.h"
//#include "nrf_log.h"
//#include "nrf_log_ctrl.h"
//#include "nrf_log_default_backends.h"
//#include "nrf_bootloader_info.h"

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_drv_clock.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_power.h"

#include "app_error.h"
#include "app_util.h"

#include "state_machine.h"
#include "rtc_functions.h"

#include "spo2.h"

#include "battery.h"

volatile bool usb_sync = false;
volatile bool usb_detect = false;

void leds_init()
{
  nrf_gpio_cfg_output(LED_RED);
  nrf_gpio_cfg_output(LED_GREEN);
  nrf_gpio_cfg_output(LED_BLUE);

  led_on(LED_GREEN);
  led_on(LED_BLUE);
  //led_on(LED_RED);


  nrf_delay_ms(400);
  led_off(LED_GREEN);
  led_off(LED_BLUE);


}
/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}




/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    UNUSED_RETURN_VALUE(NRF_LOG_PROCESS());
    nrf_pwr_mgmt_run();
}


static void usb_gpio_interrupt_handler()
{

usb_sync = true;
}


ret_code_t usb_gpiote_init(void)
{
    ret_code_t err_code;
    if (!nrfx_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        APP_ERROR_CHECK(err_code);
    }
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
    in_config.pull = NRF_GPIO_PIN_NOPULL;

    err_code = nrf_drv_gpiote_in_init(29, &in_config, usb_gpio_interrupt_handler);

    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_event_enable(29, true);

    APP_ERROR_CHECK(err_code);
}


void device_init(void)
{
    bool erase_bonds;
current_state.device_state=DEVICE_STATE_IDLE;
    // Initialize.
 //   uart_init();
    log_init();
    timer_modules_init();
    rtcc_init();
    power_management_init();
    leds_init();
    usb_gpiote_init();
    battery_init();
    spo2_init();
    temperature_init();


  //  initialize_fds();
 //   check_existing_user(&current_user);
    //TODO set the default time interval for measurement
    /*
    if(check_existing_user(&current_user)){
//TODO if  data exists
    }
    else
    {
    current_user.sps = ble_new_user.sps;
    current_user.subscription_days = ble_new_user.sd;
    //TODO if no data, create the data with default values.
    //save new user
    }
    */
//          timer_module_run(&measure_spo2_timer_module,MEASURE_SPO2_INTERVAL);

        ble_init();

    
 
}//CONFIG_GPIO_AS_PINRESET

/**@brief Application main function.

 */
int main(void)
{

           device_init();
    

    // Enter main loop.
    

    for (;;)
    {
    
   state_machine_run();
//spo2_measure();
   idle_state_handle();
    }
}

//TODO
/**

 PI is an indicator of the relative strength of the pulsatile signal from pulse oximetry 
 and has been found to be a reliable indicator of peripheral perfusion. PI is calculated by 
 dividing the pulsatile signal (AC) by the nonpulsatile signal (DC) times 100, and is expressed 
 as a percent ranging from 0.02% to 20%. 

 https://www.maximintegrated.com/en/design/technical-documents/app-notes/6/6845.html
 https://pediatrics.medresearch.in/index.php/ijpr/article/view/308/614
 https://www.maximintegrated.com/en/design/technical-documents/app-notes/7/7082.html

  * @}
 */ 