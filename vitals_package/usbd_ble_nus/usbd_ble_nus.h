/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _USBD_BLE_NUS_H_
#define _USBD_BLE_NUS_H_

/* Includes ------------------------------------------------------------------*/
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "ble_nus.h"


#include "app_timer.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "app_util.h"
#include "bsp_btn_ble.h"

#include "dfu_functions.h"
#include "packet_handler.h"

/* Exported types ------------------------------------------------------------*/
typedef enum
{
   BLE_STATE_OFF,
   BLE_STATE_ADVERTISING,
   BLE_STATE_CONNECTED,
}ble_state_t;

extern ble_state_t current_ble_state;
extern volatile bool ble_state_changed;
extern volatile bool usb_state_changed;
/* Exported constants --------------------------------------------------------*/
#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump. Can be used to identify stack location on stack unwind. */
/* Exported Variables --------------------------------------------------------*/
extern uint16_t   m_conn_handle;                                                    /**< Handle of the current connection. */
extern volatile bool m_ble_connected;
extern volatile bool m_usb_connected;
/* Exported macro ------------------------------------------------------------*/
BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */
BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT); /**< BLE NUS service instance. */

/* Exported functions ------------------------------------------------------- */
void advertising_start(void);
void advertising_stop(void);
void ble_init(void);
void app_usb_init(void);
void process_outgoing();
void ble_disconnect(void);
#endif /* _USBD_BLE_NUS_H_ */