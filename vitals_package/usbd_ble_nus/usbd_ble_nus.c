/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "ble_dis.h"
#include "usbd_ble_nus.h"
#include "nrf_ble_qwr.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "timer_functions.h"
#include "nrf_delay.h"
#include "bio2board.h"
#include "state_machine.h"
extern void idle_state_handle(void);
extern void watchdog_reset(void);


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define MANUFACTURER_NAME               "Waferchips"                   /**< Manufacturer. Will be passed to Device Information Service. */
#define MODEL_NUM                       "V1.1"                        /**< Model number. Will be passed to Device Information Service. */
#define MANUFACTURER_ID                 0x1122334455                            /**< Manufacturer ID, part of System ID. Will be passed to Device Information Service. */
#define ORG_UNIQUE_ID                   0x667788                                /**< Organizational Unique ID, part of System ID. Will be passed to Device Information Service. */

#define LED_BLE_NUS_CONN (BSP_BOARD_LED_0)
#define LED_BLE_NUS_RX (BSP_BOARD_LED_1)
#define LED_CDC_ACM_CONN (BSP_BOARD_LED_2)
#define LED_CDC_ACM_RX (BSP_BOARD_LED_3)

#define LED_BLINK_INTERVAL 800

#define ENDLINE_STRING "\r\n"

#define APP_BLE_CONN_CFG_TAG 1 /**< A tag identifying the SoftDevice BLE configuration. */

#define APP_FEATURE_NOT_SUPPORTED BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2 /**< Reply when unsupported features are requested. */

#define DEVICE_NAME "OXYCONNECT"                        /**< Name of device. Will be included in the advertising data. */
#define NUS_SERVICE_UUID_TYPE BLE_UUID_TYPE_VENDOR_BEGIN /**< UUID type for the Nordic UART Service (vendor specific). */

#define APP_BLE_OBSERVER_PRIO 3 /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define APP_ADV_INTERVAL 128    /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_DURATION 3000//18000 /**< The advertising duration (180 seconds) in units of 10 milliseconds. */

#define MIN_CONN_INTERVAL MSEC_TO_UNITS(10, UNIT_1_25_MS)    /**< Minimum acceptable connection interval (20 ms). Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL MSEC_TO_UNITS(50, UNIT_1_25_MS)    /**< Maximum acceptable connection interval (75 ms). Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY 0                                      /**< Slave latency. */
#define CONN_SUP_TIMEOUT MSEC_TO_UNITS(4000, UNIT_10_MS)     /**< Connection supervisory timeout (4 seconds). Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY APP_TIMER_TICKS(5000) /**< Time from initiating an event (connect or start of notification) to the first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY APP_TIMER_TICKS(30000) /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT 3                       /**< Number of attempts before giving up the connection parameter negotiation. */

#define UART_TX_BUF_SIZE 256 /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 256 /**< UART RX buffer size. */

/* Private variables ---------------------------------------------------------*/
uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;
static char m_cdc_data_array[BLE_NUS_MAX_DATA_LEN];
static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
static ble_uuid_t m_adv_uuids[] =                                      /**< Universally unique service identifier. */
    {
        {BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}, {BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}
};
static char m_nus_data_array[BLE_NUS_MAX_DATA_LEN];

volatile bool m_usb_connected = false;
volatile bool m_ble_connected = false;

ble_state_t current_ble_state;
volatile bool ble_state_changed = false;
volatile bool usb_state_changed = false;
extern volatile bool sleep_mode_active_cmd;
/* Private function prototypes -----------------------------------------------*/

static void nus_data_handler(ble_nus_evt_t *p_evt);
static void services_init(void);
static void conn_params_error_handler(uint32_t nrf_error);
static void on_adv_evt(ble_adv_evt_t ble_adv_evt);
static void ble_evt_handler(ble_evt_t const *p_ble_evt, void *p_context);
static void ble_stack_init(void);
static void gatt_evt_handler(nrf_ble_gatt_t *p_gatt, nrf_ble_gatt_evt_t const *p_evt);
static void gatt_init(void);
static void advertising_init(void);
static void nrf_qwr_error_handler(uint32_t nrf_error);
extern void device_sleep(void);
/* Private macro -------------------------------------------------------------*/



NRF_BLE_GATT_DEF(m_gatt);                         /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                           /**< Context for the Queued Write module.*/

/* Private functions ---------------------------------------------------------*/

/**
 * @brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
void gap_params_init(void) {
  uint32_t err_code;
  ble_gap_conn_params_t gap_conn_params;
  ble_gap_conn_sec_mode_t sec_mode;
  //----------------------------------------------------
  ble_gap_addr_t ble_addr;
  uint8_t dev_name[9];
  uint8_t mac_id_truncated[6];
  //strcpy(dev_name,DEVICE_NAME);
  strcpy(dev_name,current_user.adv_id);
 // sd_ble_gap_addr_get(&ble_addr);   // mac address attacin to dev name
 // sprintf(mac_id_truncated,":%x%x",ble_addr.addr[1],ble_addr.addr[0]);
 // strcat(dev_name,mac_id_truncated);
  //----------------------------------------------------
  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
if(current_user.adv_id[0]!=0 || current_user.adv_id[1]!=0 || current_user.adv_id[2]!=0 || current_user.adv_id[3]!=0 || current_user.adv_id[4]!=0 || current_user.adv_id[5]!=0 && current_user.adv_id[6]!=0 && current_user.adv_id[7]!=0 || current_user.adv_id[8]!=0)
{

  err_code = sd_ble_gap_device_name_set(&sec_mode,
      (const uint8_t *)dev_name,
      strlen(dev_name));
}
else
{
     err_code = sd_ble_gap_device_name_set(&sec_mode,
     (const uint8_t *)DEVICE_NAME,
     strlen(DEVICE_NAME));
}

//  err_code = sd_ble_gap_device_name_set(&sec_mode,
//      (const uint8_t *)DEVICE_NAME,
//      strlen(DEVICE_NAME));

  APP_ERROR_CHECK(err_code);

  memset(&gap_conn_params, 0, sizeof(gap_conn_params));

  gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
  gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
  gap_conn_params.slave_latency = SLAVE_LATENCY;
  gap_conn_params.conn_sup_timeout = CONN_SUP_TIMEOUT;

  err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
  APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function processes the data received from the Nordic UART BLE Service and sends
 *          it to the USBD CDC ACM module.
 *
 * @param[in] p_evt Nordic UART Service event.
 */
static void nus_data_handler(ble_nus_evt_t *p_evt) {

  if (p_evt->type == BLE_NUS_EVT_RX_DATA) {
    int len = p_evt->params.rx_data.length;
    NRF_LOG_DEBUG("Received data from BLE NUS. Length %d", len);
    NRF_LOG_HEXDUMP_DEBUG(p_evt->params.rx_data.p_data, len);
    memcpy(m_nus_data_array, p_evt->params.rx_data.p_data, len);
    if (len == BLE_BUFFER_SIZE) {
      ble_data_received = true;
      memcpy(ble_receive_buffer, m_nus_data_array, BLE_BUFFER_SIZE);
    }
  }
}

/** @brief Function for initializing services that will be used by the application. */
static void services_init(void) {
  uint32_t err_code;
  ble_nus_init_t nus_init;
  ble_dis_init_t     dis_init;
  ble_dis_sys_id_t   sys_id;
  nrf_ble_qwr_init_t qwr_init = {0};

  qwr_init.error_handler = nrf_qwr_error_handler;
  err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
  APP_ERROR_CHECK(err_code);

  memset(&nus_init, 0, sizeof(nus_init));

  nus_init.data_handler = nus_data_handler;

  err_code = ble_nus_init(&m_nus, &nus_init);
  APP_ERROR_CHECK(err_code);

  ble_dfu_buttonless_init_t dfus_init =
      {
          .evt_handler = ble_dfu_buttonless_evt_handler};
  err_code = ble_dfu_buttonless_init(&dfus_init);
  APP_ERROR_CHECK(err_code);

      // Initialize Device Information Service.
    memset(&dis_init, 0, sizeof(dis_init));

    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, MANUFACTURER_NAME);
    ble_srv_ascii_to_utf8(&dis_init.model_num_str, MODEL_NUM);

    sys_id.manufacturer_id            = MANUFACTURER_ID;
    sys_id.organizationally_unique_id = ORG_UNIQUE_ID;
    dis_init.p_sys_id                 = &sys_id;

    dis_init.dis_char_rd_sec = SEC_OPEN;

    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error) {
  APP_ERROR_HANDLER(nrf_error);
}

/**
 * @brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error) {
  APP_ERROR_HANDLER(nrf_error);
}

/** @brief Function for initializing the Connection Parameters module. */
static void conn_params_init(void) {
  uint32_t err_code;
  ble_conn_params_init_t cp_init;

  memset(&cp_init, 0, sizeof(cp_init));

  cp_init.p_conn_params = NULL;
  cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
  cp_init.next_conn_params_update_delay = NEXT_CONN_PARAMS_UPDATE_DELAY;
  cp_init.max_conn_params_update_count = MAX_CONN_PARAMS_UPDATE_COUNT;
  cp_init.start_on_notify_cccd_handle = BLE_GATT_HANDLE_INVALID;
  cp_init.disconnect_on_fail = true;
  cp_init.evt_handler = NULL;
  cp_init.error_handler = conn_params_error_handler;

  err_code = ble_conn_params_init(&cp_init);
  APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for handling advertising events.
 *
 * @details This function is called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt) {
  uint32_t err_code;

  switch (ble_adv_evt) {
  case BLE_ADV_EVT_FAST:
//    err_code = app_timer_start(m_blink_ble,
//        APP_TIMER_TICKS(LED_BLINK_INTERVAL),
//        (void *)LED_BLE_NUS_CONN);
//    APP_ERROR_CHECK(err_code);
    break;
  case BLE_ADV_EVT_IDLE:
      NRF_LOG_INFO("-----------------BLE NUS idle-------------");
 // PulseOximeter_begin();
   // advertising_start();
  
//sleep_mode_active_cmd = true;
if(current_state.device_state == DEVICE_STATE_RECORDING)
{
//ble_disconnect();
//advertising_stop();
}
else{
  //  device_sleep();
 // PulseOximeter_proxymode();
 //sleep_mode_active_cmd = true;
    }
    break;
  default:
    break;
  }
}

/** @brief Function for starting advertising. */
void advertising_start(void) {
  uint32_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
  if(err_code != NRF_ERROR_INVALID_STATE)
      APP_ERROR_CHECK(err_code);
  current_ble_state = BLE_STATE_ADVERTISING;
  ble_state_changed = true;
  NRF_LOG_INFO("START");
 // sd_ble_gap_tx_power_set( BLE_GAP_TX_POWER_ROLE_ADV,m_advertising.adv_handle,4);
}

/**
 * @brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const *p_ble_evt, void *p_context) {
  uint32_t err_code;
  //TODO remove led functions
  switch (p_ble_evt->header.evt_id) {
  case BLE_GAP_EVT_CONNECTED:
    NRF_LOG_INFO("BLE NUS connected");
    m_ble_connected = true;
    current_ble_state = BLE_STATE_CONNECTED;
    ble_state_changed = true;
//    err_code = app_timer_stop(m_blink_ble);
//    APP_ERROR_CHECK(err_code);
//    bsp_board_led_on(LED_BLE_NUS_CONN);
    //TODO change to ble connect timeout
  //  timer_module_stop(&idle_to_sleep_timer_module);
   //    timer_module_run(&idle_to_sleep_timer_module,IDLE_TO_SLEEP_INTERVAL);

    m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    break;

  case BLE_GAP_EVT_DISCONNECTED:
    m_ble_connected = false;
    current_ble_state = BLE_STATE_ADVERTISING;
    ble_state_changed  = true;
    NRF_LOG_INFO("BLE NUS disconnected");
    // LED indication will be changed when advertising starts.
    m_conn_handle = BLE_CONN_HANDLE_INVALID;
    break;

  case BLE_GAP_EVT_PHY_UPDATE_REQUEST: {
    NRF_LOG_DEBUG("PHY update request.");
    ble_gap_phys_t const phys =
        {
            .rx_phys = BLE_GAP_PHY_AUTO,
            .tx_phys = BLE_GAP_PHY_AUTO,
        };
    err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
    APP_ERROR_CHECK(err_code);
  } break;

  case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
    // Pairing not supported.
    err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
    APP_ERROR_CHECK(err_code);
    break;

  case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST: {
    ble_gap_data_length_params_t dl_params;

    // Clearing the struct will effectively set members to @ref BLE_GAP_DATA_LENGTH_AUTO.
    memset(&dl_params, 0, sizeof(ble_gap_data_length_params_t));
    err_code = sd_ble_gap_data_length_update(p_ble_evt->evt.gap_evt.conn_handle, &dl_params, NULL);
    APP_ERROR_CHECK(err_code);
  } break;

  case BLE_GATTS_EVT_SYS_ATTR_MISSING:
    // No system attributes have been stored.
    err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
    APP_ERROR_CHECK(err_code);
    break;

  case BLE_GATTC_EVT_TIMEOUT:
    // Disconnect on GATT Client timeout event.
    err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
        BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    APP_ERROR_CHECK(err_code);
    current_ble_state = BLE_STATE_ADVERTISING;
    ble_state_changed = true;
    break;

  case BLE_GATTS_EVT_TIMEOUT:
    // Disconnect on GATT Server timeout event.
    err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
        BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    APP_ERROR_CHECK(err_code);
    current_ble_state = BLE_STATE_ADVERTISING;
    ble_state_changed = true;
    break;

  case BLE_EVT_USER_MEM_REQUEST:
    err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
    APP_ERROR_CHECK(err_code);
    break;

  case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST: {
    ble_gatts_evt_rw_authorize_request_t req;
    ble_gatts_rw_authorize_reply_params_t auth_reply;

    req = p_ble_evt->evt.gatts_evt.params.authorize_request;

    if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID) {
      if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ) ||
          (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
          (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL)) {
        if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE) {
          auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
        } else {
          auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
        }
        auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
        err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
            &auth_reply);
        APP_ERROR_CHECK(err_code);
      }
    }
  } break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

  default:
    // No implementation needed.
    break;
  }
}

/**
 * @brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void) {
  ret_code_t err_code;

  err_code = nrf_sdh_enable_request();
  APP_ERROR_CHECK(err_code);

  // Configure the BLE stack using the default settings.
  // Fetch the start address of the application RAM.
  uint32_t ram_start = 0;
  err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
  APP_ERROR_CHECK(err_code);

  // Enable BLE stack.
  err_code = nrf_sdh_ble_enable(&ram_start);
  APP_ERROR_CHECK(err_code);

  // Register a handler for BLE events.
  NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

/** @brief Function for handling events from the GATT library. */
static void gatt_evt_handler(nrf_ble_gatt_t *p_gatt, nrf_ble_gatt_evt_t const *p_evt) {
  if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)) {
    m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
    NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
  }
  NRF_LOG_DEBUG("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
      p_gatt->att_mtu_desired_central,
      p_gatt->att_mtu_desired_periph);
}

/** @brief Function for initializing the GATT library. */
static void gatt_init(void) {
  ret_code_t err_code;

  err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, 64);
  APP_ERROR_CHECK(err_code);
}

/** @brief Function for initializing the Advertising functionality. */
static void advertising_init(void) {
  uint32_t err_code;
  ble_advertising_init_t init;

  memset(&init, 0, sizeof(init));

  init.advdata.name_type = BLE_ADVDATA_FULL_NAME;
  init.advdata.include_appearance = true;
  init.advdata.flags = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;

  init.srdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
  init.srdata.uuids_complete.p_uuids = m_adv_uuids;

  init.config.ble_adv_fast_enabled = true;
  init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
  init.config.ble_adv_fast_timeout = APP_ADV_DURATION;

  init.evt_handler = on_adv_evt;

  err_code = ble_advertising_init(&m_advertising, &init);
  APP_ERROR_CHECK(err_code);

  ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}


void ble_init() {
//uint8_t adv_name[9]= "BIOCATEMP";
  ble_stack_init();
  gap_params_init();
//  strcpy(current_user.adv_id,adv_name); 
//  gap_params_init();
  gatt_init();
  services_init();
  advertising_init();
  conn_params_init();
  advertising_start();
}

void process_outgoing() {
  if (usb_response_ready == false && ble_response_ready == false)
    return;
  ret_code_t ret;

  if (ble_response_ready) {
  ble_response_ready = false;
    if( current_ble_state != BLE_STATE_CONNECTED)
      return;
    uint16_t len = BLE_BUFFER_SIZE;
//    NRF_LOG_INFO("Transmitting BLE Response");
//    NRF_LOG_HEXDUMP_INFO(ble_transmit_buffer,BLE_BUFFER_SIZE);
    ret = ble_nus_data_send(&m_nus,ble_transmit_buffer,&len,m_conn_handle);

    if (ret == NRF_ERROR_NOT_FOUND) {
      NRF_LOG_INFO("BLE NUS unavailable");
      
    }

    if (ret == NRF_ERROR_RESOURCES) {
      NRF_LOG_ERROR("BLE NUS Too many notifications queued.");
      
    }

    if ((ret != NRF_ERROR_INVALID_STATE) && (ret != NRF_ERROR_BUSY)) {
      //    NRF_LOG_ERROR("=============RESET==================");

    //  APP_ERROR_CHECK(ret);
    }
    
  }
}


void ble_disconnect(void)
{
   NRF_LOG_INFO("Disconnecting BLE");
   ret_code_t err_code;
   if(m_conn_handle == BLE_CONN_HANDLE_INVALID)
      return;
   err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
   APP_ERROR_CHECK(err_code);
   current_ble_state = BLE_STATE_ADVERTISING;
   ble_state_changed = true;
}

void advertising_stop(void)
{
  NRF_LOG_INFO("Advertising Stop");
  if(current_ble_state != BLE_STATE_ADVERTISING)
      return;
  ret_code_t err_code = sd_ble_gap_adv_stop(m_advertising.adv_handle);
  if(err_code != NRF_ERROR_INVALID_STATE)
      APP_ERROR_CHECK(err_code);
  current_ble_state = BLE_STATE_OFF;
  ble_state_changed = true;
}
