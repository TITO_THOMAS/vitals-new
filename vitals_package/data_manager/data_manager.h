/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef DATA_MANAGER_H
#define DATA_MANAGER_H
/* Includes -------------------------------------------------------------------*/
#include "fds_functions.h"
#include "rtc_functions.h"
#include "bio2board.h"

/* Exported types ------------------------------------------------------------*/
#define USER_MAGIC_NUMBER 0xABABABAB

#define DEVICE_TYPE 'O' // Biocalculus, taken from code
#define VERSION_NUMBER 1

#define USER_ID_LEN 6
#define USER_NOT_REGISTERED 'N'
#define USER_REGISTERED 'R'
#define ADV_ID_LEN 9


#define SPS_250 1
#define SPS_500 0
#define MAX_SD 365
#define DEFAULT_SPS SPS_250
#define DEFAULT_SUBSCRIPTION_DAYS 7
#define DEFAULT_TIME_INTERVAL

typedef struct
{
    uint8_t user_id[USER_ID_LEN];
    uint8_t sps;
    uint16_t subscription_days;
    uint8_t user_number;
    uint32_t user_magic_number;
    uint8_t otg_upload_flag;
    time_t record_start_time;
    time_t signup_time;
    uint8_t adv_id[ADV_ID_LEN];
} user_data_t;

typedef struct
{
    uint8_t user_id[USER_ID_LEN];
    uint8_t sps;
    uint16_t sd;
    bool app_cipher_success;
    bool cloud_cipher_success;
    bool user_valid;
    user_data_t * matched_user;
} new_user_req_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern user_data_t current_user;
extern flags_t device_flags;
extern new_user_req_t ble_new_user;
extern volatile bool clearing_user_data;

bool check_existing_user(user_data_t * user);
bool create_new_user(
    user_data_t * user, uint8_t * user_id, uint8_t sps, uint16_t subscription_days);
bool is_sps_valid(uint8_t sps);
bool is_subscription_days_valid(uint8_t sd);
user_data_t * is_existing_user(new_user_req_t * new_user_req);
void save_ble_new_user();
void data_manager_init();
void save_user_record_start_time();
void delete_all_data();
void set_otg_upload_status(bool status);
#endif /* DATA_MANAGER_H */