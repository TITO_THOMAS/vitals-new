/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "data_manager.h"
#include "fds_functions.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define SPS_MULTIPLIER 250
/* File ID and Key used for the User record. */

#define USER_FILE (0xF010)
#define USER_REC_KEY (0x7010)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile bool clearing_user_data = false;
user_data_t current_user;

flags_t device_flags;
new_user_req_t ble_new_user;
/* Private function prototypes -----------------------------------------------*/
static void print_user_details(user_data_t * user);
void save_user(bool is_existing);
void delete_current_user();

/* Private functions ---------------------------------------------------------*/

bool is_sps_valid(uint8_t sps)
{
    if (sps == SPS_250 || sps == SPS_500)
        return true;
    return false;
}

bool is_subscription_days_valid(uint8_t sd)
{
    if (sd > 0 && sd <= MAX_SD)
        return true;
    return false;
}

static void print_user_details(user_data_t * user)
{
    NRF_LOG_INFO("User Magic Number %x", user->user_magic_number);
    NRF_LOG_INFO("User ID");
    NRF_LOG_HEXDUMP_INFO(user->user_id, USER_ID_LEN);
    NRF_LOG_INFO("SPS %d", (user->sps + 1) * SPS_MULTIPLIER);
    NRF_LOG_INFO("Subscription days %d", user->subscription_days);
    NRF_LOG_INFO("Serial number");
    NRF_LOG_HEXDUMP_INFO(user->adv_id, ADV_ID_LEN);
}


bool check_existing_user(user_data_t * user)
{
    fds_record_desc_t desc = {0};
    fds_find_token_t tok = {0};

    memset(user, NULL, sizeof(user_data_t));
    while (fds_record_iterate(&desc, &tok) != FDS_ERR_NOT_FOUND)
    {
        ret_code_t rc;
        fds_flash_record_t temp = {0};

        rc = fds_record_open(&desc, &temp);
        APP_ERROR_CHECK(rc);
        if (rc == FDS_SUCCESS)
        {
            memcpy(user, temp.p_data, sizeof(user_data_t));

            /* Close the record when done reading. */
            rc = fds_record_close(&desc);
            APP_ERROR_CHECK(rc);
            print_user_details(user);
            if (user->user_magic_number == USER_MAGIC_NUMBER)
            {
                memcpy(&current_user, user, sizeof(user_data_t));
                NRF_LOG_INFO("User Found");
                print_user_details(&current_user);
                return true;
            }
        }
    }
    NRF_LOG_INFO("No existing user");
    return false;
}

bool create_new_user(user_data_t * user, uint8_t * user_id, uint8_t sps, uint16_t subscription_days)
{
    user->sps = sps;
    user->subscription_days = subscription_days;
    memcpy(user->user_id, user_id, USER_ID_LEN);
    user->user_magic_number = USER_MAGIC_NUMBER;

    fds_record_t const rec = {.file_id = USER_FILE,
        .key = USER_REC_KEY,
        .data.p_data = user,
        .data.length_words = (sizeof(user_data_t) + 3) / sizeof(uint32_t)};
    print_user_details(user);

    NRF_LOG_INFO("FDS Writing %d words", rec.data.length_words);

    ret_code_t rc = fds_record_write(NULL, &rec);
    if (rc != FDS_SUCCESS)
    {
        NRF_LOG_ERROR("Record Write Returned %s", fds_err_str[rc]);
        return false;
    }
    return true;
}


user_data_t * is_existing_user(new_user_req_t * new_user_req)
{
    user_data_t * temp_user = NULL;
    if (memcmp(current_user.user_id, new_user_req->user_id, USER_ID_LEN) == 0)
    {
        temp_user = &current_user;
    }
    return temp_user;
}


// TODO add background sync for fds
void save_ble_new_user()
{
    bool is_existing;
    if (ble_new_user.matched_user == &current_user)
    {
        is_existing = true;
        NRF_LOG_INFO("is existing= true");
    }
    else
    {
        NRF_LOG_INFO("is existing= false");

        is_existing == false;
        memcpy(current_user.user_id, ble_new_user.user_id, USER_ID_LEN);
        current_user.user_magic_number = USER_MAGIC_NUMBER;
        current_user.signup_time = get_current_time();
    }
    current_user.sps = ble_new_user.sps;
    current_user.subscription_days = ble_new_user.sd;
    save_user(is_existing);
}

void save_user(bool is_existing)
{
    bool user_saved = false;
    user_data_t temp_user;

    fds_record_t const rec = {.file_id = USER_FILE,
        .key = USER_REC_KEY,
        .data.p_data = &current_user,
        .data.length_words = (sizeof(user_data_t) + 3) / sizeof(uint32_t)};

    fds_find_token_t tok = {0};
    fds_record_desc_t desc = {0};

    if (is_existing)
    {
        while (fds_record_iterate(&desc, &tok) != FDS_ERR_NOT_FOUND)
        {
            ret_code_t rc;
            fds_flash_record_t frec = {0};

            rc = fds_record_open(&desc, &frec);
            if (rc == FDS_SUCCESS)
            {
                // compare record with user
                memcpy(&temp_user, frec.p_data, sizeof(user_data_t));
                if (memcmp(temp_user.user_id, current_user.user_id, USER_ID_LEN) == 0)
                {
                    /* Write the updated record to flash. */
                    rc = fds_record_update(&desc, &rec);
                    APP_ERROR_CHECK(rc);
                    user_saved = true;
                    break;
                }
            }
        }
    }
    if (!user_saved)
    {
        // Create new record
        NRF_LOG_INFO("FDS Writing %d words", rec.data.length_words);
        ret_code_t rc = fds_record_write(NULL, &rec);
        if (rc != FDS_SUCCESS)
            NRF_LOG_ERROR("Record Write Returned %s", fds_err_str[rc]);
    }
}

void data_manager_init()
{
    memset(&current_user, 0, sizeof(user_data_t));
    memset(&ble_new_user, 0, sizeof(new_user_req_t));
    memset(&device_flags, 0, sizeof(device_flags));
}

void save_user_record_start_time()
{
    NRF_LOG_INFO("Saving record start time");
    current_user.record_start_time = get_current_time();
    save_user(true);
}

void set_otg_upload_status(bool status)
{
    NRF_LOG_INFO("Setting otg upload flag %d", status);
    if (current_user.otg_upload_flag == status)
        return;
    current_user.otg_upload_flag = status;
    save_user(true);
}

void delete_all_data()
{
    NRF_LOG_INFO("Deleting all data");
//    clearing_user_data = true;
//    delete_current_user();
//    fs_data_reset(true);
//    clearing_user_data = false;
}

void delete_current_user()
{
    NRF_LOG_INFO("Deleting Current user record");
    user_data_t temp_user;

    fds_record_t const rec = {.file_id = USER_FILE,
        .key = USER_REC_KEY,
        .data.p_data = &current_user,
        .data.length_words = (sizeof(user_data_t) + 3) / sizeof(uint32_t)};

    fds_find_token_t tok = {0};
    fds_record_desc_t desc = {0};

    while (fds_record_iterate(&desc, &tok) != FDS_ERR_NOT_FOUND)
    {
        ret_code_t rc;
        fds_flash_record_t frec = {0};

        rc = fds_record_open(&desc, &frec);
        if (rc == FDS_SUCCESS)
        {
            // compare record with user
            memcpy(&temp_user, frec.p_data, sizeof(user_data_t));
            if (memcmp(temp_user.user_id, current_user.user_id, USER_ID_LEN) == 0)
            {
                rc = fds_record_delete(&desc);
                APP_ERROR_CHECK(rc);

                break;
            }
        }
    }
}

void set_adv_name(void) {}