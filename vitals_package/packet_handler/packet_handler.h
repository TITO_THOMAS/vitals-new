/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _PACKET_HANDLER_H_
#define _PACKET_HANDLER_H_

/* Includes -------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "data_manager.h"
#include "authentication.h"

/* Defines -------------------------------------------------------------------*/
#define USB_BUFFER_SIZE     20
#define BLE_BUFFER_SIZE     20
#define MAX_DATA_LEN        20

extern  uint8_t usb_receive_buffer[USB_BUFFER_SIZE];
extern  uint8_t ble_receive_buffer[BLE_BUFFER_SIZE];
extern volatile bool usb_data_received;
extern volatile bool ble_data_received;

extern  uint8_t usb_transmit_buffer[USB_BUFFER_SIZE];
extern  uint8_t ble_transmit_buffer[BLE_BUFFER_SIZE];
extern volatile bool usb_response_ready;
extern volatile bool ble_response_ready;

extern volatile bool activity_send;

/* Exported types ------------------------------------------------------------*/
typedef enum 
{
    USB_PACKET_UNKNOWN,
    USB_PACKET_USER,
    USB_PACKET_CIPHER,
    USB_PACKET_SPECS,
    USB_PACKET_SYNC,
    USB_PACKET_CHECK,
    BLE_PACKET_USER,
    BLE_PACKET_CIPHER,
    BLE_PACKET_TIMESTAMP,
    BLE_PACKET_RECORD_START,
    BLE_PACKET_RECORD_STOP,
    BLE_PACKET_VIEW_START,
    BLE_PACKET_VIEW_STOP,
    BLE_PACKET_SLEEP,
    BLE_PACKET_DELETE,
    BLE_PACKET_SERIAL,
}packet_type_t;

typedef struct
{
    packet_type_t packet_type;
    uint8_t packet_length;
    uint8_t packet_data[MAX_DATA_LEN];
}data_packet_t;

typedef enum
{
  RESPONSE_PACKET_UNKNOWN,
  BLE_USER_RESPONSE,
}response_type_t;

typedef enum
{
    PACKET_OK,
    PACKET_UNKNOWN,
    PACKET_LEN_MISMATCH,
    PACKET_INVALID_PARAM,
}packet_error_t;

typedef struct
{
    response_type_t response_type;
    uint8_t response_data[MAX_DATA_LEN];
}response_packet_t;

/* Exported variables --------------------------------------------------------*/

extern data_packet_t usb_packet, ble_packet;
extern volatile bool ble_view_start_packet_received;
extern volatile bool ble_view_stop_packet_received;
extern volatile bool ble_record_start_packet_received;
extern volatile bool ble_record_stop_packet_received;
extern volatile bool ble_transmit_activity;
extern volatile bool usb_crc_success;
extern volatile bool usb_sync_initiated;
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
bool new_packet_available();
void process_incoming_packet();
void package_ecg_data();
void package_activity_data();
void package_spo2_graph_data();
void package_hr_spo2(void);
void package_alert_packet(void);
extern void save_user(bool is_existing);
#endif /* _PACKET_HANDLER_H_ */