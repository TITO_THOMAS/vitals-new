/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "ble_advdata.h"
#include "usbd_ble_nus.h"
#include "nrf_delay.h"
#include "packet_handler.h"
#include "data_manager.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "battery.h"
#include "MAX30102.h"
#include "temperature.h"
#include "spo2.h"
#include "tmp_interface.h"


#include <string.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define USB_HEAD 'B'
#define USB_TAIL 'Z'
#define BLE_HEAD 'V'
#define BLE_TAIL 'S'

#define SPO2_ALERT_CMD 'W'
#define SPO2_BLE_HEAD 'V'
#define SPO2_BLE_TAIL 'S'
#define HR_BLE_HEAD 'V'
#define HR_BLE_TAIL  'S'

#define USB_USER_CMD 'U'
#define USB_CIPHER_CMD 'C'
#define USB_SPECS_CMD 0x0B
#define USB_SYNC_CMD 0x0E
#define USB_CHECK_RESPONSE 'R'

#define BLE_USER_CMD 'I'
#define BLE_CIPHER_CMD 'C'
#define BLE_TIMESTAMP_CMD 'T'
#define BLE_RECORD_START_CMD 'S'
#define BLE_RECORD_STOP_CMD 'X'
#define BLE_VIEW_START_CMD 'P'
#define BLE_VIEW_STOP_CMD 'p'
#define BLE_SLEEP_CMD 'F'
#define BLE_DELETE_CMD 'U'
#define BLE_SERIAL_NO_CMD 'Q'
#define BLE_USER_INFO_CMD '#'

#define CIPHER_TYPE_APP 'A'
#define CIPHER_TYPE_CLOUD_ACT 'C'
#define CIPHER_TYPE_CLOUD_DEACT 'X'

#define OK 'O'
#define NOT_OK 'N'
#define ERROR 'E'
#define USB_ACK 'K'
#define USB_NACK 'E'

#define HEAD_INDEX 0
#define TAIL_INDEX 19
#define CMD_INDEX 1

#define DATA_INDEX 2

#define USB_USER_LEN 6
#define USB_CIPHER_LEN 16
#define USB_ACK_LEN 1

#define BLE_USER_LEN 9
#define BLE_CIPHER_LEN 17
#define BLE_TIMESTAMP_LEN 6
#define BLE_ACT_LEN 1

#define CIPHER_DATA_LEN 16

#define SPO2_SAMPLES_PER_PACKET 5


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t usb_receive_buffer[USB_BUFFER_SIZE];
uint8_t ble_receive_buffer[BLE_BUFFER_SIZE];
uint8_t usb_transmit_buffer[USB_BUFFER_SIZE];
uint8_t ble_transmit_buffer[BLE_BUFFER_SIZE];
uint32_t num_files;


// TODO implement queue and counters for output data ----- extra
// TODO implement queue and counters for input data ----- extra
volatile bool usb_data_received = false;
volatile bool ble_data_received = false;
volatile bool ble_response_ready = false;
volatile bool usb_response_ready = false;
volatile bool ble_transmit_activity = false;

volatile bool ble_view_start_packet_received = false;
volatile bool ble_view_stop_packet_received = false;
volatile bool ble_record_start_packet_received = false;
volatile bool ble_record_stop_packet_received = false;

volatile bool activity_send = false;
volatile bool usb_sync_initiated = false;
volatile bool usb_crc_success = false;

data_packet_t usb_packet, ble_packet;

/* Private function prototypes -----------------------------------------------*/
static packet_error_t process_ble_serial_number(data_packet_t * packet);
static packet_error_t process_ble_user_packet(data_packet_t * packet);
static bool otg_parse_incoming(data_packet_t * packet);
static void otg_print_packet(data_packet_t * packet);
static bool ble_parse_incoming(data_packet_t * packet);
static void ble_print_packet(data_packet_t * packet);
static bool process_packet(data_packet_t * packet);
packet_error_t process_ble_cipher_packet(data_packet_t * packet);
void prepare_ble_user_response(bool packet_valid);
void prepare_ble_serial_num_response(bool packet_valid);
void prepare_ble_cipher_response(uint8_t cipher_type, bool cipher_success);
packet_error_t process_ble_time_packet(data_packet_t * packet);
void prepare_ble_time_response(bool packet_ok);
packet_error_t process_usb_user_packet(data_packet_t *packet);
packet_error_t process_usb_cipher_packet(data_packet_t *packet);
packet_error_t process_usb_specs_packet();
void process_usb_ack_packet(data_packet_t *packet);
/* Private functions ---------------------------------------------------------*/

static bool otg_parse_incoming(data_packet_t * packet)
{
    uint8_t head, tail, cmd, aux;
    bool ret;
    head = usb_receive_buffer[HEAD_INDEX];
    tail = usb_receive_buffer[TAIL_INDEX];
    if (head != USB_HEAD || tail != USB_TAIL)
        return false;
    cmd = usb_receive_buffer[CMD_INDEX];

    switch (cmd)
    {
        case USB_USER_CMD:
        {
            packet->packet_type = USB_PACKET_USER;
            packet->packet_length = USB_USER_LEN;
            memcpy(packet->packet_data, &usb_receive_buffer[DATA_INDEX], USB_USER_LEN);
            return true;
        }

        case USB_CIPHER_CMD:
        {
            packet->packet_type = USB_PACKET_CIPHER;
            packet->packet_length = USB_CIPHER_LEN;
            memcpy(packet->packet_data, &usb_receive_buffer[DATA_INDEX], USB_CIPHER_LEN);
            return true;
        }

        case USB_SPECS_CMD:
        {
            packet->packet_type = USB_PACKET_SPECS;
            return true;
        }

        case USB_SYNC_CMD:
        {
            packet->packet_type = USB_PACKET_SYNC;
            return true;
        }

        case USB_CHECK_RESPONSE:
        {
            packet->packet_type = USB_PACKET_CHECK;
            packet->packet_length = USB_ACK_LEN;
            packet->packet_data[0] = usb_receive_buffer[DATA_INDEX];
            return true;
        }

        default:
            return false;
    }
}

static void otg_print_packet(data_packet_t * packet)
{
    packet_type_t type = packet->packet_type;
    switch (type)
    {
        case USB_PACKET_USER:
            NRF_LOG_INFO("USB User Packet Received");
            break;
        case USB_PACKET_CIPHER:
            NRF_LOG_INFO("USB Cipher Packet Receieved");
            break;
        case USB_PACKET_SPECS:
            NRF_LOG_INFO("USB Specs request Receieved");
            break;
        case USB_PACKET_SYNC:
            NRF_LOG_INFO("USB Sync Packet Receieved");
            break;
        case USB_PACKET_CHECK:
            NRF_LOG_INFO("USB CRC Response Packet Receieved");
            break;
        default:
            NRF_LOG_INFO("USB Packet Unknown");
            break;
    }
}

static bool ble_parse_incoming(data_packet_t * packet)
{
    uint8_t head, tail, cmd, aux;
    bool ret;
    head = ble_receive_buffer[HEAD_INDEX];
    tail = ble_receive_buffer[TAIL_INDEX];
    if (head != BLE_HEAD || tail != BLE_TAIL)
        return false;
    cmd = ble_receive_buffer[CMD_INDEX];
//NRF_LOG_HEXDUMP_INFO(ble_receive_buffer, 20);
    switch (cmd)
    {
        case BLE_USER_CMD:
            packet->packet_type = BLE_PACKET_USER;
            packet->packet_length = BLE_USER_LEN;
            memcpy(packet->packet_data, &ble_receive_buffer[DATA_INDEX], BLE_USER_LEN);
            return true;

        case BLE_CIPHER_CMD:
            packet->packet_type = BLE_PACKET_CIPHER;
            packet->packet_length = BLE_CIPHER_LEN;
            memcpy(packet->packet_data, &ble_receive_buffer[DATA_INDEX], BLE_CIPHER_LEN);
            return true;

        case BLE_TIMESTAMP_CMD:
            packet->packet_type = BLE_PACKET_TIMESTAMP;
            packet->packet_length = BLE_TIMESTAMP_LEN;
            memcpy(packet->packet_data, &ble_receive_buffer[DATA_INDEX], BLE_TIMESTAMP_LEN);
            return true;

        case BLE_RECORD_START_CMD:
            packet->packet_type = BLE_PACKET_RECORD_START;
            return true;

        case BLE_RECORD_STOP_CMD:
            packet->packet_type = BLE_PACKET_RECORD_STOP;
            return true;

        case BLE_VIEW_START_CMD:
            packet->packet_type = BLE_PACKET_VIEW_START;
            return true;

        case BLE_VIEW_STOP_CMD:
            packet->packet_type = BLE_PACKET_VIEW_STOP;
            return true;

        case BLE_SLEEP_CMD:
            packet->packet_type = BLE_PACKET_SLEEP;
            return true;

        case BLE_DELETE_CMD:
            packet->packet_type = BLE_PACKET_DELETE;
            packet->packet_length = BLE_ACT_LEN;
            packet->packet_data[0] = ble_receive_buffer[DATA_INDEX];
            return true;
        case BLE_SERIAL_NO_CMD:
              
            packet->packet_type = BLE_PACKET_SERIAL;
            packet->packet_length = BLE_USER_LEN;
            memcpy(packet->packet_data, &ble_receive_buffer[DATA_INDEX], BLE_USER_LEN);
            return true;
        default:
            return false;
    }
}

static void ble_print_packet(data_packet_t * packet)
{
    packet_type_t type = packet->packet_type;
    switch (type)
    {
        case BLE_PACKET_USER:
            NRF_LOG_INFO("BLE User Packet Received");
            break;
        case BLE_PACKET_CIPHER:
            NRF_LOG_INFO("BLE Cipher Packet Receieved");
            break;
        case BLE_PACKET_TIMESTAMP:
            NRF_LOG_INFO("BLE Timestamp Packet Receieved");
            break;
        case BLE_PACKET_RECORD_START:
            NRF_LOG_INFO("BLE Record start Packet Receieved");
            break;
        case BLE_PACKET_RECORD_STOP:
            NRF_LOG_INFO("BLE Record stop Packet Received");
            break;
        case BLE_PACKET_VIEW_START:
            NRF_LOG_INFO("BLE View start Packet Receieved");
            break;
        case BLE_PACKET_VIEW_STOP:
            NRF_LOG_INFO("BLE View stop packet Receieved");
            break;
        case BLE_PACKET_SLEEP:
            NRF_LOG_INFO("BLE sleep Packet Receieved");
            break;
        case BLE_PACKET_DELETE:
            NRF_LOG_INFO("BLE USER Delete Packet Receieved");
            break;
        case BLE_PACKET_SERIAL:
            NRF_LOG_INFO("BLE Serial number Packet Receieved");
            break;
        default:
            NRF_LOG_INFO("BLE Packet Unknown");
            break;
    }
}

static bool process_packet(data_packet_t * packet)
{
    packet_type_t type = packet->packet_type;
    packet_error_t result = PACKET_UNKNOWN;
    switch (type)
    {
        case BLE_PACKET_USER:
        result = PACKET_OK;
            result = process_ble_user_packet(packet);
            break;
        case BLE_PACKET_CIPHER:
        result = PACKET_OK;
         //   result = process_ble_cipher_packet(packet);
            break;
        case BLE_PACKET_TIMESTAMP:
        result = PACKET_OK;
          //  result = process_ble_time_packet(packet);
            ble_transmit_activity = true;
            break;
        case BLE_PACKET_VIEW_START:
            result = PACKET_OK;
            ble_view_start_packet_received = true;
            break;
        case BLE_PACKET_VIEW_STOP:
            ble_view_stop_packet_received = true;
            result = PACKET_OK;
            break;
        case BLE_PACKET_RECORD_START:
            result = PACKET_OK;
            ble_record_start_packet_received = true;
            device_flags.ecg_record_request_received = true;
            break;
        case BLE_PACKET_RECORD_STOP:
            ble_record_stop_packet_received = true;
            result = PACKET_OK;
            break;
        case BLE_PACKET_SERIAL:
             result = process_ble_serial_number(packet);
             result = PACKET_OK;
             break;
        case USB_PACKET_USER:
            result = process_usb_user_packet(packet);
            break;
        case USB_PACKET_CIPHER:
            result = process_usb_cipher_packet(packet);
            break;
        case USB_PACKET_SPECS:
            result = process_usb_specs_packet();
            break;
        case USB_PACKET_SYNC:
            usb_sync_initiated = true;
        case USB_PACKET_CHECK:
            process_usb_ack_packet(packet);
            break;
        default:
            break;
    }
    if (result == PACKET_OK)
    {
        if (type > USB_PACKET_UNKNOWN && type <= USB_PACKET_CHECK)
        {
            usb_response_ready = true;
            
        }
        else
        {
            ble_response_ready = true;
            
        }
        return true;
    }
    return false;
}

static packet_error_t process_ble_user_packet(data_packet_t * packet)
{
    bool packet_valid = true;
    uint8_t * ptr = packet->packet_data;
    memset(&ble_new_user, NULL, sizeof(new_user_req_t));
    memcpy(&ble_new_user.user_id, ptr, USER_ID_LEN);
    ptr = ptr + USER_ID_LEN;
    ble_new_user.sps = *ptr;
    ptr++;
    ble_new_user.sd = (*ptr) * 10;
    ptr++;
    ble_new_user.sd = ble_new_user.sd + *ptr;
    NRF_LOG_INFO("Got SPS %d SD %d", ble_new_user.sps, ble_new_user.sd);
    if (!is_sps_valid(ble_new_user.sps))
    {
        ble_new_user.sps = DEFAULT_SPS;
        packet_valid = false;
    }
    if (!is_subscription_days_valid(ble_new_user.sd))
    {
        ble_new_user.sd = DEFAULT_SUBSCRIPTION_DAYS;
        packet_valid = false;
    }
    ble_new_user.matched_user = is_existing_user(&ble_new_user);
    if(ble_new_user.matched_user != NULL)
    {
//    if(memcmp(ble_new_user.matched_user->user_id,ble_new_user.user_id,USER_ID_LEN) == 0)
       NRF_LOG_DEBUG("Matched to Existing User");
    }
    else
      NRF_LOG_DEBUG("No Matched to Existing User");
    prepare_ble_user_response(packet_valid);
    return PACKET_OK;
}

static packet_error_t process_ble_serial_number(data_packet_t * packet)
{
    bool packet_valid = true;
    bool is_existing = true;
    uint8_t * ptr = packet->packet_data;
    memcpy(&current_user.adv_id, ptr, ADV_ID_LEN);

    NRF_LOG_INFO("Serial numb = %s", current_user.adv_id);
    save_user(is_existing);
    prepare_ble_serial_num_response(packet_valid);
    //https://devzone.nordicsemi.com/f/nordic-q-a/35942/device-name-change/236571#236571
    return PACKET_OK;
}

bool new_packet_available() 
{ 
    return (ble_data_received || usb_data_received); 
}

void process_incoming_packet()
{
    bool packet_valid;
    if (ble_data_received == true)
    {
        ble_data_received = false;
        packet_valid = ble_parse_incoming(&ble_packet);
        if (packet_valid)
        {
            ble_print_packet(&ble_packet);
            process_packet(&ble_packet);
        }
        else
        {
            NRF_LOG_INFO("BLE Packet invalid");
        }
    }
}
void prepare_ble_serial_num_response(bool packet_valid)
{
    ble_transmit_buffer[HEAD_INDEX] = BLE_HEAD;
    ble_transmit_buffer[CMD_INDEX] = BLE_SERIAL_NO_CMD;
    ble_transmit_buffer[TAIL_INDEX] = BLE_TAIL;
    ble_transmit_buffer[2]= 'O';
    return;

}
void prepare_ble_user_response(bool packet_valid)
{
    ble_gap_addr_t ble_addr;
    memset(ble_transmit_buffer, 0, BLE_BUFFER_SIZE);
    ble_transmit_buffer[HEAD_INDEX] = BLE_HEAD;
    ble_transmit_buffer[CMD_INDEX] = BLE_USER_CMD;
    ble_transmit_buffer[TAIL_INDEX] = BLE_TAIL;
    if (ble_new_user.matched_user == NULL)
    {
        ble_transmit_buffer[2] = USER_NOT_REGISTERED;
        ble_transmit_buffer[8] = DEFAULT_SPS;
        ble_transmit_buffer[9] = (DEFAULT_SUBSCRIPTION_DAYS >> 8) & 0xFF;
        ble_transmit_buffer[10] = DEFAULT_SUBSCRIPTION_DAYS & 0xFF;
    }
    else
    {
        ble_transmit_buffer[2] = USER_REGISTERED;
        ble_transmit_buffer[8] = ble_new_user.sps;
        ble_transmit_buffer[9] = (ble_new_user.sd >> 8) & 0xFF;
        ble_transmit_buffer[10] = ble_new_user.sd & 0xFF;
    }
    get_random_number();
    ble_transmit_buffer[3] = random_buffer[0];
    ble_transmit_buffer[4] = random_buffer[1];
    ble_transmit_buffer[5] = device_flags.ecg_record_request_received;
    ble_transmit_buffer[6] = device_flags.flash_storage_full;
    ble_transmit_buffer[7] = current_user.otg_upload_flag;
  //  ble_transmit_buffer[11] = packet_valid;
// mac address of device
    sd_ble_gap_addr_get(&ble_addr);
    ble_transmit_buffer[11] = ble_addr.addr[5];
    ble_transmit_buffer[12] = ble_addr.addr[4];
    ble_transmit_buffer[13] = ble_addr.addr[3];
    ble_transmit_buffer[14] = ble_addr.addr[2];
    ble_transmit_buffer[15] = ble_addr.addr[1];
    ble_transmit_buffer[16] = ble_addr.addr[0];
    ble_transmit_buffer[17] = DEVICE_TYPE;
    ble_transmit_buffer[18] = VERSION_NUMBER;
    save_ble_new_user();
    return;
}

packet_error_t process_ble_cipher_packet(data_packet_t * packet)
{
    uint8_t cipher_type = packet->packet_data[0];
    uint8_t cipher_generated[CIPHER_DATA_LEN], clear_text[CIPHER_DATA_LEN],temp_advname[ADV_ID_LEN];
    uint8_t * cipher_received = &packet->packet_data[1];
    memset(clear_text, '0' , CIPHER_DATA_LEN);
    bool cipher_success = false;
    if (cipher_type == CIPHER_TYPE_APP)
    {
        memcpy(clear_text, ble_new_user.user_id, USER_ID_LEN);
        clear_text[14] = random_buffer[0];
        clear_text[15] = random_buffer[1];
        aes_encrypt(APP_KEY, clear_text, cipher_generated);
        NRF_LOG_DEBUG("Cipher Received");
        NRF_LOG_HEXDUMP_DEBUG(cipher_received,16);
        if (memcmp(cipher_generated, cipher_received, CIPHER_DATA_LEN) == 0)
        {
            NRF_LOG_INFO("App Cipher Success");
            ble_new_user.app_cipher_success = true;
            cipher_success = true;
        }
        else
        {
            ble_new_user.app_cipher_success = false;
            NRF_LOG_INFO("App Cipher Fail");
        }
    }
    else if (cipher_type == CIPHER_TYPE_CLOUD_ACT) 
    {

        memcpy(clear_text, &ble_new_user.user_id, USER_ID_LEN);
        clear_text[0] = random_buffer[0];
        clear_text[15] = random_buffer[1];
        aes_encrypt(CLOUD_ACT_KEY, clear_text, cipher_generated);
        NRF_LOG_DEBUG("Cipher Received Text");
        NRF_LOG_HEXDUMP_DEBUG(cipher_received,16);
        if (memcmp(cipher_generated, cipher_received, CIPHER_DATA_LEN) == 0)
        {
            ble_new_user.cloud_cipher_success = true;
            cipher_success = true;
            save_ble_new_user();
        }
        else
            ble_new_user.cloud_cipher_success = false;
    }
    else if (cipher_type == CIPHER_TYPE_CLOUD_DEACT)
    {
       memcpy(clear_text, current_user.user_id, USER_ID_LEN);
       clear_text[0] = random_buffer[0];
       clear_text[15] = random_buffer[1];
       aes_encrypt(CLOUD_DEACT_KEY, clear_text, cipher_generated);
       if(memcmp(cipher_generated,cipher_received,CIPHER_DATA_LEN) == 0)
       {
          cipher_success = true; 
          check_existing_user(&current_user);    // reading current user details.
          memcpy(temp_advname, current_user.adv_id, ADV_ID_LEN);  // storing temporary 
          delete_all_data();      // deleting all data
          memcpy(current_user.adv_id, temp_advname, ADV_ID_LEN);     // saving the data.
          save_user(true);
          check_existing_user(&current_user);    // reading current user details to check if everything is cleared.

       }
    }
    else;
    prepare_ble_cipher_response(cipher_type, cipher_success);
    return PACKET_OK;
}

void prepare_ble_cipher_response(uint8_t cipher_type, bool cipher_success)
{
    memset(ble_transmit_buffer, 0, BLE_BUFFER_SIZE);
    ble_transmit_buffer[HEAD_INDEX] = BLE_HEAD;
    ble_transmit_buffer[TAIL_INDEX] = BLE_TAIL;
    ble_transmit_buffer[CMD_INDEX] = BLE_CIPHER_CMD;
    ble_transmit_buffer[DATA_INDEX] = cipher_type;
    if (cipher_success == true)
        ble_transmit_buffer[3] = OK;
    else
        ble_transmit_buffer[3] = NOT_OK;
    NRF_LOG_INFO("Cipher BLE Response");
    NRF_LOG_HEXDUMP_INFO(ble_transmit_buffer,BLE_BUFFER_SIZE);
}

packet_error_t process_ble_time_packet(data_packet_t * packet)
{

   
    uint8_t day, month, year, hour, minute, second;
    year = packet->packet_data[0];
    month = packet->packet_data[1];
    day = packet->packet_data[2];
    hour = packet->packet_data[3];
    minute = packet->packet_data[4];
    second = packet->packet_data[5];

    bool packet_ok = true;
    
    if(!is_date_valid(day,month,year)){
      packet_ok = false;
          NRF_LOG_INFO("Date invalid");

      }
    if(!is_time_valid(hour, minute, second)){
      packet_ok = false;
                NRF_LOG_INFO("time invalid");

      }

    if(packet_ok)
    {
      set_rtc_date_time(year + 100, month, day, hour, minute, second);
    }
    prepare_ble_time_response(packet_ok);
    return PACKET_OK;
}


void prepare_ble_time_response(bool packet_ok)
{
    memset(ble_transmit_buffer,NULL,BLE_BUFFER_SIZE);
    ble_transmit_buffer[HEAD_INDEX] = BLE_HEAD;
    ble_transmit_buffer[TAIL_INDEX] = BLE_TAIL;
    ble_transmit_buffer[CMD_INDEX] = BLE_TIMESTAMP_CMD;
    if(packet_ok)
    {
      ble_transmit_buffer[DATA_INDEX] = OK;
      nrf_delay_ms(10);
//      activity_send  = true;
//      activity_read  = true;
      nrf_delay_ms(10);
      //enable_r2r();
      NRF_LOG_INFO("Enabling activity send");
    }
    else
      ble_transmit_buffer[DATA_INDEX] = ERROR;
}

void package_ecg_data()
{
//   memset(ble_transmit_buffer,0,BLE_BUFFER_SIZE);
//   ble_transmit_buffer[HEAD_INDEX] = ECG_BLE_HEAD;
//   ble_transmit_buffer[TAIL_INDEX] = ECG_BLE_TAIL;
//   uint8_t ble_buffer_index = 1;
//   for(uint8_t i = 0 ; i < ECG_SAMPLES_PER_PACKET ; i++)
//   {
//      memcpy(&ble_transmit_buffer[ble_buffer_index],ecg_read_data[ble_ecg_transmit_index].samples,ECG_SAMPLE_LEN);
//      ble_buffer_index = ble_buffer_index + ECG_SAMPLE_LEN;
//      ble_ecg_transmit_index++;
//      num_samples--;
//   }
//  NRF_LOG_INFO("Number of samples left=%d",num_samples);

}

void package_alert_packet(void)
{
   memset(ble_transmit_buffer,0,BLE_BUFFER_SIZE);
       ble_transmit_buffer[HEAD_INDEX] = BLE_HEAD;
    ble_transmit_buffer[TAIL_INDEX] = BLE_TAIL;
    ble_transmit_buffer[1] = 'A';
//ble_transmit_buffer[2] = address;
//ble_transmit_buffer[3] = address;

    ble_transmit_buffer[CMD_INDEX] = SPO2_ALERT_CMD;

}
void package_spo2_graph_data()
{
   memset(ble_transmit_buffer,0,BLE_BUFFER_SIZE);
   ble_transmit_buffer[HEAD_INDEX] = SPO2_BLE_HEAD;
   ble_transmit_buffer[TAIL_INDEX] = SPO2_BLE_TAIL;
   ble_transmit_buffer[1] = 'S';
  // ble_transmit_buffer[2] = address;
  // ble_transmit_buffer[3] = address;
   uint8_t ble_buffer_index = 4;

ble_ecg_transmit_index = 0;
   for(uint8_t i = 0 ; i < SPO2_SAMPLES_PER_PACKET ; i++)
   {
      memcpy(&ble_transmit_buffer[ble_buffer_index],spo2_read_data[ble_ecg_transmit_index].samples,SPO2_SAMPLE_LEN);
      ble_buffer_index = ble_buffer_index + SPO2_SAMPLE_LEN;
      ble_ecg_transmit_index++;
      num_samples--;
   }
  //NRF_LOG_INFO("Number of samples left=%d",num_samples);
  //NRF_LOG_HEXDUMP_INFO(ble_transmit_buffer,20);
  //NRF_LOG_INFO("Number of samples left=%d",num_samples);

}
void package_hr_spo2(void)
{
   memset(ble_transmit_buffer,0,BLE_BUFFER_SIZE);
   ble_transmit_buffer[HEAD_INDEX] = HR_BLE_HEAD;
   ble_transmit_buffer[TAIL_INDEX] = HR_BLE_TAIL;

//   uint32_t temp_ir_value = get_current_ir();
  // ble_transmit_buffer[1] = (uint8_t) get_current_hr();//(uint8_t)BeatDetector_getRate();
     ble_transmit_buffer[1] = 'P';
     //ble_transmit_buffer[2] = ADDRESS;
     //ble_transmit_buffer[3] = ADDRESS;
     ble_transmit_buffer[4] = (uint8_t) get_current_hr();
     ble_transmit_buffer[5] = (uint8_t)get_current_spo2();//(uint8_t) SpO2Calculator_getSpO2();
   if( ble_transmit_buffer[4]>0 && ble_transmit_buffer[5] >0)
   ble_transmit_buffer[6] = 1;
   else
  ble_transmit_buffer[6] = 0;
  //alertbutton;
 //  ble_transmit_buffer[7] = alert;
   ble_transmit_buffer[8] = get_battery_level();
   ble_transmit_buffer[9] = (uint8_t) get_current_temperature();
   ble_transmit_buffer[10] = (uint8_t) get_current_pi_ir();
   ble_transmit_buffer[11] = VERSION_NUMBER;
NRF_LOG_INFO("====pir====: %d",ble_transmit_buffer[10]);
  // NRF_LOG_INFO("HR_NEW=%d, SPO2=%d",ble_transmit_buffer[1],ble_transmit_buffer[2]);
 // sprintf(ble_transmit_buffer,"I:%d,R:%d",((uint32_t)get_current_pi_ir()),((uint32_t)get_current_pi_red()));

}
void package_activity_data()
{
//   memset(ble_transmit_buffer,0,BLE_BUFFER_SIZE);
//   ble_transmit_buffer[HEAD_INDEX] = ACTIVITY_BLE_HEAD;
//   ble_transmit_buffer[TAIL_INDEX] = ACTIVITY_BLE_TAIL;
//   ble_transmit_buffer[1] = (r2r >> 8) & 0xFF;
//   ble_transmit_buffer[2] = r2r & 0xFF;
//   ble_transmit_buffer[3] = get_activity_level();
//   ble_transmit_buffer[4] = get_battery_level();
//   if(lead_current_status == LEAD_STATUS_ON)
//   ble_transmit_buffer[5] = '1';
//   else
//    ble_transmit_buffer[5] = '0';
}

packet_error_t process_usb_user_packet(data_packet_t *packet)
{
    memset(usb_transmit_buffer,NULL,USB_BUFFER_SIZE);
    usb_transmit_buffer[HEAD_INDEX] = USB_HEAD;
    usb_transmit_buffer[TAIL_INDEX] = USB_TAIL;
    usb_transmit_buffer[CMD_INDEX] = USB_USER_CMD;
    if(memcmp(&current_user.user_id,packet->packet_data,USER_ID_LEN) != 0)
    {
        NRF_LOG_INFO("USB User not recognized");
        usb_transmit_buffer[DATA_INDEX] = OK;//NOT_OK;
    }
    else
    {
        usb_transmit_buffer[DATA_INDEX] = OK;
        NRF_LOG_INFO("USB user matched");
        get_random_number();
        usb_transmit_buffer[3] = random_buffer[0];
        usb_transmit_buffer[4] = random_buffer[1];
    }    
    return PACKET_OK;
}

packet_error_t process_usb_cipher_packet(data_packet_t *packet)
{
    uint8_t cipher_generated[CIPHER_DATA_LEN], clear_text[CIPHER_DATA_LEN];
    uint8_t * cipher_received = &packet->packet_data[0];
    memset(&clear_text[6], '0' , CIPHER_DATA_LEN - USER_ID_LEN);
    memcpy(clear_text, current_user.user_id,USER_ID_LEN);
    clear_text[0] = random_buffer[0];
    clear_text[15] = random_buffer[1];
    aes_encrypt(USB_AUTH_KEY,clear_text,cipher_generated);

    memset(usb_transmit_buffer,NULL,USB_BUFFER_SIZE);
    usb_transmit_buffer[HEAD_INDEX] = USB_HEAD;
    usb_transmit_buffer[TAIL_INDEX] = USB_TAIL;
    usb_transmit_buffer[CMD_INDEX] = USB_CIPHER_CMD;
    if(memcmp(cipher_generated,cipher_received,CIPHER_DATA_LEN) == 0)
    {
        usb_transmit_buffer[DATA_INDEX] = OK; 
    }
    else
        usb_transmit_buffer[DATA_INDEX] = OK;//ERROR;
    
    usb_transmit_buffer[3] = DEVICE_TYPE;
    usb_transmit_buffer[4] = VERSION_NUMBER;
    memcpy(&usb_transmit_buffer[9], cipher_received + 6, 10);
    return PACKET_OK;
}

packet_error_t process_usb_specs_packet()
{
//    memset(usb_transmit_buffer,NULL,USB_BUFFER_SIZE);
//    usb_transmit_buffer[HEAD_INDEX] = USB_HEAD;
//    usb_transmit_buffer[TAIL_INDEX] = USB_TAIL;
//    usb_transmit_buffer[CMD_INDEX] = USB_SPECS_CMD;
//    uint32_t total_file_size = get_total_file_size(&num_files);
//    usb_transmit_buffer[2] = (total_file_size >> 24) & 0xFF;
//    usb_transmit_buffer[3] = (total_file_size >> 16) & 0xFF;
//    usb_transmit_buffer[4] = (total_file_size >> 8) & 0xFF;
//    usb_transmit_buffer[5] = total_file_size & 0xFF;
//    usb_transmit_buffer[6] = num_files;
//    NRF_LOG_INFO("File size=%d, num file=%d\r\n",total_file_size,num_files);
//
//    struct tm* record_start = localtime(&current_user.record_start_time);
//    usb_transmit_buffer[7] = record_start->tm_year - 100;
//    usb_transmit_buffer[8] = record_start->tm_mon + 1;
//    usb_transmit_buffer[9] = record_start->tm_mday;
//    usb_transmit_buffer[10] = record_start->tm_hour;
//    usb_transmit_buffer[11] = record_start->tm_min;
//    usb_transmit_buffer[12] = record_start->tm_sec;

    return PACKET_OK;
}

void process_usb_ack_packet(data_packet_t *packet)
{
    usb_crc_success = false;
    if(packet->packet_data[0] == USB_ACK)
        usb_crc_success = true;
}
