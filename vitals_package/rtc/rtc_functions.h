/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RTC_FUNCTIONS_H
#define _RTC_FUNCTIONS_H

#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"
#include <stdint.h>
#include <stdbool.h>
#include "time.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
extern volatile bool activity_timer_expired;

/* Exported functions ------------------------------------------------------- */
void rtcc_init();
void set_rtc_date_time(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second);
char *rtc_get_time_string(bool calibrated);
bool is_time_valid(uint8_t hour,uint8_t minute,uint8_t second);
bool is_date_valid(uint8_t year, uint8_t month, uint8_t day);
time_t get_current_time();
struct tm *rtc_get_time(void);
#endif /* _TEST_H_ */