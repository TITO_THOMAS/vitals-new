/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "rtc_functions.h"
#include "debug_config.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "battery.h"
#include "led_functions.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define RTC_CC_VALUE 8

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(2); /**< Declaring an instance of nrf_drv_rtc for RTC0. */

static struct tm time_struct, m_tm_return_time;
static time_t m_time, m_last_calibrate_time = 0;
static float m_calibrate_factor = 0.0f;

/* Private function prototypes -----------------------------------------------*/
static void rtc_handler(nrf_drv_rtc_int_type_t int_type);

/* Private functions ---------------------------------------------------------*/
void rtcc_init() {
  uint32_t err_code;

  //Initialize RTC instance
  nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
  config.prescaler = 4095;
  err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_rtc_cc_set(&rtc, 0, RTC_CC_VALUE, true); //Set RTC compare value to trigger interrupt. Configure the interrupt frequency by adjust RTC_CC_VALUE and RTC_FREQUENCY constant in top of main.c
  APP_ERROR_CHECK(err_code);

  //Power on RTC instance
  nrf_drv_rtc_enable(&rtc);
}

static void rtc_handler(nrf_drv_rtc_int_type_t int_type) {
  uint32_t err_code;
  if (int_type == NRF_DRV_RTC_INT_COMPARE0) {
    m_time++;
    err_code = nrf_drv_rtc_cc_set(&rtc, 0, RTC_CC_VALUE, true); //Set RTC compare value. This needs to be done every time as the nrf_drv_rtc clears the compare register on every compare match
    APP_ERROR_CHECK(err_code);
    nrf_drv_rtc_counter_clear(&rtc);
#ifdef RTC_DEBUG
    NRF_LOG_INFO("%s", rtc_get_time_string(false));
#endif
    handle_battery_every_second();
    handle_leds_every_second();
  }
}

void set_rtc_date_time(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second) {
  static time_t uncal_difftime, difftime, newtime;
  time_struct.tm_year = year;
  time_struct.tm_mon = month;
  time_struct.tm_mday = day;
  time_struct.tm_hour = hour;
  time_struct.tm_min = minute;
  time_struct.tm_sec = second;
  newtime = mktime(&time_struct);

  nrfx_rtc_counter_clear(&rtc);

  // Calculate the calibration offset
  if (m_last_calibrate_time != 0) {
    difftime = newtime - m_last_calibrate_time;
    uncal_difftime = m_time - m_last_calibrate_time;
    m_calibrate_factor = (float)difftime / (float)uncal_difftime;
  }

  // Assign the new time to the local time variables
  m_time = m_last_calibrate_time = newtime;
}

struct tm *rtc_get_time(void) {
  time_t return_time;
  return_time = m_time;
  m_tm_return_time = *localtime(&return_time);
  return &m_tm_return_time;
}

struct tm *rtc_get_time_calibrated(void) {
  time_t uncalibrated_time, calibrated_time;
  if (m_calibrate_factor != 0.0f) {
    uncalibrated_time = m_time;
    calibrated_time = m_last_calibrate_time + (time_t)((float)(uncalibrated_time - m_last_calibrate_time) * m_calibrate_factor + 0.5f);
    m_tm_return_time = *localtime(&calibrated_time);
    return &m_tm_return_time;
  } else
    return rtc_get_time();
}

char *rtc_get_time_string(bool calibrated) {
  static char cal_string[80];
  strftime(cal_string, 80, "%x - %H:%M:%S", (calibrated ? rtc_get_time_calibrated() : rtc_get_time()));
  return cal_string;
}

bool is_time_valid(uint8_t hour, uint8_t minute, uint8_t second) {
  if (hour > 23 || minute > 59 || second > 59)
    return false;
  return true;
}

bool is_date_valid(uint8_t year, uint8_t month, uint8_t day) {
  if (year > 99 || month > 12 || day > 31)
    return false;
  if ((month == 4 || month == 6 || month == 9 || month == 11) && (day > 30))
    return false;
  if (month != 2)
    return true;
  if (year % 4 == 0 && day > 29)
    return false;
  if (year % 4 != 0 && day > 28)
    return false;
  return true;
}

time_t get_current_time()
{
    return m_time;
}

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */