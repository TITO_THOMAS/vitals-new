/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "timer_functions.h"
#include "bsp_btn_ble.h"
#include "bio2board.h"
#include "state_machine.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
app_timer_module_t idle_to_sleep_timer_module;
app_timer_module_t blue_led_off_timer_module;
app_timer_module_t lead_on_adv_start_timer_module;
app_timer_module_t lead_off_detect_timer_module;
app_timer_module_t red_led_off_timer_module;
app_timer_module_t green_led_off_timer_module;
app_timer_module_t ble_disconnect_timer_module;

app_timer_module_t turn_on_spo2_timer_module;
app_timer_module_t measure_spo2_timer_module;



/* Private function prototypes -----------------------------------------------*/
extern void idle_to_sleep_callback();
/* Private functions ---------------------------------------------------------*/
void turn_off_led(void *p_context)
{
    led_off((uint32_t)p_context);
    if((uint32_t)p_context == LED_BLUE)
    {
      timer_module_stop(&blue_led_off_timer_module);
    }
    else if ((uint32_t)p_context == LED_RED)
    {
      timer_module_stop(&red_led_off_timer_module);
    }
    else if ((uint32_t)p_context == LED_GREEN)
    {
      timer_module_stop(&green_led_off_timer_module);
    }
}

void lead_on_adv_start_timer_hander()
{
    signal_advertising = true;
}

void lead_off_detect_hander()
{
 //   signal_advertising = true;
}
void ble_disconnect_handler()
{
            NRF_LOG_INFO("---------------BLE Disconnect----------------------------");
sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
current_ble_state = BLE_STATE_ADVERTISING;
ble_disconnect_timer_flag = true;

}
void turn_on_spo2_handler()
{
            NRF_LOG_INFO("---------------turn on spo2 timer interrupt----------------------------");

turn_on_max_timer_flag = true;
}

void measure_spo2_handler()
{
            NRF_LOG_INFO("---------------turn off spo2 timer interrupt----------------------------");

send_spo2_hr_timer_flag = true;
}

/** @brief Function for initializing the timer module. */
void timers_init(void) {
  ret_code_t err_code = app_timer_init();
  APP_ERROR_CHECK(err_code);
  err_code = app_timer_create(&idle_to_sleep_timer, APP_TIMER_MODE_SINGLE_SHOT, idle_to_sleep_callback);
  APP_ERROR_CHECK(err_code);
  err_code = app_timer_create(&blue_led_off_timer, APP_TIMER_MODE_SINGLE_SHOT, turn_off_led);
  APP_ERROR_CHECK(err_code);
  err_code = app_timer_create(&red_led_off_timer, APP_TIMER_MODE_SINGLE_SHOT, turn_off_led);
  APP_ERROR_CHECK(err_code);
  err_code = app_timer_create(&green_led_off_timer, APP_TIMER_MODE_SINGLE_SHOT, turn_off_led);
  APP_ERROR_CHECK(err_code);
  err_code = app_timer_create(&ble_disconnect_timer, APP_TIMER_MODE_SINGLE_SHOT, ble_disconnect_handler);
  APP_ERROR_CHECK(err_code);
  err_code = app_timer_create(&turn_on_spo2_timer, APP_TIMER_MODE_SINGLE_SHOT, turn_on_spo2_handler);
  APP_ERROR_CHECK(err_code);
  err_code = app_timer_create(&measure_spo2_timer, APP_TIMER_MODE_SINGLE_SHOT, measure_spo2_handler);
  APP_ERROR_CHECK(err_code);

}

void timer_modules_init()
{
  timers_init();
  idle_to_sleep_timer_module.state = APP_TIMER_CREATED;
  idle_to_sleep_timer_module.id = idle_to_sleep_timer;
  idle_to_sleep_timer_module.interval = IDLE_TO_SLEEP_INTERVAL;
  idle_to_sleep_timer_module.context = NULL;

  blue_led_off_timer_module.state = APP_TIMER_CREATED;
  blue_led_off_timer_module.id = blue_led_off_timer;
  blue_led_off_timer_module.context = (void *)LED_BLUE;
  blue_led_off_timer_module.interval = LED_OFF_INTERVAL;
  red_led_off_timer_module.state = APP_TIMER_CREATED;
  red_led_off_timer_module.id = red_led_off_timer;
  red_led_off_timer_module.context = (void *)LED_RED;
  red_led_off_timer_module.interval = LED_OFF_INTERVAL;
  green_led_off_timer_module.state = APP_TIMER_CREATED;
  green_led_off_timer_module.id = green_led_off_timer;
  green_led_off_timer_module.context = (void *)LED_GREEN;
  green_led_off_timer_module.interval = LED_OFF_INTERVAL;

  ble_disconnect_timer_module.state = APP_TIMER_CREATED;
  ble_disconnect_timer_module.id = ble_disconnect_timer;
  ble_disconnect_timer_module.context = NULL;
  ble_disconnect_timer_module.interval = BLE_DISCONNECT_INTERVAL;

  turn_on_spo2_timer_module.state = APP_TIMER_CREATED;
  turn_on_spo2_timer_module.id = turn_on_spo2_timer;
  turn_on_spo2_timer_module.context = NULL;
  turn_on_spo2_timer_module.interval = TURN_ON_SPO2_INTERVAL;

  measure_spo2_timer_module.state = APP_TIMER_CREATED;
  measure_spo2_timer_module.id = measure_spo2_timer;
  measure_spo2_timer_module.context = NULL;
  measure_spo2_timer_module.interval = MEASURE_SPO2_INTERVAL;

}

void timer_module_run(app_timer_module_t *timer_mod,uint32_t period)
{
   if(timer_mod->state == APP_TIMER_RUNNING)
      return;
   if(period == NULL)
      period = timer_mod->interval;
   ret_code_t err_code = app_timer_start(timer_mod->id,period,timer_mod->context);
   APP_ERROR_CHECK(err_code);
   timer_mod->state = APP_TIMER_RUNNING;
}

void timer_module_stop(app_timer_module_t *timer_mod)
{
  if(timer_mod->state != APP_TIMER_RUNNING)
      return;
  ret_code_t err_code = app_timer_stop(timer_mod->id);
  APP_ERROR_CHECK(err_code);
  timer_mod->state = APP_TIMER_STOPPED;
}

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
