/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _TIMER_FUNCTIONS_H
#define _TIMER_FUNCTIONS_H

#include "app_timer.h"

/* Exported types ------------------------------------------------------------*/

typedef enum
{
   APP_TIMER_CREATED,
   APP_TIMER_STOPPED,
   APP_TIMER_RUNNING,
   APP_TIMER_PAUSED,
}app_timer_status_t;

typedef struct
{
  app_timer_status_t state;
  app_timer_id_t id;
  uint32_t interval;
  void * context;
}app_timer_module_t;

#define SECS_TO_MSECS(x)  (x * 1000L)
#define MINS_TO_MSECS(x)  (x * 60 * 1000L)

/* Exported constants --------------------------------------------------------*/
#define IDLE_TO_SLEEP_INTERVAL  APP_TIMER_TICKS(MINS_TO_MSECS(1))
#define LEAD_ON_ADV_START_INTERVAL APP_TIMER_TICKS(SECS_TO_MSECS(3))
#define LEAD_OFF_DETECT_INTERVAL APP_TIMER_TICKS(MINS_TO_MSECS(2))
#define LED_OFF_INTERVAL APP_TIMER_TICKS(100)
#define ADVERTISING_INDICATION_INTERVAL APP_TIMER_TICKS(1000)
#define BLE_DISCONNECT_INTERVAL APP_TIMER_TICKS(SECS_TO_MSECS(20))
#define MEASURE_SPO2_INTERVAL APP_TIMER_TICKS(SECS_TO_MSECS(10))
#define TURN_ON_SPO2_INTERVAL APP_TIMER_TICKS(SECS_TO_MSECS(30))

/* Exported macro ------------------------------------------------------------*/
APP_TIMER_DEF(idle_to_sleep_timer);
APP_TIMER_DEF(blue_led_off_timer);
APP_TIMER_DEF(red_led_off_timer);
APP_TIMER_DEF(green_led_off_timer);
APP_TIMER_DEF(lead_on_adv_start_timer);
APP_TIMER_DEF(lead_off_detect_timer);
APP_TIMER_DEF( ble_disconnect_timer);
APP_TIMER_DEF( turn_on_spo2_timer);
APP_TIMER_DEF( measure_spo2_timer);

extern app_timer_module_t ble_disconnect_timer_module;
extern app_timer_module_t idle_to_sleep_timer_module;
extern app_timer_module_t lead_on_adv_start_timer_module;
extern app_timer_module_t lead_off_detect_timer_module;  

extern app_timer_module_t blue_led_off_timer_module;
extern app_timer_module_t red_led_off_timer_module;
extern app_timer_module_t green_led_off_timer_module;

extern app_timer_module_t turn_on_spo2_timer_module;
extern app_timer_module_t measure_spo2_timer_module;

extern bool signal_advertising;
extern volatile bool send_spo2_hr_timer_flag;
extern volatile bool turn_on_max_timer_flag;
extern volatile bool ble_disconnect_timer_flag;
/* Exported functions ------------------------------------------------------- */
void timers_init(void);
void timer_module_stop(app_timer_module_t * timer_mod);
void timer_module_run(app_timer_module_t *timer_mod,uint32_t period);
void timer_modules_init();
void blink_handler(void *p_context);
#endif /* _TIMER_FUNCTIONS_H */