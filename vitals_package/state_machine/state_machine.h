/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_

#include <stdint.h>
#include "bio2board.h"
#include "usbd_ble_nus.h"
#include "packet_handler.h"
//#include "dfu_functions.h"
#include "data_manager.h"
#include "timer_functions.h"
#include "power_manage.h"
#include "battery.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
//#include "usb_functions.h"
#include "temperature.h"
#include "spo2.h"
#include "tmp_interface.h"
#include "nrf_delay.h"


#define BIO2_BOARD
/* Exported types ------------------------------------------------------------*/
#define STATE_STACK_SIZE 5

typedef enum
{
   DEVICE_STATE_UNKNOWN,
   DEVICE_STATE_IDLE,
   DEVICE_STATE_SLEEP,
   DEVICE_STATE_COMM,
   DEVICE_STATE_RECORDING,
   DEVICE_STATE_BLE_TRANSMIT,
   DEVICE_STATE_USB_SYNC,
}state_type_t;


typedef struct
{
  
}device_status_t;

typedef struct
{
   state_type_t device_state;
   ble_state_t ble_state;
   device_status_t device_status;
   uint8_t battery_level;
}state_t;
static char const * device_state_str[] = {
    "UNKNOWN",
    "IDLE",
    "SLEEP",
    "COMM",
    "RECORDING",
    "BLE TRANSMIT",
    "USB_SYNC",
};

static char const * lead_current_status_str[] = {
    "LEAD_STATUS_UNKNOWN",
    "LEAD_STATUS_ON",
    "LEAD_STATUS_OFF"
};
extern state_t device_state_stack[STATE_STACK_SIZE], current_state;
extern volatile uint8_t device_state_top;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void state_machine_run(void);

#endif /* _STATE_MACHINE_H_ */