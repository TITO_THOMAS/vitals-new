/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Includes
 * ------------------------------------------------------------------*/
#include "state_machine.h"
extern uint32_t read_file_index;
extern void prepare_for_sleep(void);


/* Private typedef
 * -----------------------------------------------------------*/
/* Private define
 * ------------------------------------------------------------*/
/* Private macro
 * -------------------------------------------------------------*/
/* Private variables
 * ---------------------------------------------------------*/
volatile bool battery_timer_expired = false;
volatile bool activity_timer_expired = false;
volatile bool ble_disconnect_timer_flag;
bool signal_advertising = false;


void check_flag();
volatile bool measure_spo2_hr = false;
volatile bool send_spo2_hr_timer_flag = false;
volatile bool turn_on_max_timer_flag = false;
static char const * ble_state_str[] = {
    "OFF",
    "ADVERSITING",
    "CONNECTED",
};

state_t current_state, last_state;
volatile uint8_t device_state_top;
state_type_t usb_saved_state = DEVICE_STATE_UNKNOWN;
static app_timer_status_t idle_to_sleep_timer_status;
volatile bool sleep_mode_active_cmd = false;
static volatile bool recording_paused = false;
volatile bool lead_off_recorded = false;
volatile bool lead_on_recorded = false;
static uint32_t tsLastReport=0;

/* Private function prototypes
 * -----------------------------------------------*/
void device_idle();
void device_sleep();
void device_comm();
void device_shut_down();
void check_state_change();
void previous_state();
void device_ble_transmit();
void device_ecg_record();
void handle_record_state_init();
void process_activity();
void handle_ble_transmit_init();
void handle_record_state_deinit();
void handle_lead_status_change();
void handle_ble_transmit_deinit();
void device_usb_sync();
void handle_usb_sync_init();
void handle_usb_state_change(bool * state_changed);
bool get_finger_status(void);


extern bool usb_sync;
extern bool usb_detect;
/* Private functions
 * ---------------------------------------------------------*/

void idle_to_sleep_callback()
{
    NRF_LOG_INFO("Idle to sleep timer expired. Going to sleep mode");
    sleep_mode_active_cmd = true;
}

void state_machine_run(void)
{
    check_state_change();
    
    switch (current_state.device_state)
    {
    
        case DEVICE_STATE_IDLE:
            device_idle();
            break;
        case DEVICE_STATE_SLEEP:
            device_sleep();
            break;
        case DEVICE_STATE_COMM:
            device_comm();
            break;
        case DEVICE_STATE_BLE_TRANSMIT:
            device_ble_transmit();
            break;
        case DEVICE_STATE_RECORDING:
            device_ecg_record();
            break;
        case DEVICE_STATE_USB_SYNC:
//            if (m_usb_connected)
//                device_usb_sync();
            break;
        default:
            break;
    }
}


void device_idle()
{

  
    static uint32_t temp_time=10000;
    static uint32_t pre_ir_value=0;
    NRF_LOG_INFO("----------idle------------------");
    timer_module_run(&idle_to_sleep_timer_module,IDLE_TO_SLEEP_INTERVAL);
//    if (signal_advertising && current_state.ble_state == BLE_STATE_OFF && !m_usb_connected)
//    {
//        signal_advertising = false;
//        advertising_start();
//        led_on(LED_GREEN);
//        led_on(LED_RED);
//        timer_module_run(&red_led_off_timer_module, ADVERTISING_INDICATION_INTERVAL);
//        timer_module_run(&green_led_off_timer_module, ADVERTISING_INDICATION_INTERVAL);
//        NRF_LOG_INFO("=====start idle to sleep timer========");
//        timer_module_stop(&idle_to_sleep_timer_module);
//        // nrf_delay_ms(10);
//        timer_module_run(&idle_to_sleep_timer_module, IDLE_TO_SLEEP_INTERVAL);
//    }
//    process_activity();
//    //            NRF_WDT->RR[0] = WDT_RR_RR_Reload;
//
/*
if(millis()-temp_time >5000)
{
// NRF_LOG_INFO("IRVAL=%d",get_current_ir());
 if(get_current_ir()>400){
  if((pre_ir_value - get_current_ir())<10){
//   NRF_LOG_INFO("preL=%d",pre_ir_value - get_current_ir());

    PulseOximeter_begin();
//    PulseOximeter_proxymode();
    }
   pre_ir_value = get_current_ir();

  }
temp_time = millis();
}
*/

    //power_manage();
}

void device_sleep()
{
   // accel_setup_double_tap();
    // Go to system-off mode (this function will not return; wakeup will cause a
    // reset).
    ret_code_t err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}

void device_comm()
{
    timer_module_stop(&idle_to_sleep_timer_module);
    process_incoming_packet();
//    process_outgoing();
    previous_state();
}

void check_state_change()
{
    bool state_changed = false;

    if(usb_sync)
    {
       usb_sync = false;

      if(nrf_gpio_pin_read(29))
           {

            battery_level_updated=true;
            usb_detect = true;
           //NRF_LOG_INFO("USB DETECT");
           }
      else
          {
            led_off(LED_GREEN);
            usb_detect = false;
           // NRF_LOG_INFO("USB REMOVED");
          }
    }


    if(alert_pin_interrupt == true)
    {
        alert_pin_interrupt=false;
         NRF_LOG_INFO("PUSH BUTTON PRESSED");
         package_alert_packet();
         ble_response_ready = true;
         process_outgoing();
         process_outgoing();
         process_outgoing();

    }
    if(ble_disconnect_timer_flag)
    {
    ble_disconnect_timer_flag = false;
    ble_view_stop_packet_received  =true;
    }
         
    if (ble_state_changed)
    {
        ble_state_changed = false;
        current_state.ble_state = current_ble_state;
        NRF_LOG_INFO("BLE State %s", ble_state_str[current_ble_state]);
    }
// 
////    if (fifo_wtm_interrupt)
////    {
////        NRF_LOG_INFO("max30101 fifo interrupt");
////        fifo_wtm_interrupt = false;
////    }
//    if (usb_state_changed)
//    {
//        usb_state_changed = false;
//        handle_usb_state_change(&state_changed);
//    }
    if (battery_timer_expired)
    {
        NRF_LOG_INFO("Sampling Battery");
        battery_timer_expired = false;
        battery_task_trigger();
    }
    if (battery_level_updated)
    {
        battery_level_updated = false;
        current_state.battery_level = get_battery_level();
        NRF_LOG_INFO("Battery level Updated= %d", current_state.battery_level);

        if(current_state.battery_level==100 && usb_detect==true)
          {
             NRF_LOG_INFO("usb_interrupt %d", usb_detect);
             led_on(LED_GREEN);
          }
    }
    if (new_packet_available())
    {
        memcpy(&last_state, &current_state, sizeof(state_t));
        current_state.device_state = DEVICE_STATE_COMM;
        state_changed = true;
  //      NRF_LOG_INFO("===New packet ====");

    }
    if (sleep_mode_active_cmd)// && current_state.device_state == DEVICE_STATE_IDLE)
    {
        NRF_LOG_INFO("+slp cmd true+");
        sleep_mode_active_cmd= false;
        memcpy(&last_state, &current_state, sizeof(state_t));
        current_state.device_state = DEVICE_STATE_SLEEP;
        prepare_for_sleep();
        state_changed = true;
    }
    if (current_state.device_state == DEVICE_STATE_IDLE && ble_view_start_packet_received == true)
    {
        handle_ble_transmit_init();
        // sd_ble_gap_disconnect(m_conn_handle,
        // BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        current_state.device_state = DEVICE_STATE_BLE_TRANSMIT;
        ble_view_start_packet_received = false;
        state_changed = true;
    }
  //  if (current_state.device_state == DEVICE_STATE_RECORDING &&
        if(ble_view_stop_packet_received == true)
        {
        ble_view_stop_packet_received = false;
        if(current_state.device_state == DEVICE_STATE_RECORDING && current_ble_state == BLE_STATE_CONNECTED){
          handle_record_state_deinit();
          current_state.device_state = DEVICE_STATE_BLE_TRANSMIT;
          NRF_LOG_INFO("++-------- graph stop & HR send-----++++++++++");
          }
        else{
          current_state.device_state = DEVICE_STATE_IDLE;
          handle_ble_transmit_deinit();
          NRF_LOG_INFO("++-------- STOP-----++++++++++");

          }
        state_changed = true;
    }
    if (ble_record_start_packet_received == true)
    {
        handle_record_state_init();
        current_state.device_state = DEVICE_STATE_RECORDING;
        ble_record_start_packet_received = false;
        state_changed = true;
    }
    if (current_state.device_state == DEVICE_STATE_RECORDING &&
        ble_record_stop_packet_received == true)
    {
        handle_record_state_deinit();
        ble_record_stop_packet_received = false;
        current_state.device_state = DEVICE_STATE_IDLE;
        state_changed = true;
    }



    if (state_changed)
    {
        NRF_LOG_INFO("State Changed %s ---------> %s", device_state_str[last_state.device_state],
            device_state_str[current_state.device_state]);
    }
}
//
void previous_state()
{
    if (current_state.device_state == last_state.device_state)
        return;
    state_type_t temp_state = current_state.device_state;
    current_state.device_state = last_state.device_state;
    last_state.device_state = temp_state;
    NRF_LOG_INFO("State Changed %s ---------> %s", device_state_str[last_state.device_state],
        device_state_str[current_state.device_state]);
}

void device_ble_transmit() {
  static uint32_t send_hr_count = 0;
    static uint32_t temp_time    = 0;
  // TODO
  // read the spo2 and temperature for 10 seconds
  if (measure_spo2_hr) {
//    PulseOximeter_update();
    spo2_measure();
    temperature_measure();
  if (millis() - temp_time >2000) {
//    if (get_finger_status()) {
    package_hr_spo2();
    ble_response_ready = true;
    process_outgoing();
    timer_module_stop(&ble_disconnect_timer_module);
    NRF_LOG_INFO("Finger detected");
    timer_module_run(&measure_spo2_timer_module,MEASURE_SPO2_INTERVAL);

//    } 
//    else {
//      timer_module_stop(&measure_spo2_timer_module);
//      timer_module_run(&ble_disconnect_timer_module, BLE_DISCONNECT_INTERVAL);
//      NRF_LOG_INFO("No finger detected");
//
//    }
    temp_time = millis();
    }
  }

  // send data after 10 seconds of data measurement
  if (send_spo2_hr_timer_flag == true) {
    send_spo2_hr_timer_flag = false;
    measure_spo2_hr = false;
    spo2_off();
    timer_module_stop(&turn_on_spo2_timer_module);
    timer_module_run(&turn_on_spo2_timer_module, TURN_ON_SPO2_INTERVAL);
    NRF_LOG_INFO("sending HR, turn off and start timer");
    timer_module_stop(&ble_disconnect_timer_module);
  }
  // turn on the max module to start measuring
  if (turn_on_max_timer_flag == true) {
    turn_on_max_timer_flag = false;
    handle_ble_transmit_init();
    NRF_LOG_INFO("-------------------------------------turn on");
  }
  if (current_ble_state != BLE_STATE_CONNECTED) {
    spo2_off();
    ble_view_stop_packet_received = true;
  }
  power_manage();
}

bool get_finger_status(void) {
  static uint32_t pre_ir_value = 0;
  bool finger_status=0;
    if (get_current_ir() > 400) {
      if ((pre_ir_value - get_current_ir()) < 10) {
        //   NRF_LOG_INFO("preL=%d",pre_ir_value - get_current_ir());
        finger_status = 0;
        PulseOximeter_begin();
        //    PulseOximeter_proxymode();
      }
      else
      {
        finger_status=1;
      }
      pre_ir_value = get_current_ir();
    }
 return finger_status;
}


void device_ecg_record()
{
 static uint32_t temp_time = 0;
  if (measure_spo2_hr) {
//    PulseOximeter_update();
    spo2_measure();
    temperature_measure();
  if (millis() - temp_time >2000) {
//    if (get_finger_status()) {
    package_hr_spo2();
    ble_response_ready = true;
    process_outgoing();
    timer_module_stop(&ble_disconnect_timer_module);
    NRF_LOG_INFO("Finger detected");
    timer_module_run(&measure_spo2_timer_module,MEASURE_SPO2_INTERVAL);

//    } else {
//      timer_module_stop(&measure_spo2_timer_module);
//      timer_module_run(&ble_disconnect_timer_module, BLE_DISCONNECT_INTERVAL);
//      NRF_LOG_INFO("No finger detected");
//
//    }
    temp_time = millis();
    }
  }

    if (current_ble_state != BLE_STATE_CONNECTED){
//        spo2_off();
        ble_view_stop_packet_received = true;
        }

}

void handle_record_state_init()
{

    if (current_state.device_state == DEVICE_STATE_BLE_TRANSMIT)
    {
        // TODO start timer to turn off BLE in one minute
   //     timer_module_run(&ble_disconnect_timer_module, BLE_DISCONNECT_INTERVAL);
        NRF_LOG_INFO("--------------turn off ble timer start-------");
    }
    spo2_read_active = true;
    spo2_off();
    nrf_delay_ms(100);
  spo2_on();
  nrf_delay_ms(100);
  led_on(LED_BLUE);
  nrf_delay_ms(200);
  led_off(LED_BLUE);
  measure_spo2_hr = true;      
  
}

void process_activity()
{

}

void handle_ble_transmit_init()
{

  timer_module_stop(&measure_spo2_timer_module);
  timer_module_stop(&ble_disconnect_timer_module);
  timer_module_stop(&turn_on_spo2_timer_module);

    spo2_off();
    nrf_delay_ms(100);
  spo2_on();
  nrf_delay_ms(100);  measure_spo2_hr = true;  
    led_on(LED_BLUE);
  nrf_delay_ms(200);
  led_off(LED_BLUE);
 timer_module_run(&measure_spo2_timer_module,MEASURE_SPO2_INTERVAL);


}

void handle_record_state_deinit()
{
  //  ecg_read_active = false;
    spo2_read_active = false;
}

void handle_ble_transmit_deinit()
{
//    ecg_read_active = false;
//    disable_ecg_fifo_interrupt();
//    accel_disable_fifo();
    spo2_read_active = false;
    spo2_off();

}

void handle_lead_status_change()
{
//    switch (current_state.device_state)
//    {
//        case DEVICE_STATE_IDLE:
//            if (current_state.ble_state == BLE_STATE_OFF)
//            {
////                if (lead_current_status == LEAD_STATUS_ON)
////                {
////                    timer_module_run(&lead_on_adv_start_timer_module, NULL);
////                    NRF_LOG_INFO("==============start adv timer=================");
////                }
////                else
////                {
////                    timer_module_stop(&lead_on_adv_start_timer_module);
////                    NRF_LOG_INFO("==============stop adv timer=================");
////                    //           NRF_WDT->RR[0] = WDT_RR_RR_Reload;
////                }
//            }
//            break;
//        case DEVICE_STATE_BLE_TRANSMIT:
//            break;
//        case DEVICE_STATE_COMM:
//            break;
//        case DEVICE_STATE_RECORDING:
////            if (lead_current_status == LEAD_STATUS_OFF && device_flags.ecg_record_request_received)
////            {
////                NRF_LOG_INFO("Recording Paused");
////
////                recording_paused = true;
////                lead_off_recorded = false;
////            }
////            else if (lead_current_status == LEAD_STATUS_ON &&
////                     device_flags.ecg_record_request_received)
////            {
////                NRF_LOG_INFO("Recording Resumed");
////
////                recording_paused = false;
////                lead_on_recorded = false;
////                lead_off_recorded = false;
////            }
////            else
////                ;
//            break;
//        case DEVICE_STATE_USB_SYNC:
//            break;
//        default:
//            break;
//    }
}

void handle_usb_sync_init()
{
    /*TODO check as per current status
        Send lis and ecg to sleep mode
        */
//    files_available = true;
//    file_packet_count = 0;
//    read_file_index = 0;
}

void device_usb_sync()
{
    //    if (current_user.otg_upload_flag)
    //        return;
//    if (files_available == false)
//        return;
//    bool be_ack;
//    bool sync_success = true;
//    do
//    {
//        be_ack = send_be_get_ack(USB_ACK_DEFAULT_RETRY, USB_ACK_DEFAULT_DELAY);
//        if (!be_ack)
//            return;
//        NRF_LOG_INFO("BE Ack Success");
//        sync_success = sync_file();
//        if (!sync_success)
//            return;
//        NRF_LOG_INFO("File Sync Success");
//    } while (m_usb_connected && files_available);
//    if (!files_available)
//        set_otg_upload_status(true);
}

void handle_usb_state_change(bool * state_changed)
{
//    if (m_usb_connected)
//    {
//        // Power down max, lis, and stop advertising
//        // Stop recording, view
//        // stop idle to sleep timer
//        NRF_LOG_INFO("=====USB state change: Connected=====");
//
//        NRF_LOG_INFO("=====stop idle to sleep timer=====");
//        timer_module_stop(&idle_to_sleep_timer_module);
//        NRF_LOG_INFO("=====stop lead on timer=====");
//        timer_module_stop(&lead_on_adv_start_timer_module);
//        if (current_state.device_state == DEVICE_STATE_BLE_TRANSMIT)
//        {
//            handle_ble_transmit_deinit();
//            usb_saved_state = DEVICE_STATE_BLE_TRANSMIT;
//            *state_changed = true;
//            current_state.device_state = DEVICE_STATE_IDLE;
//        }
//        else if (current_state.device_state == DEVICE_STATE_RECORDING)
//        {
//            handle_record_state_deinit();
//            usb_saved_state = DEVICE_STATE_RECORDING;
//            *state_changed = true;
//            current_state.device_state = DEVICE_STATE_IDLE;
//        }
//        else
//            ;
//        if (current_state.ble_state == BLE_STATE_CONNECTED ||
//            current_state.ble_state == BLE_STATE_ADVERTISING)
//        {
//            ble_disconnect();
//            advertising_stop();
//        }
////        accel_power_down();
////        ecg_power_down();
//        activity_send = false;
//    }
//    else
//    {
//        NRF_LOG_INFO("=====USB state change: removed=====");
////        accel_init();
////        ecg_init();
//        //     advertising_start();
//        if (usb_saved_state != DEVICE_STATE_UNKNOWN)
//        {
//            *state_changed = true;
//            if (usb_saved_state == DEVICE_STATE_BLE_TRANSMIT)
//                ble_view_start_packet_received = true;
//            else if (usb_saved_state == DEVICE_STATE_RECORDING)
//                ble_record_start_packet_received = true;
//            else
//                ;
//            usb_saved_state = DEVICE_STATE_UNKNOWN;
//        }
//
//        if (current_state.device_state == DEVICE_STATE_USB_SYNC)
//        {
//            *state_changed = true;
//            current_state.device_state = DEVICE_STATE_IDLE;
//        }
//    }
}
/**
 * @brief  Main program
 * @param  None
 * @retval None
 */