/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LED_FUNCTIONS_H
#define _LED_FUNCTIONS_H

#include "bio2board.h"
#include "timer_functions.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void battery_led_handler(void);
void handle_leds_every_second(void);
#endif /* _LED_FUNCTIONS_H */