/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "led_functions.h"
#include "state_machine.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define IDLE_LED_BLINK_INTERVAL_S 5
#define CHARGING_LED_BLINK_INTERVAL_S 3
#define DEVICE_DETAILS_LOG_INTERVAL_S 3
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
static uint32_t blue_led_periodic_counter = 0;
static uint32_t red_led_periodic_counter = 0;
static uint32_t device_details_log_counter = 0;
extern usb_detect;

//Blink blue led as per battery when connected to application or recording
void battery_led_handler(void)
{
    if (current_state.device_state == DEVICE_STATE_BLE_TRANSMIT || current_state.device_state == DEVICE_STATE_RECORDING) 
    {
        led_on(BATTERY_LED);
        timer_module_run(&blue_led_off_timer_module,NULL);
    }
}

void handle_leds_every_second(void)
{

      if(usb_detect && current_state.battery_level!=100)// && current_state.battery_level!=100
     {
        
        led_on(LED_RED);
        timer_module_run(&red_led_off_timer_module, NULL);
      //  NRF_LOG_INFO("USB CONNECTED");
     }

  blue_led_periodic_counter++;
//    red_led_periodic_counter++;
//    //Turn on blue led every 5 seconds if device is idle and not charging
    if (blue_led_periodic_counter % IDLE_LED_BLINK_INTERVAL_S == 0 && current_state.device_state == DEVICE_STATE_IDLE && !usb_detect) 
    {
        led_on(LED_BLUE);
        timer_module_run(&blue_led_off_timer_module, NULL);
    }
//    //Turn on red led every 3 seconds if battery is charging
//    else if (red_led_periodic_counter % CHARGING_LED_BLINK_INTERVAL_S == 0 && m_usb_connected && current_state.battery_level < 100 && current_state.device_state != DEVICE_STATE_USB_SYNC) 
//    {
//        led_on(LED_RED);
//        timer_module_run(&red_led_off_timer_module, NULL);
//    }
//    //Turn on Green led if battery is fully charged
//    else if (m_usb_connected && current_state.battery_level == 100) 
//    {
//        led_on(LED_GREEN);
//    } 
//    else
//        ;
//    //Blink blue led every 1 second if transmitting via usb
//    if (current_state.device_state == DEVICE_STATE_USB_SYNC || clearing_user_data) 
//    {
//        led_on(LED_BLUE);
//        timer_module_run(&blue_led_off_timer_module, NULL);
//    }
//    //If lead of is detected when recording, blink red led, once every second
//    else if (current_state.device_state == DEVICE_STATE_RECORDING && !m_usb_connected) 
//    {
//        led_on(LED_RED);
//        timer_module_run(&red_led_off_timer_module, NULL);
//
//    } 
//    else
//        ;
//
//device_details_log_counter++;
//   if(device_details_log_counter % DEVICE_DETAILS_LOG_INTERVAL_S == 0){
//       device_details_log_counter=0;
//       // CDS: current device state; LCS : Lead current status
//        NRF_LOG_INFO("CDS: %s, USB: %d BL: %d",  device_state_str[current_state.device_state],  m_usb_connected, current_state.battery_level); // checking the state every second
//   }
}
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */