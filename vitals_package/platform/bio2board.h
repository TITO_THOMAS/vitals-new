/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _BIO2_BOARD_H_
#define _BIO2_BOARD_H_

#include "nrf_gpio.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define LED_BLUE        22
#define LED_RED         23
#define LED_GREEN       24

#define LIS_MISO_SDA  NRF_GPIO_PIN_MAP(1, 7)
#define LIS_MOSI      26
#define LIS_SCK       NRF_GPIO_PIN_MAP(1, 5)
#define LIS_CS        27
#define LIS_PWR       NRF_GPIO_PIN_MAP(1, 6)
#define LIS_INT1      28
#define LIS_INT2      29


#define MAX_MOSI        20
#define MAX_MISO        19
#define MAX_SCK         22
#define MAX_SS          24
#define MAX_INT1        17
#define MAX_INT2        21

#define TPS_CRTL        30

#define led_on   nrf_gpio_pin_set
#define led_off  nrf_gpio_pin_clear
#define led_toggle nrf_gpio_pin_toggle

#define MINS_TO_SECS(x)  (x * 60)

#define BATTERY_LED   LED_BLUE
/* Exported functions ------------------------------------------------------- */

typedef struct
{
   bool ble_user_request_received;
   bool usb_user_request_receieved;
   bool flash_storage_full;
   bool ecg_record_request_received;
}flags_t;
#endif /* _BIO2_BOARD_H_ */