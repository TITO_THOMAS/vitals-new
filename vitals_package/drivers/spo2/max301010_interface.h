#ifndef MAX301010_INTERFACE_H_
#define MAX301010_INTERFACE_H_

#include "MAX30102.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include <string.h>
#define BUTTON_INT 20
#define MAX30101_INT 12

extern volatile bool alert_pin_interrupt;
extern ret_code_t max30101_gpiote_init();
extern void max30101_power_off(void);
extern void max30101_power_on(void);
extern ret_code_t max30101_gpiote_unint(void);
extern void prepare_for_sleep(void);
extern void wakeup_pin_init(void);


#endif /* MAX30102_H_ */