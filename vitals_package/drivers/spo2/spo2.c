#include "spo2.h"
#include "nrf_delay.h"


uint32_t aun_ir_buffer[500]; //IR LED sensor data
int32_t n_ir_buffer_length;    //data length
uint32_t aun_red_buffer[500];    //Red LED sensor data
int32_t n_sp02; //SPO2 value
int8_t ch_spo2_valid;   //indicator to show if the SP02 calculation is valid
int32_t n_heart_rate;   //heart rate value
int8_t  ch_hr_valid;    //indicator to show if the heart rate calculation is valid
uint8_t uch_dummy;

uint32_t un_min, un_max, un_prev_data;  //variables to calculate the on-board LED brightness that reflects the heartbeats
int i;
int32_t n_brightness;
float f_temp;
static float current_spo2=0;
static int current_hr=0;
void spo2_init(void);
void spo2_measure(void);
int get_current_hr(void);
float get_current_spo2(void);
void spo2_off(void);
void spo2_on(void);
extern void max30101_twi_init(void);
extern void max30101_gpio_init(void);
extern bool maxim_max30102_write_reg(uint8_t reg, uint8_t p_content);
extern bool maxim_max30102_read_reg(uint8_t reg, uint8_t * p_content);
extern void max30101_power_off(void);
extern void max30101_power_on(void);
extern ret_code_t max30101_gpiote_init();

volatile bool spo2_data_ready = false;
volatile bool spo2_read_active = false;

/**
* \brief        Write a value to a MAX30102 register
* \par          Details
*               This function writes a value to a MAX30102 register
*
* \param[in]    uch_addr    - register address
* \param[in]    uch_data    - register data
*
* \retval       true on success
*/
void spo2_init(void)
{
    max30101_gpio_init();
    
    max30101_gpiote_init();
    //nrf_delay_ms(10);
    max30101_twi_init();
   // nrf_delay_ms(10);
    maxim_max30102_reset(); //resets the MAX30102
    //read and clear status register
    maxim_max30102_read_reg(0,&uch_dummy);
    maxim_max30102_init();
//    PulseOximeter_begin();
    spo2_off();


}
void spo2_on(void)
{
max30101_power_on();
nrf_delay_ms(200);
maxim_max30102_reset(); //resets the MAX30102
//read and clear status register
maxim_max30102_read_reg(0,&uch_dummy);
maxim_max30102_init();

//PulseOximeter_begin();
//    spo2_init();

    un_min=0x3FFFF;
    un_max=0;
  
    n_ir_buffer_length=500; //buffer length of 100 stores 5 seconds of samples running at 100sps
    
    //read the first 500 samples, and determine the signal range
    for(i=0;i<n_ir_buffer_length;i++)
    {
        while(nrf_gpio_pin_read(MAX30101_INT)==1);   //wait until the interrupt pin asserts
        
        maxim_max30102_read_fifo((aun_red_buffer+i), (aun_ir_buffer+i));  //read from MAX30102 FIFO
            
        if(un_min>aun_red_buffer[i])
            un_min=aun_red_buffer[i];    //update signal min
        if(un_max<aun_red_buffer[i])
            un_max=aun_red_buffer[i];    //update signal max
      //  NRF_LOG_INFO("red=%d,ir=%d",aun_red_buffer[i],aun_ir_buffer[i]);
    }
    un_prev_data=aun_red_buffer[i];
    
    
    //calculate heart rate and SpO2 after first 500 samples (first 5 seconds of samples)
    maxim_heart_rate_and_oxygen_saturation(aun_ir_buffer, n_ir_buffer_length, aun_red_buffer, &n_sp02, &ch_spo2_valid, &n_heart_rate, &ch_hr_valid); 
    NRF_LOG_INFO("HR=%d,HR valid=%d\r\n,SpO2=%d,SPO2Valid=%d",n_heart_rate,ch_hr_valid,n_sp02,ch_spo2_valid);


nrf_delay_ms(100);

}
void spo2_off(void)
{
max30101_power_off();
}

void spo2_measure(void)
{

        i=0;
        un_min=0x3FFFF;
        un_max=0;
        
        //dumping the first 100 sets of samples in the memory and shift the last 400 sets of samples to the top
        for(i=100;i<500;i++)
        {
            aun_red_buffer[i-100]=aun_red_buffer[i];
            aun_ir_buffer[i-100]=aun_ir_buffer[i];
            
            //update the signal min and max
            if(un_min>aun_red_buffer[i])
            un_min=aun_red_buffer[i];
            if(un_max<aun_red_buffer[i])
            un_max=aun_red_buffer[i];
        }
        
        //take 100 sets of samples before calculating the heart rate.
        for(i=400;i<500;i++)
        {
            un_prev_data=aun_red_buffer[i-1];
        while(nrf_gpio_pin_read(MAX30101_INT)==1);   //wait until the interrupt pin asserts
        maxim_max30102_read_fifo((aun_red_buffer+i), (aun_ir_buffer+i));
        
     //   NRF_LOG_INFO("red=%d,ir=%d",aun_red_buffer[i],aun_ir_buffer[i]);
     //   NRF_LOG_INFO("HR=%d,HRvalid=%d\r\n,SpO2=%d,SPO2Valid=%d",n_heart_rate,ch_hr_valid,n_sp02,ch_spo2_valid);
            //send samples and calculation result to terminal program through UART
        }
        maxim_heart_rate_and_oxygen_saturation(aun_ir_buffer, n_ir_buffer_length, aun_red_buffer, &n_sp02, &ch_spo2_valid, &n_heart_rate, &ch_hr_valid);
        current_spo2 = n_sp02;
        current_hr = n_heart_rate;
        NRF_LOG_INFO("HR=%d,HR valid=%d\r\n,SpO2=%d,SPO2Valid=%d",n_heart_rate,ch_hr_valid,n_sp02,ch_spo2_valid);

}
float get_current_spo2(void)
{
return current_spo2;
}

int get_current_hr(void)
{
return current_hr;
}

