#ifndef SPO2_H_
#define SPO2_H_

#include "MAX30102.h"
#include "MAX301010_interface.h"
#include "algorithm.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"
#include <stdlib.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#define MAX_BRIGHTNESS 255
extern void spo2_off(void);
extern void spo2_on(void);
extern void spo2_init(void);
extern void spo2_measure(void);
extern int get_current_hr(void);
extern float get_current_spo2(void);
//extern void max30101_twi_init(void);
//extern void max30101_gpio_init(void);
//extern bool maxim_max30102_write_reg(uint8_t reg, uint8_t p_content);
//extern bool maxim_max30102_read_reg(uint8_t reg, uint8_t * p_content);
//extern void max30101_power_off(void);
//extern void max30101_power_on(void);
extern volatile bool alert_pin_interrupt;
extern volatile bool spo2_data_ready;
extern volatile bool spo2_read_active;



#endif /* MAX30102_H_ */