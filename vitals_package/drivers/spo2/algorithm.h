
#ifndef ALGORITHM_H_
#define ALGORITHM_H_
#include <stdlib.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "MAX30102.h"

#define true 1
#define false 0
#define FS 100
#define BUFFER_SIZE  (FS* 5) 
#define HR_FIFO_SIZE 7
#define MA4_SIZE  4 // DO NOT CHANGE
#define HAMMING_SIZE  5// DO NOT CHANGE
#define min(x,y) ((x) < (y) ? (x) : (y))

static  int32_t an_dx[ BUFFER_SIZE-MA4_SIZE]; // delta
static  int32_t an_x[ BUFFER_SIZE]; //ir
static  int32_t an_y[ BUFFER_SIZE]; //red

extern void maxim_heart_rate_and_oxygen_saturation(uint32_t *pun_ir_buffer ,  int32_t n_ir_buffer_length, uint32_t *pun_red_buffer ,   int32_t *pn_spo2, int8_t *pch_spo2_valid ,  int32_t *pn_heart_rate , int8_t  *pch_hr_valid);
extern void maxim_find_peaks( int32_t *pn_locs, int32_t *pn_npks,  int32_t *pn_x, int32_t n_size, int32_t n_min_height, int32_t n_min_distance, int32_t n_max_num );
extern void maxim_peaks_above_min_height( int32_t *pn_locs, int32_t *pn_npks,  int32_t *pn_x, int32_t n_size, int32_t n_min_height );
extern void maxim_remove_close_peaks( int32_t *pn_locs, int32_t *pn_npks,   int32_t  *pn_x, int32_t n_min_distance );
extern void maxim_sort_ascend( int32_t *pn_x, int32_t n_size );
extern void maxim_sort_indices_descend(  int32_t  *pn_x, int32_t *pn_indx, int32_t n_size);

//////////////////////////////////////////////////////////////
extern uint32_t millis(void);
extern void PulseOximeter_begin(void);
extern void PulseOximeter_shutdown(void);
extern void PulseOximeter_proxymode(void);
extern void begin(void);
extern void setMode(enum Mode mode);
extern void setLedsPulseWidth(enum LEDPulseWidth ledPulseWidth);
extern void setSamplingRate(enum SamplingRate samplingRate);
extern void Setspo2adcRange(uint8_t adcrange);
extern void setLedCurrentIR(enum LEDCurrent irLedCurrent, uint8_t address);
extern void setLedCurrentRED(enum LEDCurrent redLedCurrent, uint8_t address);	
extern void setLedCurrentGREEN(enum LEDCurrent greenLedCurrent, uint8_t address);
//Proximity Mode 			
extern void setLedProximity_ModePA(enum LEDCurrent ProximityPA, uint8_t address);																						 
extern void checkfordataready(void);																						 
//void setHighresModeEnabled(bool enabled);
extern void readMaxFIFO(void);
extern void SetMaxInterrupt(uint8_t interrupt_mode);
extern void SetMaxInterruptForTemp(uint8_t interrupt_mode);
extern void SetMaxFIFOConfig(uint8_t fifo_mode);	
extern void SetMaxMultiModeTimeSlot(uint8_t ledTimeSlot,uint8_t address );	
extern void SetMaxProximityThreshold(uint8_t Threshold ,uint8_t address );																							 
extern void PulseOximeter_update(void);
extern void checkSample(void);
extern void update(void);
extern bool checkForBeat(float sample_data);
extern void decreaseThreshold(void);
extern bool addSample(float sample);
extern float BeatDetector_getRate(void);
extern float getCurrentThreshold(void);
extern float LPFstep(float x);
extern float DCremoverstep(float x);
extern void SpO2Calculator_update(float irACValue, float redACValue, bool beatDetected);
extern void SpO2Calculator_reset(void);
extern float SpO2Calculator_getSpO2(void);
///////////////////////////////////////////
extern uint32_t get_current_ir(void);
extern uint32_t get_current_red(void);
extern float get_current_pi_ir(void);
extern float get_current_pi_red(void);


#endif /* ALGORITHM_H_ */