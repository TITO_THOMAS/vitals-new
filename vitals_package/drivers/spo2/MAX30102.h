#ifndef MAX30102_H_
#define MAX30102_H_

#include <stdlib.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_drv_twi.h"
#define MAX30101_DEVICE_ID 0x57
#define MAX30101_SCL 9
#define MAX30101_SDA 7

#define I2C_WRITE_ADDR 0xAE
#define I2C_READ_ADDR 0xAF

//register addresses
#define REG_INTR_STATUS_1 0x00
#define REG_INTR_STATUS_2 0x01
#define REG_INTR_ENABLE_1 0x02
#define REG_INTR_ENABLE_2 0x03
#define REG_FIFO_WR_PTR 0x04
#define REG_OVF_COUNTER 0x05
#define REG_FIFO_RD_PTR 0x06
#define REG_FIFO_DATA 0x07
#define REG_FIFO_CONFIG 0x08
#define REG_MODE_CONFIG 0x09
#define REG_SPO2_CONFIG 0x0A
#define REG_LED1_PA 0x0C
#define REG_LED2_PA 0x0D
#define REG_PILOT_PA 0x10
#define REG_MULTI_LED_CTRL1 0x11
#define REG_MULTI_LED_CTRL2 0x12
#define REG_TEMP_INTR 0x1F
#define REG_TEMP_FRAC 0x20
#define REG_TEMP_CONFIG 0x21
#define REG_PROX_INT_THRESH 0x30
#define REG_REV_ID 0xFE
#define REG_PART_ID 0xFF
#define SPO2_SAMPLE_LEN 3
#define MAX_FIFO_SIZE 32
typedef struct
{
    uint8_t samples[SPO2_SAMPLE_LEN];
    bool sample_valid;
} spo2_data_t;
extern spo2_data_t spo2_read_data[MAX_FIFO_SIZE];
extern uint8_t num_samples, ble_ecg_transmit_index;
extern bool maxim_max30102_init();
extern bool maxim_max30102_read_fifo(uint32_t *pun_red_led, uint32_t *pun_ir_led);
extern bool maxim_max30102_reset(void);
extern bool maxim_max30102_write_reg(uint8_t reg, uint8_t p_content);
extern bool maxim_max30102_read_reg(uint8_t reg, uint8_t *p_content);
extern bool readFifoData(uint32_t *pun_red_led, uint32_t *pun_ir_led);



/*
///////////////////////////////////////////////////////////////////////////////////
*/
						// INTERRUPT_STATUS REGISTER
#define MAX30101_REG_INTERRUPT_STATUS_1          	 0x00         // Interrupt status register 1 Address(RO) 
#define MAX30101_REG_INTERRUPT_STATUS_2          	 0x01    			// Interrupt status register 2 Address(RO)
						
#define MAX30101_INT_RDY                         0xF0		// interrupt Ready Flag

#define MAX30101_IS_PWR_RDY                     (1 << 0)				// Power Ready Flag
#define MAX30101_IS_PROX_INT                    (1 << 4)				// The proximity interrupt is triggered when the proximity threshold is reached, and SpO2/HR mode has begIn.
#define MAX30101_IS_ALC_OVF                     (1 << 5)				// Ambient Light Cancellation Overflow
#define MAX30101_IS_PPG_RDY                      0x40   // (1 << 6)				// In SpO2 and HR modes, this interrupt triggers when there is a new sample in the data FIFO
#define MAX30101_IS_A_FULL                      (1 << 7)				// FIFO Almost Full Flag
#define MAX30101_IS_DIE_TEMP_RDY                (1 << 1)				// Internal Temperature Ready Flag
						// INTERRUPT_ENABLE REGISTER   
#define MAX30101_REG_INTERRUPT_ENABLE_1            0x02         // Interrupt enable register 1 Address
#define MAX30101_REG_INTERRUPT_ENABLE_2            0x03         // Interrupt enable register 2 Address

#define MAX30101_IE_ENB_PROX_INT                (1 << 4)				// Interrupt enable for proximity
#define MAX30101_IE_ENB_ALC_OVF                 (1 << 5)				// Interrupt enable for Ambient Light Cancellation
#define MAX30101_IE_ENB_PPG_RDY                 (1 << 6)				// Interrupt enable for data
#define MAX30101_IE_ENB_A_FULL                  (1 << 7)				// Interrupt enable for FIFO Ready
#define MAX30101_IE_DIE_TEMP_RDY                (1 << 1)				// Internal Temperature Ready Flag

					   // FIFO control and data registers Address
#define MAX30101_REG_FIFO_WRITE_POINTER          0x04           // FIFO Write Pointer Address
#define MAX30101_REG_FIFO_OVERFLOW_COUNTER       0x05						// FIFO Overflow Counter Address
#define MAX30101_REG_FIFO_READ_POINTER           0x06						// FIFO Read Pointer Address
#define MAX30101_REG_FIFO_DATA                   0x07           // FIFO Data Address	

						 // FIFO CONFIGURATION REGISTER
#define MAX30101_REG_FIFO_CONFIGURATION          0x08           // Mode Configuration register Address 
#define FIFO_CONFIG_MODE 												 0x7F
					   // MODE CONFIGURATION REGISTER
#define MAX30101_REG_MODE_CONFIGURATION          0x09           // Mode Configuration register Address             
#define MAX30101_MC_RESET                       (1 << 6)				// Reset Control (RESET)
#define MAX30101_MC_SHDN                        (1 << 7)				// Shutdown Control(SHDN) power-save mode by setting this bit to one.
					 
					 // SpO2 CONFIGURATION REGISTER
#define MAX30101_REG_SPO2_CONFIGURATION          0x0A           // SpO2 Configuration register Address

					  // LED PULSE AMPLITUDE	
#define MAX30101_REG_LED_1_PA        						 0x0C         	// LED Configuration register Address
#define MAX30101_REG_LED_2_PA        						 0x0D         	// LED Configuration register Address
#define MAX30101_REG_LED_3_PA        						 0x0E         	// LED Configuration register Address

						//Proximity Mode LED Pulse Amplitude
#define MAX30101_LED_PILOT_PA       						 0x10         	// LED Proximity Mode LED Pulse Amplitude

						//Multi-LED Mode Control Registers
#define MAX30101_LED_MODE_CONTROL_REG_1      		 0x11         	// Multi-LED Mode Control Registers
#define MAX30101_LED_MODE_CONTROL_REG_2      		 0x12         	// Multi-LED Mode Control Registers

#define TimeSlot1 																 0x21
#define TimeSlot2 																 0x01

						//DIE TEMPERATURE
#define MAX30101_Die_Temp_Integer_Reg						 0x1F
#define MAX30101_Die_Temp_Fraction_Reg					 0x20
#define MAX30101_Die_Temperature_Config_Reg      0x21

						//PROXIMITY FUNCTION
#define MAX30101_Proximity_Interrupt_Threshold   0x30
#define Proxi_Threshold													 0x01


						// PART ID	
#define MAX30101_REG_REVISION_ID                 0xFE            		// Revision ID register (RO)
#define MAX30101_REG_PART_ID                     0xFF             	// Part ID register

					  // BEATDETECTOR 	
#define BEATDETECTOR_INIT_HOLDOFF                2000            		// in ms, how long to wait before counting
#define BEATDETECTOR_MASKING_HOLDOFF             200            		// in ms, non-retriggerable window after beat detection
#define BEATDETECTOR_BPFILTER_ALPHA              0.6             		// EMA factor for the beat period value
#define BEATDETECTOR_MIN_THRESHOLD               20//3000//1600//800             		// minimum threshold (filtered) value
#define BEATDETECTOR_MAX_THRESHOLD               800//30000//130000//2000             		// maximum threshold (filtered) value
#define BEATDETECTOR_STEP_RESILIENCY             30//10//30              		// maximum negative jump that triggers the beat edge
#define BEATDETECTOR_THRESHOLD_FALLOFF_TARGET    0.3             		// thr chasing factor of the max value when beat
#define BEATDETECTOR_THRESHOLD_DECAY_FACTOR      0.99            		// thr chasing factor when no beat
#define BEATDETECTOR_INVALID_READOUT_DELAY       2000            		// in ms, no-beat time to cause a reset
#define BEATDETECTOR_SAMPLES_PERIOD              10              		// in ms, 1/Fs

#define DEFAULT_MODE                            MAX30101_MODE_HRONLY
#define SPO2_MODE                               MAX30101_MODE_SPO2_HR
#define Multi_LED_mode                          MAX30101_Multi_LED_mode
#define DEFAULT_SAMPLING_RATE                   MAX30101_SAMPRATE_100HZ//MAX30101_SAMPRATE_100HZ
#define DEFAULT_ADC_RANGE												0x03
#define PULSE_WIDTH_18_BIT											0x03//MAX30100_SPC_PW_411US_18BITS
#define DEFAULT_PULSE_WIDTH                     MAX30100_SPC_PW_411US_18BITS// MAX30100_SPC_PW_411US_18BITS     
#define DEFAULT_RED_LED_CURRENT                 MAX30101_LED_CURR_50MA
#define DEFAULT_IR_LED_CURRENT                  MAX30101_LED_CURR_25_4MA
#define DEFAULT_GREEN_LED_CURRENT               MAX30101_LED_CURR_25_4MA
#define DEFAULT_LED_CURRENT          			      MAX30101_LED_CURR_25_4MA

#define SAMPLING_FREQUENCY                       100
#define CURRENT_ADJUSTMENT_PERIOD_MS             500
#define IR_LED_CURRENT                           MAX30101_LED_CURR_25_4MA
#define RED_LED_CURRENT_START                    MAX30101_LED_CURR_50MA
#define GREEN_LED_CURRENT_START                  0x7F//MAX30101_LED_CURR_25_4MA

#define PILOT_PA                                 0x03
#define SHUTDOWN_PA                              0x00

#define DC_REMOVER_ALPHA                         0.95

#define REPORTING_PERIOD_MS                      3000
#define CALCULATE_EVERY_N_BEATS                  3
		//MODE[2:0]
enum Mode {                                    							// combinations of all modes
    MAX30101_MODE_HRONLY    = 0x02,
    MAX30101_MODE_SPO2_HR   = 0x03,
	  MAX30101_Multi_LED_mode = 0x07
};
       // SPO2_SR[4:2]
enum SamplingRate {                             						// Combinations of all samplingRates in the datasheet 
  MAX30101_SAMPRATE_50HZ      = 0x00,
	MAX30101_SAMPRATE_100HZ     = 0x01,
	MAX30101_SAMPRATE_200HZ     = 0x02,
	MAX30101_SAMPRATE_400HZ     = 0x03,
	MAX30101_SAMPRATE_800HZ     = 0x04,
	MAX30101_SAMPRATE_1000HZ    = 0x05,
	MAX30101_SAMPRATE_1600HZ    = 0x06,
	MAX30101_SAMPRATE_3200HZ    = 0x07
};
       // LED_PW[1:0]
enum LEDPulseWidth {                             						// Combinations of all LEDPulseWidth in the datasheet
    MAX30100_SPC_PW_69US_15BITS    = 0x00,
    MAX30100_SPC_PW_118US_16BITS   = 0x01,
    MAX30100_SPC_PW_215US_17BITS   = 0x02,
    MAX30100_SPC_PW_411US_18BITS   = 0x03
};
	   //LEDx_PA [7:0], RED_PA[7:0], IR_PA[7:0], or G_PA[7:0]
enum LEDCurrent {                                   					// Combinations of LEDcurrent in the datasheet
	MAX30101_LED_CURR_0MA        = 0x00,
	MAX30101_LED_CURR_12_5MA     = 0x3F,
	MAX30101_LED_CURR_25_4MA     = 0x7F,
	MAX30101_LED_CURR_50MA       = 0xFF
};


enum PulseOximeterState {               // All available PulseOximeter  states
    PULSEOXIMETER_STATE_INIT,
    PULSEOXIMETER_STATE_IDLE,
    PULSEOXIMETER_STATE_DETECTING
} ;

enum PulseOximeterDebuggingMode {       // All available PulseOximeterDebugging Modes  
    PULSEOXIMETER_DEBUGGINGMODE_NONE,
    PULSEOXIMETER_DEBUGGINGMODE_RAW_VALUES,
    PULSEOXIMETER_DEBUGGINGMODE_AC_VALUES,
    PULSEOXIMETER_DEBUGGINGMODE_PULSEDETECT
};

#endif /* MAX30102_H_ */