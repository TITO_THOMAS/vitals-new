#include "MAX301010_interface.h"
#include "nrf_drv_gpiote.h"
#include "usbd_ble_nus.h"
#include "nrf_delay.h"


volatile bool alert_pin_interrupt = false;

void max30101_twi_init(void);
void max30101_gpio_init(void);
bool maxim_max30102_write_reg(uint8_t reg, uint8_t p_content);
bool maxim_max30102_read_reg(uint8_t reg, uint8_t * p_content);
void max30101_power_off(void);
void max30101_power_on(void);
ret_code_t max30101_gpiote_init();
ret_code_t max30101_gpiote_unint(void);
void prepare_for_sleep(void);
void wakeup_pin_init(void);


void max30101_power_on(void)
{
nrf_gpio_pin_set(25);

}
void max30101_power_off(void)
{
nrf_gpio_pin_clear(25);
}
void max30101_gpio_init(void)
{
nrf_gpio_cfg_output(25);          // MAX30101 power pin

//nrf_gpio_cfg_output(9);          // MAX30101 power pin
//nrf_gpio_cfg_output(7);          // MAX30101 power pin
//nrf_gpio_cfg_output(12);          // MAX30101 power pin
//nrf_gpio_cfg_output(20);          // MAX30101 power pin

//nrf_gpio_pin_set(12);
//nrf_gpio_pin_set(20);
//nrf_gpio_pin_set(25);
//nrf_gpio_pin_set(9);
//nrf_gpio_pin_set(7);
//while(1){
//}

nrf_gpio_cfg_input(MAX30101_INT,NRF_GPIO_PIN_PULLUP);   // Interrupt pin

max30101_power_on();
nrf_gpio_cfg_output(BUTTON_INT);
nrf_gpio_pin_set(BUTTON_INT);


}


static void max30101_gpio_interrupt_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    alert_pin_interrupt = true;
NRF_LOG_INFO("INTERRUPT");

}

ret_code_t max30101_gpiote_init()
{
    ret_code_t err_code;
    if (!nrfx_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        APP_ERROR_CHECK(err_code);
    }
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
    in_config.pull = NRF_GPIO_PIN_PULLUP;

    err_code = nrf_drv_gpiote_in_init(BUTTON_INT, &in_config, max30101_gpio_interrupt_handler);

    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_event_enable(BUTTON_INT, true);

    APP_ERROR_CHECK(err_code);
}

ret_code_t max30101_gpiote_unint(void)
{
ret_code_t err_code;
nrf_drv_gpiote_in_event_disable(BUTTON_INT);

nrf_drv_gpiote_in_uninit(BUTTON_INT);
//    APP_ERROR_CHECK(err_code);

nrf_drv_gpiote_uninit();
//    APP_ERROR_CHECK(err_code);
}


ret_code_t usb_gpio_unint(void)
{
ret_code_t err_code;
nrf_drv_gpiote_in_event_disable(29);

nrf_drv_gpiote_in_uninit(29);
//    APP_ERROR_CHECK(err_code);

nrf_drv_gpiote_uninit();
//    APP_ERROR_CHECK(err_code);
}

void wakeup_pin_init(void)
{
led_off(LED_GREEN);
led_off(LED_BLUE);
led_off(LED_RED);
nrf_gpio_cfg_sense_input(BUTTON_INT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW);
nrf_gpio_cfg_sense_input(29, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_SENSE_HIGH);
}


void prepare_for_sleep(void)
{
max30101_gpiote_unint();
usb_gpio_unint();
wakeup_pin_init();
}
///////////////////////////////////////////////////////
////////////////////////////////////////////////////////

