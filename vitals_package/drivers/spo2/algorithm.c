#include "algorithm.h"
#include "MAX30102.h"
#include "max301010_interface.h"
#include <math.h>

static uint32_t current_ir_value  = 0;
static uint32_t current_red_value = 0;

/*
**** old code
*/
//TODO put all these into functions
uint32_t rawIRValue;
uint32_t rawRedValue;
uint32_t rawGREENValue;
static float threshold = BEATDETECTOR_MIN_THRESHOLD;
static float beatPeriod;
static float v_spo2[6];
float lastMaxValue;
float filteredPulseValue;
float tsLastBeat;
static float per_index_ir =0;
static float per_index_red =0;
enum BeatDetectorState {    // All available beatdetector states
  BEATDETECTOR_STATE_INIT,
  BEATDETECTOR_STATE_WAITING,
  BEATDETECTOR_STATE_FOLLOWING_SLOPE,
  BEATDETECTOR_STATE_MAYBE_DETECTED,
  BEATDETECTOR_STATE_MASKING
};

enum PulseOximeterState poxState;
// enum BeatDetectorState state;
static float dcw;
static uint32_t tsLastReport = 0;
uint32_t shutdowntime        = 0;
// uint32_t shutdowntime_not_connected=0;

float irACValueSqSum;
float redACValueSqSum;
uint8_t beatsDetectedNum;
float samplesRecorded;
float spO2;
const float spo2lookup[250] = {99.3,
    99.3,
    99,
    98.7,
    98.4,
    98.1,
    97.9,
    97.7,
    97.4,
    97.2,
    96.9,
    96.7,
    96.5,
    96.2,
    96,
    95.8,
    95.6,
    95.3,
    95,
    94.8,
    94.6,
    94.3,
    94,
    93.7,
    93.5,
    93.2,
    92.9,
    92.6,
    92.4,
    92.1,
    91.9,
    91.5,
    91.3,
    90.9,
    90.7,
    90.4,
    90.2,
    89.7,
    89.4,
    89.2,
    88.8,
    88.6,
    88.1,
    88,
    87.5,
    87.2,
    86.8,
    86.5,
    86.1,
    86,
    85.4,
    85.2,
    84.8,
    84.5,
    84.2,
    83.9,
    83.5,
    83.1,
    82.7,
    82.3,
    81.9,
    81.6,
    81.2,
    80.7,
    80.4,
    80,
    79.7,
    79.2,
    78.8,
    78.4,
    78,
    77.4,
    77,
    76.5,
    76.3,
    75.4,
    75.2,
    74.8,
    74.5,
    73.9,
    73.6,
    73.1,
    72.7,
    72.2,
    71.7,
    71.2,
    70.6,
    70.2,
    69.6,
    69.3,
    68.6,
    68.3,
    67.2,
    67,
    66.5,
    65.8,
    65.5,
    64.8,
    64.3,
    64,
    63.3,
    62.8,
    62.3,
    61.8,
    61.3,
    60.8,
    60.1,
    59.8,
    59.3,
    58.7,
    58.2,
    57.5,
    56.9,
    56.4,
    56.1,
    55.5,
    55.1,
    54.4,
    53.8,
    53.4,
    53,
    52.2,
    51.8,
    51.4,
    50.8,
    50.5,
    49.7,
    49.2,
    48.8,
    48.4,
    47.9,
    47.1,
    46.9,
    46.6,
    45.8,
    45.6,
    44.7,
    44.1,
    43.7,
    43.1,
    42.7,
    42.1,
    41.8,
    41.1,
    40.5,
    40.1,
    39.5,
    39.1,
    38.6,
    38.2,
    37.6,
    37.3,
    36.6,
    35.5,
    35.2,
    34.9,
    34.3,
    33.7,
    33.1,
    32.9,
    32.3,
    31.7,
    31.2,
    30.7,
    30,
    29.4,
    29,
    28.5,
    27.8,
    27.5,
    26.8,
    26.2,
    25.8,
    25.2,
    24.7,
    24.1,
    23.7,
    23.5,
    22.7,
    22.3,
    21.7,
    21,
    20.4,
    20.2,
    19.5,
    19,
    18.5,
    18,
    17.5,
    17.2,
    16.5,
    16.2,
    15.5,
    15,
    14.3,
    13.9,
    13.3,
    12.9,
    12.3,
    11.8,
    11.4,
    10.9,
    10.1,
    9.8,
    9.2,
    8.8,
    8.2,
    7.4,
    7.1,
    6.6,
    6.1,
    5.5,
    5,
    4.5,
    3.8,
    3.2,
    2.8,
    2.2,
    1.7,
    1.1,
    0.6,
    0.3};
const uint8_t spO2LUT[43]   = {100,
    100,
    100,
    100,
    99,
    99,
    99,
    99,
    99,
    99,
    98,
    98,
    98,
    98,
    98,
    97,
    97,
    97,
    97,
    97,
    97,
    96,
    96,
    96,
    96,
    96,
    96,
    95,
    95,
    95,
    95,
    95,
    95,
    94,
    94,
    94,
    94,
    94,
    93,
    93,
    93,
    93,
    93};
float v[2];

///////////////////////////////////////////////////////////////////////////////////////////////////
uint32_t millis(void);
void PulseOximeter_begin(void);
void PulseOximeter_shutdown(void);
void PulseOximeter_proxymode(void);
void begin(void);
void setMode(enum Mode mode);
void setLedsPulseWidth(enum LEDPulseWidth ledPulseWidth);
void setSamplingRate(enum SamplingRate samplingRate);
void Setspo2adcRange(uint8_t adcrange);
void setLedCurrentIR(enum LEDCurrent irLedCurrent, uint8_t address);
void setLedCurrentRED(enum LEDCurrent redLedCurrent, uint8_t address);
void setLedCurrentGREEN(enum LEDCurrent greenLedCurrent, uint8_t address);
// Proximity Mode
void setLedProximity_ModePA(enum LEDCurrent ProximityPA, uint8_t address);
void checkfordataready(void);
// void setHighresModeEnabled(bool enabled);
void readMaxFIFO(void);
void SetMaxInterrupt(uint8_t interrupt_mode);
void SetMaxInterruptForTemp(uint8_t interrupt_mode);
void SetMaxFIFOConfig(uint8_t fifo_mode);
void SetMaxMultiModeTimeSlot(uint8_t ledTimeSlot, uint8_t address);
void SetMaxProximityThreshold(uint8_t Threshold, uint8_t address);
uint32_t millis(void);
void PulseOximeter_update(void);
void checkSample(void);
void update(void);
bool checkForBeat(float sample_data);
void decreaseThreshold(void);
bool addSample(float sample);
float BeatDetector_getRate(void);
float getCurrentThreshold(void);
float LPFstep(float x);
float DCremoverstep(float x);
void SpO2Calculator_update(float irACValue, float redACValue, bool beatDetected);
void SpO2Calculator_reset(void);
float SpO2Calculator_getSpO2(void);
/////////////////////////////////////////////////////////////////////////////////////
/*
*** old code
*/
uint32_t get_current_ir(void);
uint32_t get_current_red(void);
float get_current_pi_ir(void);
float get_current_pi_red(void);

void maxim_heart_rate_and_oxygen_saturation(uint32_t *pun_ir_buffer,
    int32_t n_ir_buffer_length,
    uint32_t *pun_red_buffer,
    int32_t *pn_spo2,
    int8_t *pch_spo2_valid,
    int32_t *pn_heart_rate,
    int8_t *pch_hr_valid);
void maxim_find_peaks(int32_t *pn_locs,
    int32_t *pn_npks,
    int32_t *pn_x,
    int32_t n_size,
    int32_t n_min_height,
    int32_t n_min_distance,
    int32_t n_max_num);
void maxim_peaks_above_min_height(
    int32_t *pn_locs, int32_t *pn_npks, int32_t *pn_x, int32_t n_size, int32_t n_min_height);
void maxim_remove_close_peaks(int32_t *pn_locs, int32_t *pn_npks, int32_t *pn_x, int32_t n_min_distance);
void maxim_sort_ascend(int32_t *pn_x, int32_t n_size);
void maxim_sort_indices_descend(int32_t *pn_x, int32_t *pn_indx, int32_t n_size);

const uint16_t auw_hamm[31] = {41, 276, 512, 276, 41};    // Hamm=  long16(512* hamming(5)');
// uch_spo2_table is computed as  -45.060*ratioAverage* ratioAverage + 30.354 *ratioAverage + 94.845 ;
const uint8_t uch_spo2_table[184] = {95,
    95,
    95,
    96,
    96,
    96,
    97,
    97,
    97,
    97,
    97,
    98,
    98,
    98,
    98,
    98,
    99,
    99,
    99,
    99,
    99,
    99,
    99,
    99,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    100,
    99,
    99,
    99,
    99,
    99,
    99,
    99,
    99,
    98,
    98,
    98,
    98,
    98,
    98,
    97,
    97,
    97,
    97,
    96,
    96,
    96,
    96,
    95,
    95,
    95,
    94,
    94,
    94,
    93,
    93,
    93,
    92,
    92,
    92,
    91,
    91,
    90,
    90,
    89,
    89,
    89,
    88,
    88,
    87,
    87,
    86,
    86,
    85,
    85,
    84,
    84,
    83,
    82,
    82,
    81,
    81,
    80,
    80,
    79,
    78,
    78,
    77,
    76,
    76,
    75,
    74,
    74,
    73,
    72,
    72,
    71,
    70,
    69,
    69,
    68,
    67,
    66,
    66,
    65,
    64,
    63,
    62,
    62,
    61,
    60,
    59,
    58,
    57,
    56,
    56,
    55,
    54,
    53,
    52,
    51,
    50,
    49,
    48,
    47,
    46,
    45,
    44,
    43,
    42,
    41,
    40,
    39,
    38,
    37,
    36,
    35,
    34,
    33,
    31,
    30,
    29,
    28,
    27,
    26,
    25,
    23,
    22,
    21,
    20,
    19,
    17,
    16,
    15,
    14,
    12,
    11,
    10,
    9,
    7,
    6,
    5,
    3,
    2,
    1};

/**
 * \brief        Calculate the heart rate and SpO2 level
 * \par          Details
 *               By detecting  peaks of PPG cycle and corresponding AC/DC of red/infra-red signal, the ratio for the
 * SPO2 is computed. Since this algorithm is aiming for Arm M0/M3. formaula for SPO2 did not achieve the accuracy due to
 * register overflow. Thus, accurate SPO2 is precalculated and save longo uch_spo2_table[] per each ratio.
 *
 * \param[in]    *pun_ir_buffer           - IR sensor data buffer
 * \param[in]    n_ir_buffer_length      - IR sensor data buffer length
 * \param[in]    *pun_red_buffer          - Red sensor data buffer
 * \param[out]    *pn_spo2                - Calculated SpO2 value
 * \param[out]    *pch_spo2_valid         - 1 if the calculated SpO2 value is valid
 * \param[out]    *pn_heart_rate          - Calculated heart rate value
 * \param[out]    *pch_hr_valid           - 1 if the calculated heart rate value is valid
 *
 * \retval       None
 */
void maxim_heart_rate_and_oxygen_saturation(uint32_t *pun_ir_buffer,
    int32_t n_ir_buffer_length,
    uint32_t *pun_red_buffer,
    int32_t *pn_spo2,
    int8_t *pch_spo2_valid,
    int32_t *pn_heart_rate,
    int8_t *pch_hr_valid) {
  uint32_t un_ir_mean, un_only_once;
  int32_t k, n_i_ratio_count;
  int32_t i, s, m, n_exact_ir_valley_locs_count, n_middle_idx;
  int32_t n_th1, n_npks, n_c_min;
  int32_t an_ir_valley_locs[15];
  int32_t an_exact_ir_valley_locs[15];
  int32_t an_dx_peak_locs[15];
  int32_t n_peak_interval_sum;

  int32_t n_y_ac, n_x_ac;
  int32_t n_spo2_calc;
  int32_t n_y_dc_max, n_x_dc_max;
  int32_t n_y_dc_max_idx, n_x_dc_max_idx;
  int32_t an_ratio[5], n_ratio_average;
  int32_t n_nume, n_denom;
  // remove DC of ir signal
  un_ir_mean = 0;
  for (k = 0; k < n_ir_buffer_length; k++)
    un_ir_mean += pun_ir_buffer[k];
  un_ir_mean = un_ir_mean / n_ir_buffer_length;
  for (k = 0; k < n_ir_buffer_length; k++)
    an_x[k] = pun_ir_buffer[k] - un_ir_mean;

  // 4 pt Moving Average
  for (k = 0; k < BUFFER_SIZE - MA4_SIZE; k++) {
    n_denom = (an_x[k] + an_x[k + 1] + an_x[k + 2] + an_x[k + 3]);
    an_x[k] = n_denom / (int32_t)4;
  }

  // get difference of smoothed IR signal

  for (k = 0; k < BUFFER_SIZE - MA4_SIZE - 1; k++)
    an_dx[k] = (an_x[k + 1] - an_x[k]);

  // 2-pt Moving Average to an_dx
  for (k = 0; k < BUFFER_SIZE - MA4_SIZE - 2; k++) {
    an_dx[k] = (an_dx[k] + an_dx[k + 1]) / 2;
  }

  // hamming window
  // flip wave form so that we can detect valley with peak detector
  for (i = 0; i < BUFFER_SIZE - HAMMING_SIZE - MA4_SIZE - 2; i++) {
    s = 0;
    for (k = i; k < i + HAMMING_SIZE; k++) {
      s -= an_dx[k] * auw_hamm[k - i];
    }
    an_dx[i] = s / (int32_t)1146;    // divide by sum of auw_hamm
  }

  n_th1 = 0;    // threshold calculation
  for (k = 0; k < BUFFER_SIZE - HAMMING_SIZE; k++) {
    n_th1 += ((an_dx[k] > 0) ? an_dx[k] : ((int32_t)0 - an_dx[k]));
  }
  n_th1 = n_th1 / (BUFFER_SIZE - HAMMING_SIZE);
  // peak location is acutally index for sharpest location of raw signal since we flipped the signal
  maxim_find_peaks(an_dx_peak_locs,
      &n_npks,
      an_dx,
      BUFFER_SIZE - HAMMING_SIZE,
      n_th1,
      50,
      3);    // peak_height, peak_distance, max_num_peaks

  n_peak_interval_sum = 0;
  if (n_npks >= 2) {
    for (k = 1; k < n_npks; k++)
      n_peak_interval_sum += (an_dx_peak_locs[k] - an_dx_peak_locs[k - 1]);
    n_peak_interval_sum = n_peak_interval_sum / (n_npks - 1);
    *pn_heart_rate      = (int32_t)(6000 / n_peak_interval_sum);    // beats per minutes
    *pch_hr_valid       = 1;
  } else {
    *pn_heart_rate = -999;
    *pch_hr_valid  = 0;
  }

  for (k = 0; k < n_npks; k++)
    an_ir_valley_locs[k] = an_dx_peak_locs[k] + HAMMING_SIZE / 2;

  // raw value : RED(=y) and IR(=X)
  // we need to assess DC and AC value of ir and red PPG.
  for (k = 0; k < n_ir_buffer_length; k++) {
    an_x[k] = pun_ir_buffer[k];
    an_y[k] = pun_red_buffer[k];
  }

  // find precise min near an_ir_valley_locs
  n_exact_ir_valley_locs_count = 0;
  for (k = 0; k < n_npks; k++) {
    un_only_once = 1;
    m            = an_ir_valley_locs[k];
    n_c_min      = 16777216;    // 2^24;
    if (m + 5 < BUFFER_SIZE - HAMMING_SIZE && m - 5 > 0) {
      for (i = m - 5; i < m + 5; i++)
        if (an_x[i] < n_c_min) {
          if (un_only_once > 0) {
            un_only_once = 0;
          }
          n_c_min                    = an_x[i];
          an_exact_ir_valley_locs[k] = i;
        }
      if (un_only_once == 0)
        n_exact_ir_valley_locs_count++;
    }
  }
  if (n_exact_ir_valley_locs_count < 2) {
    *pn_spo2        = -999;    // do not use SPO2 since signal ratio is out of range
    *pch_spo2_valid = 0;
    return;
  }
  // 4 pt MA
  for (k = 0; k < BUFFER_SIZE - MA4_SIZE; k++) {
    an_x[k] = (an_x[k] + an_x[k + 1] + an_x[k + 2] + an_x[k + 3]) / (int32_t)4;
    an_y[k] = (an_y[k] + an_y[k + 1] + an_y[k + 2] + an_y[k + 3]) / (int32_t)4;
  }

  // using an_exact_ir_valley_locs , find ir-red DC andir-red AC for SPO2 calibration ratio
  // finding AC/DC maximum of raw ir * red between two valley locations
  n_ratio_average = 0;
  n_i_ratio_count = 0;

  for (k = 0; k < 5; k++)
    an_ratio[k] = 0;
  for (k = 0; k < n_exact_ir_valley_locs_count; k++) {
    if (an_exact_ir_valley_locs[k] > BUFFER_SIZE) {
      *pn_spo2        = -999;    // do not use SPO2 since valley loc is out of range
      *pch_spo2_valid = 0;
      return;
    }
  }
  // find max between two valley locations
  // and use ratio betwen AC compoent of Ir & Red and DC compoent of Ir & Red for SPO2

  for (k = 0; k < n_exact_ir_valley_locs_count - 1; k++) {
    n_y_dc_max = -16777216;
    n_x_dc_max = -16777216;
    if (an_exact_ir_valley_locs[k + 1] - an_exact_ir_valley_locs[k] > 10) {
      for (i = an_exact_ir_valley_locs[k]; i < an_exact_ir_valley_locs[k + 1]; i++) {
        if (an_x[i] > n_x_dc_max) {
          n_x_dc_max     = an_x[i];
          n_x_dc_max_idx = i;
        }
        if (an_y[i] > n_y_dc_max) {
          n_y_dc_max     = an_y[i];
          n_y_dc_max_idx = i;
        }
      }
      n_y_ac = (an_y[an_exact_ir_valley_locs[k + 1]] - an_y[an_exact_ir_valley_locs[k]]) *
          (n_y_dc_max_idx - an_exact_ir_valley_locs[k]);    // red
      n_y_ac =
          an_y[an_exact_ir_valley_locs[k]] + n_y_ac / (an_exact_ir_valley_locs[k + 1] - an_exact_ir_valley_locs[k]);

      n_y_ac = an_y[n_y_dc_max_idx] - n_y_ac;    // subracting linear DC compoenents from raw
      n_x_ac = (an_x[an_exact_ir_valley_locs[k + 1]] - an_x[an_exact_ir_valley_locs[k]]) *
          (n_x_dc_max_idx - an_exact_ir_valley_locs[k]);    // ir
      n_x_ac =
          an_x[an_exact_ir_valley_locs[k]] + n_x_ac / (an_exact_ir_valley_locs[k + 1] - an_exact_ir_valley_locs[k]);
      n_x_ac = an_x[n_y_dc_max_idx] - n_x_ac;    // subracting linear DC compoenents from raw
      n_nume = (n_y_ac * n_x_dc_max) >> 7;       // prepare X100 to preserve floating value
      n_denom = (n_x_ac * n_y_dc_max) >> 7;
      if (n_denom > 0 && n_i_ratio_count < 5 && n_nume != 0) {
        an_ratio[n_i_ratio_count] =
            (n_nume * 100) / n_denom;    // formular is ( n_y_ac *n_x_dc_max) / ( n_x_ac *n_y_dc_max) ;
        n_i_ratio_count++;
      }
    }
  }
if(n_x_dc_max!=0)
      per_index_ir = ((float) n_x_ac/ (float)n_x_dc_max);
if(n_y_dc_max!=0)
      per_index_red = ((float)n_y_ac /  (float)n_y_dc_max);
 
  maxim_sort_ascend(an_ratio, n_i_ratio_count);
  n_middle_idx = n_i_ratio_count / 2;

  if (n_middle_idx > 1)
    n_ratio_average = (an_ratio[n_middle_idx - 1] + an_ratio[n_middle_idx]) / 2;    // use median
  else
    n_ratio_average = an_ratio[n_middle_idx];

  if (n_ratio_average > 2 && n_ratio_average < 184) {
    n_spo2_calc     = uch_spo2_table[n_ratio_average];
    *pn_spo2        = n_spo2_calc;
    *pch_spo2_valid = 1;    //  float_SPO2 =  -45.060*n_ratio_average* n_ratio_average/10000 + 30.354
                            //  *n_ratio_average/100 + 94.845 ;  // for comparison with table
  } else {
    *pn_spo2        = -999;    // do not use SPO2 since signal ratio is out of range
    *pch_spo2_valid = 0;
  }
}

/**
 * \brief        Find peaks
 * \par          Details
 *               Find at most MAX_NUM peaks above MIN_HEIGHT separated by at least MIN_DISTANCE
 *
 * \retval       None
 */
void maxim_find_peaks(int32_t *pn_locs,
    int32_t *pn_npks,
    int32_t *pn_x,
    int32_t n_size,
    int32_t n_min_height,
    int32_t n_min_distance,
    int32_t n_max_num)

{
  maxim_peaks_above_min_height(pn_locs, pn_npks, pn_x, n_size, n_min_height);
  maxim_remove_close_peaks(pn_locs, pn_npks, pn_x, n_min_distance);
  *pn_npks = min(*pn_npks, n_max_num);
}

/**
 * \brief        Find peaks above n_min_height
 * \par          Details
 *               Find all peaks above MIN_HEIGHT
 *
 * \retval       None
 */
void maxim_peaks_above_min_height(
    int32_t *pn_locs, int32_t *pn_npks, int32_t *pn_x, int32_t n_size, int32_t n_min_height)

{
  int32_t i = 1, n_width;
  *pn_npks  = 0;

  while (i < n_size - 1) {
    if (pn_x[i] > n_min_height && pn_x[i] > pn_x[i - 1]) {    // find left edge of potential peaks
      n_width = 1;
      while (i + n_width < n_size && pn_x[i] == pn_x[i + n_width])    // find flat peaks
        n_width++;
      if (pn_x[i] > pn_x[i + n_width] && (*pn_npks) < 15) {    // find right edge of peaks
        pn_locs[(*pn_npks)++] = i;
        // for flat peaks, peak location is left edge
        i += n_width + 1;
      } else
        i += n_width;
    } else
      i++;
  }
}

/**
 * \brief        Remove peaks
 * \par          Details
 *               Remove peaks separated by less than MIN_DISTANCE
 *
 * \retval       None
 */
void maxim_remove_close_peaks(int32_t *pn_locs, int32_t *pn_npks, int32_t *pn_x, int32_t n_min_distance)

{
  int32_t i, j, n_old_npks, n_dist;

  /* Order peaks from large to small */
  maxim_sort_indices_descend(pn_x, pn_locs, *pn_npks);

  for (i = -1; i < *pn_npks; i++) {
    n_old_npks = *pn_npks;
    *pn_npks   = i + 1;
    for (j = i + 1; j < n_old_npks; j++) {
      n_dist = pn_locs[j] - (i == -1 ? -1 : pn_locs[i]);    // lag-zero peak of autocorr is at index -1
      if (n_dist > n_min_distance || n_dist < -n_min_distance)
        pn_locs[(*pn_npks)++] = pn_locs[j];
    }
  }

  // Resort indices longo ascending order
  maxim_sort_ascend(pn_locs, *pn_npks);
}

/**
 * \brief        Sort array
 * \par          Details
 *               Sort array in ascending order (insertion sort algorithm)
 *
 * \retval       None
 */
void maxim_sort_ascend(int32_t *pn_x, int32_t n_size)

{
  int32_t i, j, n_temp;
  for (i = 1; i < n_size; i++) {
    n_temp = pn_x[i];
    for (j = i; j > 0 && n_temp < pn_x[j - 1]; j--)
      pn_x[j] = pn_x[j - 1];
    pn_x[j] = n_temp;
  }
}

/**
 * \brief        Sort indices
 * \par          Details
 *               Sort indices according to descending order (insertion sort algorithm)
 *
 * \retval       None
 */
void maxim_sort_indices_descend(int32_t *pn_x, int32_t *pn_indx, int32_t n_size) {
  int32_t i, j, n_temp;
  for (i = 1; i < n_size; i++) {
    n_temp = pn_indx[i];
    for (j = i; j > 0 && pn_x[n_temp] > pn_x[pn_indx[j - 1]]; j--)
      pn_indx[j] = pn_indx[j - 1];
    pn_indx[j] = n_temp;
  }
}

////////////////////////////////////////////////////////////////// old code start

uint32_t millis(void) {
  float count;
  count = (NRF_RTC0->COUNTER) & 0x00FFFFFF;
  count = count / 32.94;
  return (uint32_t)count;
}

bool checkForBeat(float sample_data) {
  uint8_t err_code;
  NRF_LOG_INFO("s= %d",sample_data);
  static enum BeatDetectorState state1 = BEATDETECTOR_STATE_INIT;
  //	static float threshold = BEATDETECTOR_MIN_THRESHOLD;
  //	uint16_t  gatPeriod = 0;
  //	uint16_t lastMaxValue = 0;
  //	uint16_t tsLastBeat = 0;
  bool beatDetected = false;
  /*
                  sprintf(m_tx_buf,"%.6f",sample_data);
                  printff(m_tx_buf);
                  printff("\r\n");
  */
  switch (state1) {
  case BEATDETECTOR_STATE_INIT:

    if (millis() > BEATDETECTOR_INIT_HOLDOFF) {    // wait 2s
      state1 = BEATDETECTOR_STATE_WAITING;
    }
    break;

  case BEATDETECTOR_STATE_WAITING:

    if (sample_data > threshold) {
      threshold = min(sample_data, BEATDETECTOR_MAX_THRESHOLD);
      state1    = BEATDETECTOR_STATE_FOLLOWING_SLOPE;
      //     	nrf_delay_ms(300);
    }
    if (millis() - tsLastBeat > BEATDETECTOR_INVALID_READOUT_DELAY) {
      beatPeriod   = 0;
      lastMaxValue = 0;
    }
    decreaseThreshold();
    break;

  case BEATDETECTOR_STATE_FOLLOWING_SLOPE:
    if (sample_data < threshold) {
      state1 = BEATDETECTOR_STATE_MAYBE_DETECTED;

    } else {
      threshold = min(sample_data, BEATDETECTOR_MAX_THRESHOLD);
    }
    break;

  case BEATDETECTOR_STATE_MAYBE_DETECTED:
    if (sample_data + BEATDETECTOR_STEP_RESILIENCY < threshold) {
      // Found a beat
      beatDetected = true;
      lastMaxValue = sample_data;
      state1       = BEATDETECTOR_STATE_MASKING;
      float delta  = millis() - tsLastBeat;

      //	nrf_delay_ms(100);
      // 	nrf_delay_ms(100);
      if (delta) {
        beatPeriod = BEATDETECTOR_BPFILTER_ALPHA * delta + (1 - BEATDETECTOR_BPFILTER_ALPHA) * beatPeriod;
      }
      // sprintf(green_temp,"G=%f",delta);
      // ble_nus_string_send(&m_nus, green_temp, 15);
      tsLastBeat = millis();
    } else {
      state1 = BEATDETECTOR_STATE_FOLLOWING_SLOPE;
    }
    break;

  case BEATDETECTOR_STATE_MASKING:
    if (millis() - tsLastBeat > BEATDETECTOR_MASKING_HOLDOFF) {
      state1 = BEATDETECTOR_STATE_WAITING;
    }
    decreaseThreshold();
    break;
  }

  return beatDetected;
}

void decreaseThreshold() {
  if (lastMaxValue > 0 && beatPeriod > 0) {    // Check a valid beat rate readout is present
    threshold -=
        lastMaxValue * (1 - BEATDETECTOR_THRESHOLD_FALLOFF_TARGET) / (beatPeriod / BEATDETECTOR_SAMPLES_PERIOD);
  } else {
    // Asymptotic decay
    threshold *= BEATDETECTOR_THRESHOLD_DECAY_FACTOR;
  }

  if (threshold < BEATDETECTOR_MIN_THRESHOLD) {
    threshold = BEATDETECTOR_MIN_THRESHOLD;
  }
}

float LPFstep(float x)    // class II
{
  /*
          float v[2];
          v[0] = v[1];
          v[1] = (2.452372752527856026e-1 * x)
                   + (0.50952544949442879485 * v[0]);
          return
                   (v[0] + v[1]);
    */
  /*
        v_spo2[0] = v_spo2[1];
        v_spo2[1] = v_spo2[2];
        v_spo2[2] = v_spo2[3];
        v_spo2[3] = v_spo2[4];
        v_spo2[4] = (2.959180940647690005e-3 * x)
                 + (-0.16738407597380611236 * v_spo2[0])
                 + (0.98669490841258311598 * v_spo2[1])
                 + (-2.25624612109073208188 * v_spo2[2])
                 + (2.38958839360159203125 * v_spo2[3]);
        return
                 (v_spo2[0] + v_spo2[4])
                +4 * (v_spo2[1] + v_spo2[3])
                +6 * v_spo2[2];

  */
  /*
  v_spo2[0] = v_spo2[1];
  v_spo2[1] = v_spo2[2];
  v_spo2[2] =
      (7.820208033497201908e-3 * x) + (-0.76600660094326400440 * v_spo2[0]) + (1.73472576880927520371 * v_spo2[1]);
  return (v_spo2[0] + v_spo2[2]) + 2 * v_spo2[1];
  */
            v_spo2[0] = v_spo2[1];
          v_spo2[1] = (2.452372752527856026e-1 * x)
                   + (0.50952544949442879485 * v_spo2[0]);
          return
                   (v_spo2[0] + v_spo2[1]);
}

float DCremoverstep(float x) {
  float alpha = 0.99;

  float olddcw = dcw;
  dcw          = (float)x + (alpha * dcw);

  return dcw - olddcw;
}

float BeatDetector_getRate(void) {
  if (beatPeriod != 0) {
    return (1000 / beatPeriod) * 60;
  } else {
    return 0;
  }
}

void SpO2Calculator_update(float irACValue, float redACValue, bool beatDetected) {
  irACValueSqSum += irACValue * irACValue;
  redACValueSqSum += redACValue * redACValue;
  ++samplesRecorded;

  if (beatDetected) {
    ++beatsDetectedNum;
    if (beatsDetectedNum == CALCULATE_EVERY_N_BEATS) {
      float acSqRatio = 100.0 * log(redACValueSqSum / samplesRecorded) / log(irACValueSqSum / samplesRecorded);
      //  float acSqRatio = 100.0 * log(redACValueSqSum) / log(irACValueSqSum);
      uint8_t index = 0;

      if (acSqRatio > 75) {    /// 66
        index = (uint8_t)acSqRatio - 98;

      } else if (acSqRatio > 50) {
        index = (uint8_t)acSqRatio - 35;    //
      }

      SpO2Calculator_reset();

      spO2 = (float)spo2lookup[index];
      //	spO2 = acSqRatio;  // for debugging
    }
  }
}

void SpO2Calculator_reset() {
  samplesRecorded  = 0;
  redACValueSqSum  = 0;
  irACValueSqSum   = 0;
  beatsDetectedNum = 0;
  spO2             = 0;
}

float SpO2Calculator_getSpO2(void) {
  return spO2;    // spO2
}

void checkfordataready() {
  uint8_t result, new_fifo_data_ready, Proximity_Threshold_Triggered;
  maxim_max30102_read_reg(MAX30101_REG_INTERRUPT_STATUS_1, &new_fifo_data_ready);
  maxim_max30102_read_reg(MAX30101_REG_INTERRUPT_STATUS_1, &Proximity_Threshold_Triggered);
  //	result = (new_fifo_data_ready >> (6)) & (Proximity_Threshold_Triggered >> (4));
  //	result = (new_fifo_data_ready >> (6)) & 1;
  //    if (new_fifo_data_ready == 64 ){
  //		 read_flag=1;
  //		}
}
void PulseOximeter_begin() {
  begin();
  setMode(SPO2_MODE);    // MAX30101_MODE_SPO2_HR  //MAX30101_MODE_HRONLY //SOP2_MODE
  setLedCurrentRED(RED_LED_CURRENT_START, MAX30101_REG_LED_1_PA);
  setLedCurrentIR(IR_LED_CURRENT, MAX30101_REG_LED_2_PA);    // (50mA,27.1mA)

  //  setLedCurrentGREEN(GREEN_LED_CURRENT_START, MAX30101_REG_LED_3_PA);
  //  irDCRemover = DCRemover(DC_REMOVER_ALPHA);				  // filter
  //  redDCRemover = DCRemover(DC_REMOVER_ALPHA);				  // filter
  //  state = PULSEOXIMETER_STATE_IDLE;                   // ???

  //  enable interrupts registers
  SetMaxInterrupt(
      MAX30101_INT_RDY);    // In SpO2 and HR modes, this interrupt triggers when there is a new sample in the data FIFO
                            //	SetMaxInterruptForTemp(MAX30101_IS_DIE_TEMP_RDY);
  SetMaxFIFOConfig(FIFO_CONFIG_MODE);
  //	//Multi-LED Mode Control Registers only if multi mode set time slot for leds
  //	  SetMaxMultiModeTimeSlot(TimeSlot1,MAX30101_LED_MODE_CONTROL_REG_1);
  //	  SetMaxMultiModeTimeSlot(TimeSlot2,MAX30101_LED_MODE_CONTROL_REG_2);
  //	  //Proximity Mode
  setLedProximity_ModePA(PILOT_PA, MAX30101_LED_PILOT_PA);
  SetMaxProximityThreshold(Proxi_Threshold, MAX30101_Proximity_Interrupt_Threshold);
}

void PulseOximeter_proxymode() {
  // Proximity Mode
  setLedProximity_ModePA(PILOT_PA, MAX30101_LED_PILOT_PA);
  SetMaxProximityThreshold(Proxi_Threshold, MAX30101_Proximity_Interrupt_Threshold);
}

void PulseOximeter_shutdown() {
  setMode(SPO2_MODE);    // MAX30101_MODE_SPO2_HR  //MAX30101_MODE_HRONLY //SOP2_MODE
  setLedCurrentRED(MAX30101_LED_CURR_0MA, MAX30101_REG_LED_1_PA);
  setLedCurrentIR(MAX30101_LED_CURR_0MA, MAX30101_REG_LED_2_PA);    // (50mA,27.1mA)
  //  setLedProximity_ModePA(SHUTDOWN_PA,MAX30101_LED_PILOT_PA);
  // SetMaxProximityThreshold(Proxi_Threshold,MAX30101_Proximity_Interrupt_Threshold);
}

void begin(void) {
  threshold = BEATDETECTOR_MIN_THRESHOLD;
  beatPeriod = 0;
  lastMaxValue = 0;
  tsLastBeat = 0;
  setMode(SPO2_MODE);                        // Default mode is MAX30100 as HR mode ONLY
  setLedsPulseWidth(DEFAULT_PULSE_WIDTH);    // DEFAULT_PULSE_WIDTH=118 (117.78)	PULSE_WIDTH_18_BIT 411 (410.75)
  setSamplingRate(DEFAULT_SAMPLING_RATE);    // 100HZ
  Setspo2adcRange(DEFAULT_ADC_RANGE);
  //    setLedsCurrent(DEFAULT_IR_LED_CURRENT, DEFAULT_RED_LED_CURRENT);    // 50mA
  setLedCurrentIR(DEFAULT_IR_LED_CURRENT, MAX30101_REG_LED_1_PA);    // (50mA,27.1mA)
  setLedCurrentRED(DEFAULT_RED_LED_CURRENT, MAX30101_REG_LED_2_PA);
  //  	setLedCurrentGREEN(DEFAULT_GREEN_LED_CURRENT, MAX30101_REG_LED_3_PA);
}

void setMode(enum Mode mode) {
  uint8_t previous;
  maxim_max30102_read_reg(MAX30101_REG_MODE_CONFIGURATION, &previous);
  maxim_max30102_write_reg(MAX30101_REG_MODE_CONFIGURATION, (previous & 0x00) | mode);    // F8
}
void setLedsPulseWidth(enum LEDPulseWidth ledPulseWidth) {
  uint8_t previous;
  maxim_max30102_read_reg(MAX30101_REG_SPO2_CONFIGURATION, &previous);
  maxim_max30102_write_reg(MAX30101_REG_SPO2_CONFIGURATION, (previous & 0x7F) | ledPulseWidth);
}

void setSamplingRate(enum SamplingRate samplingRate) {
  uint8_t previous;
  maxim_max30102_read_reg(MAX30101_REG_SPO2_CONFIGURATION, &previous);
  maxim_max30102_write_reg(MAX30101_REG_SPO2_CONFIGURATION, (previous & 0x7F) | (samplingRate << 2));
}
void Setspo2adcRange(uint8_t adcrange) {
  uint8_t previous;
  maxim_max30102_read_reg(MAX30101_REG_SPO2_CONFIGURATION, &previous);
  maxim_max30102_write_reg(MAX30101_REG_SPO2_CONFIGURATION, (previous & 0x7F) | (adcrange << 5));
}
void setLedCurrentIR(enum LEDCurrent irLedCurrent, uint8_t address) {
  uint8_t previous;
  maxim_max30102_read_reg(address, &previous);
  maxim_max30102_write_reg(address, (previous & 0x00) | irLedCurrent);
}
void setLedCurrentRED(enum LEDCurrent redLedCurrent, uint8_t address) {
  uint8_t previous;
  maxim_max30102_read_reg(address, &previous);
  maxim_max30102_write_reg(address, (previous & 0x00) | redLedCurrent);
}
void setLedCurrentGREEN(enum LEDCurrent greenLedCurrent, uint8_t address) {
  uint8_t previous;
  maxim_max30102_read_reg(address, &previous);
  maxim_max30102_write_reg(address, (previous & 0x00) | greenLedCurrent);
}
void setLedProximity_ModePA(enum LEDCurrent ProximityPA, uint8_t address) {
  uint8_t previous;
  maxim_max30102_read_reg(address, &previous);
  maxim_max30102_write_reg(address, (previous & 0x00) | ProximityPA);
}

void PulseOximeter_update(void) {
  checkSample();
  // checkCurrentBias();
}

void checkSample(void) {
  static uint32_t tsLastSample;
  uint8_t err_code;
  update();
  if (millis() - tsLastSample > ((1.0 / SAMPLING_FREQUENCY) * 1000.0)) {    // >10ms
    tsLastSample = millis();
    readFifoData(&rawRedValue,&rawIRValue);
    float irACValue    = DCremoverstep((float)rawIRValue);     // filter
    float redACValue   = DCremoverstep((float)rawRedValue);    // filter
    filteredPulseValue = LPFstep((float)rawIRValue);           //-irACValue
    bool beatDetected  = checkForBeat(filteredPulseValue);
    current_ir_value = rawIRValue;
    current_red_value = rawRedValue;
    // nrf_delay_ms(10);

    if (BeatDetector_getRate() > 0) {
      poxState = PULSEOXIMETER_STATE_DETECTING;
      SpO2Calculator_update(-irACValue, redACValue, beatDetected);
    } else if (poxState == PULSEOXIMETER_STATE_DETECTING) {
      poxState = PULSEOXIMETER_STATE_IDLE;
      SpO2Calculator_reset();
    }

    /*
if (getRate() > 0) {
state = PULSEOXIMETER_STATE_DETECTING;
spO2calculator_update(irACValue, redACValue, beatDetected);
} else if (state == PULSEOXIMETER_STATE_DETECTING) {
state = PULSEOXIMETER_STATE_IDLE;
spO2calculator_reset();
}
if (beatDetected && onBeatDetected) {
SpO2Calculator_update();
}
}
    */
  }
}

void update(void) {
  uint8_t fifo_write_pointer, fifo_overflow_counter, fifo_read_pointer;
  maxim_max30102_read_reg(MAX30101_REG_FIFO_WRITE_POINTER, &fifo_write_pointer);    // init FIFO_WRITE_POINTER=0 ;
  maxim_max30102_write_reg(MAX30101_REG_FIFO_WRITE_POINTER, (fifo_write_pointer & 0x00));
  maxim_max30102_read_reg(
      MAX30101_REG_FIFO_OVERFLOW_COUNTER, &fifo_overflow_counter);    // init FIFO_OVERFLOW_COUNTER=0;
  maxim_max30102_write_reg(MAX30101_REG_FIFO_OVERFLOW_COUNTER, (fifo_overflow_counter & 0x00));
  maxim_max30102_read_reg(MAX30101_REG_FIFO_READ_POINTER, &fifo_read_pointer);    // init FIFO_READ_POINTER=0;
  maxim_max30102_write_reg(MAX30101_REG_FIFO_READ_POINTER, (fifo_read_pointer & 0x00));
  //    uint8_t AVIL_SAMPLES = FIFO_WRITE_POINTER - FIFO_READ_POINTER;
  //		for(uint8_t i =0 ; i<= AVIL_SAMPLES; i++ ){
  // Reading data from FIFO register
  //		}
  // readFifoData();                                 // Reading data from FIFO register
}

/*
Function for setting the interrupts
*/
void SetMaxInterrupt(uint8_t interrupt_mode) {
  uint8_t previous;
  maxim_max30102_read_reg(MAX30101_REG_INTERRUPT_ENABLE_1, &previous);
  maxim_max30102_write_reg(MAX30101_REG_INTERRUPT_ENABLE_1, (previous & 0x00) | interrupt_mode);
}

void SetMaxInterruptForTemp(uint8_t interrupt_mode) {
  uint8_t previous;
  maxim_max30102_read_reg(MAX30101_REG_INTERRUPT_ENABLE_2, &previous);
  maxim_max30102_write_reg(MAX30101_REG_INTERRUPT_ENABLE_2, (previous & 0xFF) | interrupt_mode);
}

void SetMaxFIFOConfig(uint8_t fifo_mode) {
  uint8_t previous;
  maxim_max30102_read_reg(MAX30101_REG_FIFO_CONFIGURATION, &previous);
  maxim_max30102_write_reg(MAX30101_REG_FIFO_CONFIGURATION, (previous & 0x00) | fifo_mode);
}

void SetMaxMultiModeTimeSlot(uint8_t ledTimeSlot, uint8_t address) {
  uint8_t previous;
  maxim_max30102_read_reg(address, &previous);
  maxim_max30102_write_reg(address, (previous & 0x00) | ledTimeSlot);
}

void SetMaxProximityThreshold(uint8_t Threshold, uint8_t address) {
  uint8_t previous;
  maxim_max30102_read_reg(address, &previous);
  maxim_max30102_write_reg(address, (previous & 0x00) | Threshold);
}
////////////////////////////////////////////////////////////////// old code end

uint32_t get_current_ir(void) {
  return current_ir_value;
}
uint32_t get_current_red(void) {
  return current_red_value;
}

float get_current_pi_ir(void){
return per_index_ir*100000;
}
float get_current_pi_red(void){
return per_index_red*100000;
}