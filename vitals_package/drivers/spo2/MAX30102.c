#include "MAX30102.h"
#include "max301010_interface.h"
#include "packet_handler.h"
#include "spo2.h"
#include "usbd_ble_nus.h"
#include "state_machine.h"

#define TWI_INSTANCE_ID 1

static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

bool maxim_max30102_init();
bool maxim_max30102_read_fifo(uint32_t *pun_red_led, uint32_t *pun_ir_led);
bool maxim_max30102_reset(void);
bool maxim_max30102_write_reg(uint8_t reg, uint8_t p_content);
bool maxim_max30102_read_reg(uint8_t reg, uint8_t *p_content);
bool readFifoData(uint32_t *pun_red_led, uint32_t *pun_ir_led);

spo2_data_t spo2_read_data[MAX_FIFO_SIZE];
uint8_t num_samples, ble_ecg_transmit_index;
/*
* private variables
*/

/*
* private variables
*/

void max30101_twi_init(void) {
  ret_code_t err_code;

  const nrf_drv_twi_config_t twi_config = {.scl = MAX30101_SCL,
      .sda                                      = MAX30101_SDA,
      .frequency                                = NRF_DRV_TWI_FREQ_100K,
      .interrupt_priority                       = APP_IRQ_PRIORITY_HIGH,
      .clear_bus_init                           = false};

  err_code = nrf_drv_twi_init(&m_twi, &twi_config, NULL, NULL);
  APP_ERROR_CHECK(err_code);

  nrf_drv_twi_enable(&m_twi);
  uint8_t address, sample_data;
  bool detected_device = false;
  for (address = 1; address <= 127; address++) {
    err_code = nrf_drv_twi_rx(&m_twi, address, &sample_data, sizeof(sample_data));
    if (err_code == NRF_SUCCESS) {
      detected_device = true;
      //#ifdef MAX301010_INTERFACE_DEBUG
      NRF_LOG_INFO("TWI device detected at address 0x%x.", address);
      //#endif
    }
    NRF_LOG_FLUSH();
  }

  if (!detected_device) {
#ifdef ACCEL_INTERFACE_DEBUG
    NRF_LOG_INFO("No device was found.");
#endif
    NRF_LOG_FLUSH();
  }
}

/**
 * \brief        Write a value to a MAX30102 register
 * \par          Details
 *               This function writes a value to a MAX30102 register
 *
 * \param[in]    uch_addr    - register address
 * \param[in]    uch_data    - register data
 *
 * \retval       true on success
 */
bool maxim_max30102_write_reg(uint8_t reg, uint8_t p_content) {
  ret_code_t err_code;
  uint8_t s_tx_buf[2];
  s_tx_buf[0] = reg;
  s_tx_buf[1] = p_content;

  // Perform SPI transfer.
  err_code = nrf_drv_twi_tx(&m_twi, MAX30101_DEVICE_ID, s_tx_buf, 2, false);
  APP_ERROR_CHECK(err_code);
#ifdef ACCEL_INTERFACE_DEBUG
  NRF_LOG_DEBUG("Lis Write Reg %x Val %x", reg, p_content);
#endif
  return err_code;
}

/**
 * \brief        Read a MAX30102 register
 * \par          Details
 *               This function reads a MAX30102 register
 *
 * \param[in]    uch_addr    - register address
 * \param[out]   puch_data    - pointer that stores the register data
 *
 * \retval       true on success
 */
bool maxim_max30102_read_reg(uint8_t reg, uint8_t *p_content) {
  ret_code_t err_code;

  err_code = nrf_drv_twi_tx(&m_twi, MAX30101_DEVICE_ID, &reg, 1, true);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_twi_rx(&m_twi, MAX30101_DEVICE_ID, p_content, 1);
  APP_ERROR_CHECK(err_code);
#ifdef ACCEL_INTERFACE_DEBUG
  NRF_LOG_INFO("Lis Read Reg %x Val %x", reg, *p_content);
#endif
  return err_code;
}

bool  maxim_max30102_init() {
  maxim_max30102_write_reg(REG_INTR_ENABLE_1, 0xc0);    // INTR setting
  maxim_max30102_write_reg(REG_INTR_ENABLE_2, 0x00);
  maxim_max30102_write_reg(REG_FIFO_WR_PTR, 0x00);    // FIFO_WR_PTR[4:0]
  maxim_max30102_write_reg(REG_OVF_COUNTER, 0x00);    // OVF_COUNTER[4:0]
  maxim_max30102_write_reg(REG_FIFO_RD_PTR, 0x00);    // FIFO_RD_PTR[4:0]
  maxim_max30102_write_reg(REG_FIFO_CONFIG, 0x0f);    // sample avg = 1, fifo rollover=false, fifo almost full = 17
  maxim_max30102_write_reg(REG_MODE_CONFIG, 0x03);    // 0x02 for Red only, 0x03 for SpO2 mode 0x07 multimode LED
  maxim_max30102_write_reg(
      REG_SPO2_CONFIG, 0x27);    // SPO2_ADC range = 4096nA, SPO2 sample rate (100 Hz), LED pulseWidth (400uS)
  maxim_max30102_write_reg(REG_LED1_PA, 0x24);     // Choose value for ~ 7mA for LED1
  maxim_max30102_write_reg(REG_LED2_PA, 0x24);     // Choose value for ~ 7mA for LED2
  maxim_max30102_write_reg(REG_PILOT_PA, 0x7f);    // Choose value for ~ 25mA for Pilot LED
  return true;
}

bool readFifoData(uint32_t *pun_red_led, uint32_t *pun_ir_led)
{
  uint32_t un_temp;
  unsigned char uch_temp;
  *pun_red_led = 0;
  *pun_ir_led  = 0;
  char ach_i2c_data[6];

  // read and clear status register
  maxim_max30102_read_reg(REG_INTR_STATUS_1, &uch_temp);
  // NRF_LOG_INFO("status1 = %d",uch_temp);
  maxim_max30102_read_reg(REG_INTR_STATUS_2, &uch_temp);
  //  NRF_LOG_INFO("status2 = %d",uch_temp);

  ach_i2c_data[0] = REG_FIFO_DATA;
  // TODO

  ret_code_t err_code;

  err_code = nrf_drv_twi_tx(&m_twi, MAX30101_DEVICE_ID, ach_i2c_data, 1, true);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_twi_rx(&m_twi, MAX30101_DEVICE_ID, ach_i2c_data, 6);
  APP_ERROR_CHECK(err_code);

  un_temp = (unsigned char)ach_i2c_data[0];
  un_temp <<= 16;
  *pun_red_led += un_temp;
  un_temp = (unsigned char)ach_i2c_data[1];
  un_temp <<= 8;
  *pun_red_led += un_temp;
  un_temp = (unsigned char)ach_i2c_data[2];
  *pun_red_led += un_temp;

  un_temp = (unsigned char)ach_i2c_data[3];
  un_temp <<= 16;
  *pun_ir_led += un_temp;
  un_temp = (unsigned char)ach_i2c_data[4];
  un_temp <<= 8;
  *pun_ir_led += un_temp;
  un_temp = (unsigned char)ach_i2c_data[5];
  *pun_ir_led += un_temp;
  *pun_red_led &= 0x03FFFF;    // Mask MSB [23:18]
  *pun_ir_led &= 0x03FFFF;     // Mask MSB [23:18]
  spo2_read_data[num_samples].samples[0] = (unsigned char)ach_i2c_data[3]&0x03;
  spo2_read_data[num_samples].samples[1] = (unsigned char)ach_i2c_data[4]&0xFF;
  spo2_read_data[num_samples].samples[2] = (unsigned char)ach_i2c_data[5]&0xFF;
  num_samples++;
  if (num_samples >= 6) {
    if (spo2_read_active == true && current_state.device_state == DEVICE_STATE_RECORDING) {
      ble_ecg_transmit_index = 0;
      package_spo2_graph_data();
      ble_response_ready = true;
      process_outgoing();
      //    ble_nus_data_send(&m_nus,ble_transmit_buffer,&len,m_conn_handle);
      memset(&spo2_read_data, 0, sizeof(spo2_data_t) * MAX_FIFO_SIZE);
      NRF_LOG_INFO("test");
    }
    num_samples = 0;
  }

}
bool maxim_max30102_read_fifo(uint32_t *pun_red_led, uint32_t *pun_ir_led)
/**
 * \brief        Read a set of samples from the MAX30102 FIFO register
 * \par          Details
 *               This function reads a set of samples from the MAX30102 FIFO register
 *
 * \param[out]   *pun_red_led   - pointer that stores the red LED reading data
 * \param[out]   *pun_ir_led    - pointer that stores the IR LED reading data
 *
 * \retval       true on success
 */
{
  uint32_t un_temp;
  unsigned char uch_temp;
  *pun_red_led = 0;
  *pun_ir_led  = 0;
  char ach_i2c_data[6];

  // read and clear status register
  maxim_max30102_read_reg(REG_INTR_STATUS_1, &uch_temp);
  // NRF_LOG_INFO("status1 = %d",uch_temp);
  maxim_max30102_read_reg(REG_INTR_STATUS_2, &uch_temp);
  //  NRF_LOG_INFO("status2 = %d",uch_temp);

  ach_i2c_data[0] = REG_FIFO_DATA;
  // TODO

  ret_code_t err_code;

  err_code = nrf_drv_twi_tx(&m_twi, MAX30101_DEVICE_ID, ach_i2c_data, 1, true);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_twi_rx(&m_twi, MAX30101_DEVICE_ID, ach_i2c_data, 6);
  APP_ERROR_CHECK(err_code);

  //  if(i2c.write(I2C_WRITE_ADDR, ach_i2c_data, 1, true)!=0)
  //    return false;
  //  if(i2c.read(I2C_READ_ADDR, ach_i2c_data, 6, false)!=0)
  //  {
  //    return false;
  //  }
  un_temp = (unsigned char)ach_i2c_data[0];
  un_temp <<= 16;
  *pun_red_led += un_temp;
  un_temp = (unsigned char)ach_i2c_data[1];
  un_temp <<= 8;
  *pun_red_led += un_temp;
  un_temp = (unsigned char)ach_i2c_data[2];
  *pun_red_led += un_temp;

  un_temp = (unsigned char)ach_i2c_data[3];
  un_temp <<= 16;
  *pun_ir_led += un_temp;
  un_temp = (unsigned char)ach_i2c_data[4];
  un_temp <<= 8;
  *pun_ir_led += un_temp;
  un_temp = (unsigned char)ach_i2c_data[5];
  *pun_ir_led += un_temp;
  *pun_red_led &= 0x03FFFF;    // Mask MSB [23:18]
  *pun_ir_led &= 0x03FFFF;     // Mask MSB [23:18]
  if (spo2_read_active == true) {
  spo2_read_data[num_samples].samples[0] = (unsigned char)ach_i2c_data[3]&0x03;
  spo2_read_data[num_samples].samples[1] = (unsigned char)ach_i2c_data[4]&0xFF;
  spo2_read_data[num_samples].samples[2] = (unsigned char)ach_i2c_data[5]&0xFF;
  spo2_read_data[num_samples].sample_valid = true;//(unsigned char)ach_i2c_data[5]&0xFF;
/////////////////////////////////
    num_samples++;
  if(num_samples>=15)
  {
  while(num_samples>=5)
  {
 
      package_spo2_graph_data();
      ble_response_ready = true;
      process_outgoing();
  }

  }

///////////////
  /*
  NRF_LOG_INFO("gr =%d",(ach_i2c_data[3]&0x03)|(ach_i2c_data[4]&0xFF)|(ach_i2c_data[5]&0xFF));
  if (num_samples >= 6) {
      package_spo2_graph_data();
      ble_response_ready = true;
      process_outgoing();
      //    ble_nus_data_send(&m_nus,ble_transmit_buffer,&len,m_conn_handle);
      memset(&spo2_read_data, 0, sizeof(spo2_data_t) * MAX_FIFO_SIZE);
    num_samples = 0;
  } */
  }
  return true;
}

bool maxim_max30102_reset()
/**
 * \brief        Reset the MAX30102
 * \par          Details
 *               This function resets the MAX30102
 *
 * \param        None
 *
 * \retval       true on success
 */
{
  maxim_max30102_write_reg(REG_MODE_CONFIG, 0x40);

  return true;
}

