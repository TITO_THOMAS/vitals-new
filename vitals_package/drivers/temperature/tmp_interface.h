/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _TMP_INTERFACE_H
#define _TMP_INTERFACE_H

#include "bio2board.h"
#include "nrf_drv_twi.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
#define TMP_ALERT 28
#define TMP_SDA 26
#define TMP_SCL 27
extern volatile bool tmp_alert_pin_flag;
ret_code_t tmp_write_register(uint8_t reg, uint16_t value);
ret_code_t tmp_read_register(uint8_t reg, uint16_t * value);
void tmp_power_on();
void tmp_power_off();
void tmp_gpio_config();
void tmp_twi_init();
ret_code_t tmp_gpiote_init();
#endif /* _LIS_INTERFACE_H */