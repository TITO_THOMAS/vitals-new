/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _TMP_H_
#define _TMP_H_

#include "tmp_interface.h"
#include <stdbool.h>

#define TEMP 0x00
#define CFGR 0x01
#define HIGH_LIM 0x02
#define LOW_LIM 0x03
#define EEPROM_UL0x04
#define EEPROM1 0x05
#define EEPROM2 0x06
#define EEPROM3 0x07
#define EEPROM4 0x08
#define DEVICE_ID 0x0f


#define TEMP_CONFIG_MASK 0x01a0
#define DATA_RDY 0x0050
#define TEMP116_DEVICE_ID 0x48

/* Exported types ------------------------------------------------------------*/


/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern volatile bool temperature_read;
bool temperature_init();
void temperature_check_interrupts();
void temperature_setup_double_tap();
float get_temperature();
void temperature_process_fifo_interrupt();
void temperature_disable_fifo();
void temperature_setup_fifo();
void temperature_power_down();
extern void tmp_device_id();
extern void tmp_temp_cfg();
extern void temperature_measure();
float get_current_temperature(void);

#endif /* _LIS_H_ */