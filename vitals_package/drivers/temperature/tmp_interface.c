/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "tmp_interface.h"
#include "debug_config.h"
#include "nrf_drv_gpiote.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TMP_WRITE_MASK 0x00
#define TMP_READ_MASK 0x10

#define TMP_TWI_INSTANCE_ID 0
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile bool tmp_alert_pin_flag = false;

static const nrf_drv_twi_t m_tmp_twi = NRF_DRV_TWI_INSTANCE(TMP_TWI_INSTANCE_ID);
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
 * @brief TWI initialization.
 */
void tmp_twi_init(void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t tmp_twi_config = {.scl = TMP_SCL,
        .sda = TMP_SDA,
        .frequency = NRF_DRV_TWI_FREQ_100K,
        .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
        .clear_bus_init = false};

    err_code = nrf_drv_twi_init(&m_tmp_twi, &tmp_twi_config, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_tmp_twi);
    uint8_t address, sample_data;
    bool detected_device = false;
    for (address = 1; address <= 127; address++)
    {
        err_code = nrf_drv_twi_rx(&m_tmp_twi, address, &sample_data, sizeof(sample_data));
        if (err_code == NRF_SUCCESS)
        {
            detected_device = true;
#ifdef ACCEL_INTERFACE_DEBUG
            NRF_LOG_INFO("TWI device detected at address 0x%x.", address);
#endif
        }
        NRF_LOG_FLUSH();
    }

    if (!detected_device)
    {
#ifdef ACCEL_INTERFACE_DEBUG
        NRF_LOG_INFO("No device was found.");
#endif
        NRF_LOG_FLUSH();
    }
}

ret_code_t tmp_read_register(uint8_t reg, uint16_t * p_content)
{
    ret_code_t err_code;
    uint8_t read_arr[2];

    err_code = nrf_drv_twi_tx(&m_tmp_twi, 0x48, &reg, 1, true);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_twi_rx(&m_tmp_twi, 0x48, read_arr, 2);
    APP_ERROR_CHECK(err_code);

    * p_content = ((read_arr[0]<<8) &0xff00) | (read_arr[1]&0xff);
#ifdef ACCEL_INTERFACE_DEBUG
    NRF_LOG_DEBUG("tmp Read Reg %x Val %x", reg, *p_content);
#endif
    return err_code;
}

/**@brief Writes one or more consecutive rgisters to the device.
 *
 * @param[in] first_reg     Address of the first register.
 * @param[in] p_contents    Data to write.
 * @param[in] num_regs      Length of data (bytes)/number of registers that should be written.
 */
ret_code_t tmp_write_register(uint8_t reg, uint16_t p_contents)
{
    ret_code_t err_code;
    uint8_t s_tx_buf[3];

    // Data to send: Register address + contents.
    s_tx_buf[0] = reg;
    s_tx_buf[1]=(p_contents>>8);
    s_tx_buf[2]=p_contents;

    // Perform SPI transfer.
    err_code = nrf_drv_twi_tx(&m_tmp_twi, 0x48, s_tx_buf, 3, false);
    APP_ERROR_CHECK(err_code);
#ifdef ACCEL_INTERFACE_DEBUG
    NRF_LOG_DEBUG("tmp Write Reg %x Val %x", reg, p_contents);
#endif
    return err_code;
}

void tmp_power_on() {  }

void tmp_power_off()
{

}

void tmp_gpio_config()
{

    nrf_gpio_cfg_sense_input(TMP_ALERT, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_SENSE_HIGH);
    //  nrf_gpio_cfg_sense_input(TMP_INT1, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_SENSE_HIGH);
}

static void tmp_gpio_interrupt_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    tmp_alert_pin_flag = true;

}

ret_code_t tmp_gpiote_init()
{
    ret_code_t err_code;
    if (!nrfx_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        APP_ERROR_CHECK(err_code);
    }
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;

    err_code = nrf_drv_gpiote_in_init(TMP_ALERT, &in_config, tmp_gpio_interrupt_handler);

    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_event_enable(TMP_ALERT, true);

    APP_ERROR_CHECK(err_code);
}

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */