/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "temperature.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include <math.h>
#define DEBUG_ACCEL 1
#if DEBUG_ACCEL
#define msg(x ...) NRF_LOG_INFO(x)
#define msg_hexdump(x ...) NRF_LOG_HEXDUMP_INFO(x)
#else
#define msg(x ...)
#define msg_hexdump(x ...)
#endif

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
/*!
 * 
 * 
 * 
 * 
 */
#define TMP_REG_T 0x00
#define TMP_REG_C 0x01 /**< 1-axis acceleration data. Low value */
#define TMP_REG_HL 0x02 /**< 1-axis acceleration data. High value */
#define TMP_REG_LL 0x03 /**< 2-axis acceleration data. Low value */
#define TMP_REG_ID 0x0F /**< 2-axis acceleration data. High value */

static uint8_t reg_current = 0x00; /**< 3-axis acceleration data. Low value */
static uint8_t mode          = 2;      // MOD  0: continuous conversion, 1: shutdown, 2: continuous conversion, 3: one-shot
static uint8_t conv          = 4;      // CONV 0: afap, 1: 0.125s, 2: 0.250s, 3: 0.500s, 4: 1s, 5: 4s, 6: 8s, 7: 16s
static uint8_t avg           = 0;      // AVG  0: no_avg (15.5ms), 1: 8 avg(125ms), 2: 32 avg(500ms), 3: 64 avg(1000ms) 
static uint8_t flags         = 0;      // FLAGS : last three bits representing: [2: hi alert, 1: lo alert, 0: data ready]
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static float current_temperature_level;
volatile bool temperature_read = false;

/* Private function prototypes -----------------------------------------------*/
void calibrate_tmp(uint8_t num_samples, uint8_t sample_delay);
void measure_activity_level(uint8_t samples_available);
void temperature_setup_double_tap();
void temperature_setup_fifo();
void temperature_disable_double_tap();
void temperature_disable_fifo();
void tmp_device_id();
void tmp_temp_cfg();
void temperature_measure();
/* Private functions ---------------------------------------------------------*/

static float current_temperature=0;

float get_current_temperature(void);
float get_current_temperature(void)
{
return ((current_temperature*9/5)+32);
}

bool temperature_init()
{
    uint8_t id;
    ret_code_t err_code;
    tmp_gpio_config();
    tmp_power_on();
    tmp_gpiote_init();
    tmp_twi_init();
    nrf_delay_ms(100);
//    err_code = tmp_read_register(TMP_REG_WHOAMI, &id);
//    APP_ERROR_CHECK(err_code);
//    msg("TMP Who Am I Read %x Expected %x", id, WHO_AM_I_CONTENT);
    nrf_delay_ms(10);
   tmp_device_id();
   tmp_temp_cfg();
}

void tmp_device_id()
{
uint16_t temp;
tmp_read_register(DEVICE_ID,&temp);
NRF_LOG_INFO("DEVICE ID = %d",temp);
}

void tmp_temp_cfg()
{
tmp_write_register(CFGR,TEMP_CONFIG_MASK);
}



void temperature_power_down()
{
    msg("Powering down TMP");
    tmp_power_off();
}

void temperature_measure()
{

 uint16_t temp;
 unsigned char temp_array[10];
 tmp_read_register(CFGR, &temp);

     if(temp & 0x2000)
       {
       NRF_LOG_FLUSH();
        static float tmp_value=0;
        temp=0;
        uint16_t tp;
        tmp_read_register(TEMP,&tp);
        current_temperature =   tp * 0.0078125;
       }


}
/**
 * @brief  Main program
 * @param  None
 * @retval None
 */