/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LIS_INTERFACE_H
#define _LIS_INTERFACE_H

#include "bio2board.h"
#include "nrf_drv_twi.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern volatile bool fifo_wtm_interrupt;
ret_code_t lis_spi_init();
ret_code_t lis_write_register(uint8_t reg, uint8_t value);
ret_code_t lis_read_register(uint8_t reg, uint8_t * value);
void lis_power_on();
void lis_power_off();
void lis_gpio_config();
void twi_init();
ret_code_t lis_gpiote_init();
#endif /* _LIS_INTERFACE_H */