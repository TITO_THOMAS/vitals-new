/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "lis_interface.h"
#include "debug_config.h"
#include "nrf_drv_gpiote.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define LIS_WRITE_MASK 0x00
#define LIS_READ_MASK 0x10

#define LIS_TWI_INSTANCE_ID 0
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static volatile bool lis_transfer_complete = false;
volatile bool fifo_wtm_interrupt = false;

static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(LIS_TWI_INSTANCE_ID);
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
 * @brief TWI initialization.
 */
void twi_init(void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_config = {.scl = LIS_SCK,
        .sda = LIS_MISO_SDA,
        .frequency = NRF_DRV_TWI_FREQ_100K,
        .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
        .clear_bus_init = false};

    err_code = nrf_drv_twi_init(&m_twi, &twi_config, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
    uint8_t address, sample_data;
    bool detected_device = false;
    for (address = 1; address <= 127; address++)
    {
        err_code = nrf_drv_twi_rx(&m_twi, address, &sample_data, sizeof(sample_data));
        if (err_code == NRF_SUCCESS)
        {
            detected_device = true;
#ifdef ACCEL_INTERFACE_DEBUG
            NRF_LOG_INFO("TWI device detected at address 0x%x.", address);
#endif
        }
        NRF_LOG_FLUSH();
    }

    if (!detected_device)
    {
#ifdef ACCEL_INTERFACE_DEBUG
        NRF_LOG_INFO("No device was found.");
#endif
        NRF_LOG_FLUSH();
    }
}

ret_code_t lis_read_register(uint8_t reg, uint8_t * p_content)
{
    ret_code_t err_code;

    err_code = nrf_drv_twi_tx(&m_twi, 0x29, &reg, 1, true);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_twi_rx(&m_twi, 0x29, p_content, 1);
    APP_ERROR_CHECK(err_code);
#ifdef ACCEL_INTERFACE_DEBUG
    NRF_LOG_DEBUG("Lis Read Reg %x Val %x", reg, *p_content);
#endif
    return err_code;
}

/**@brief Writes one or more consecutive rgisters to the device.
 *
 * @param[in] first_reg     Address of the first register.
 * @param[in] p_contents    Data to write.
 * @param[in] num_regs      Length of data (bytes)/number of registers that should be written.
 */
ret_code_t lis_write_register(uint8_t reg, uint8_t p_contents)
{
    ret_code_t err_code;
    uint8_t s_tx_buf[2];

    // Data to send: Register address + contents.
    s_tx_buf[0] = reg;
    s_tx_buf[1] = p_contents;

    // Perform SPI transfer.
    err_code = nrf_drv_twi_tx(&m_twi, 0x29, s_tx_buf, 2, false);
    APP_ERROR_CHECK(err_code);
#ifdef ACCEL_INTERFACE_DEBUG
    NRF_LOG_DEBUG("Lis Write Reg %x Val %x", reg, p_contents);
#endif
    return err_code;
}

void lis_power_on() { nrf_gpio_pin_set(LIS_PWR); }

void lis_power_off()
{
    nrf_gpio_pin_clear(LIS_PWR);
    nrfx_gpiote_in_uninit(LIS_INT1);
    nrf_drv_twi_uninit(&m_twi);
}

void lis_gpio_config()
{
    nrf_gpio_cfg_output(LIS_PWR);
    nrf_gpio_cfg_output(LIS_MOSI); // setting least significant bit of the device address
    nrf_gpio_cfg_output(LIS_CS);
    nrf_gpio_pin_set(LIS_CS);
    nrf_gpio_pin_set(LIS_MOSI);
    nrf_gpio_cfg_sense_input(LIS_INT2, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_SENSE_HIGH);
    //  nrf_gpio_cfg_sense_input(LIS_INT1, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_SENSE_HIGH);
}

static void lis_gpio_interrupt_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    fifo_wtm_interrupt = true;

}

ret_code_t lis_gpiote_init()
{
    ret_code_t err_code;
    if (!nrfx_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        APP_ERROR_CHECK(err_code);
    }
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;

    err_code = nrf_drv_gpiote_in_init(LIS_INT1, &in_config, lis_gpio_interrupt_handler);

    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_event_enable(LIS_INT1, true);

    APP_ERROR_CHECK(err_code);
}

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */