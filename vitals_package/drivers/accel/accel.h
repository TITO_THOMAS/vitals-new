/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LIS_H_
#define _LIS_H_

#include "lis_interface.h"
#include <stdbool.h>

/* Exported types ------------------------------------------------------------*/
typedef enum
{
    ACTIVITY_LEVEL_REST,
    ACTIVITY_LEVEL_LOW,
    ACTIVITY_LEVEL_MODERATE,
    ACTIVITY_LEVEL_INTENSE,
} activity_level_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern volatile bool activity_read;
bool accel_init();
void accel_check_interrupts();
void accel_setup_double_tap();
activity_level_t get_activity_level();
void accel_process_fifo_interrupt();
void accel_disable_fifo();
void accel_setup_fifo();
void accel_power_down();
#endif /* _LIS_H_ */