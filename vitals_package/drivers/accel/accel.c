/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "accel.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include <math.h>
#define DEBUG_ACCEL 1
#if DEBUG_ACCEL
#define msg(x ...) NRF_LOG_INFO(x)
#define msg_hexdump(x ...) NRF_LOG_HEXDUMP_INFO(x)
#else
#define msg(x ...)
#define msg_hexdump(x ...)
#endif

/* Private typedef -----------------------------------------------------------*/
struct
{
    int32_t x_calib;
    int32_t y_calib;
    int32_t z_calib;
} lis_calib_vals;
/* Private define ------------------------------------------------------------*/
/*!
 *  STATUS_REG_AUX register
 *   321OR  1, 2 and 3 axis data overrun. Default value: 0
 *          (0: no overrun has occurred; 1: a new set of data has overwritten
 * the previous ones) 3OR    3 axis data overrun. Default value: 0 (0: no
 * overrun has occurred; 1: a new data for the 3-axis has overwritten the
 * previous one) 2OR    2 axis data overrun. Default value: 0 (0: no overrun has
 * occurred; 1: a new data for the 4-axis has overwritten the previous one) 1OR
 * 1 axis data overrun. Default value: 0 (0: no overrun has occurred; 1: a new
 * data for the 1-axis has overwritten the previous one) 321DA  1, 2 and 3 axis
 * new data available. Default value: 0 (0: a new set of data is not yet
 * available; 1: a new set of data is available) 3DA:   3 axis new data
 * available. Default value: 0 (0: a new data for the 3-axis is not yet
 * available; 1: a new data for the 3-axis is available) 2DA:   2 axis new data
 * available. Default value: 0 (0: a new data for the 2-axis is not yet
 * available; 1: a new data for the 2-axis is available) 1DA    1 axis new data
 * available. Default value: 0 (0: a new data for the 1-axis is not yet
 * available; 1: a new data for the 1-axis is available)
 */
#define LIS3DX_REG_STATUS1 0x07
#define LIS3DX_REG_OUTADC1_L 0x08 /**< 1-axis acceleration data. Low value */
#define LIS3DX_REG_OUTADC1_H 0x09 /**< 1-axis acceleration data. High value */
#define LIS3DX_REG_OUTADC2_L 0x0A /**< 2-axis acceleration data. Low value */
#define LIS3DX_REG_OUTADC2_H 0x0B /**< 2-axis acceleration data. High value */
#define LIS3DX_REG_OUTADC3_L 0x0C /**< 3-axis acceleration data. Low value */
#define LIS3DX_REG_OUTADC3_H 0x0D /**< 3-axis acceleration data. High value */
#define LIS3DX_REG_INTCOUNT                                                                        \
    0x0E                       /**< INT_COUNTER register [IC7, IC6, IC5, IC4, IC3, IC2, IC1, IC0] */
#define LIS3DX_REG_WHOAMI 0x0F /**< Device identification register. [0, 0, 1, 1, 0, 0, 1, 1]  */
/*!
 *  TEMP_CFG_REG
 *  Temperature configuration register.
 *   ADC_PD   ADC enable. Default value: 0
 *            (0: ADC disabled; 1: ADC enabled)
 *   TEMP_EN  Temperature sensor (T) enable. Default value: 0
 *            (0: T disabled; 1: T enabled)
 */
#define LIS3DX_REG_TEMPCFG 0x1F
/*!
 *  CTRL_REG1
 *  [ODR3, ODR2, ODR1, ODR0, LPen, Zen, Yen, Xen]
 *   ODR3-0  Data rate selection. Default value: 00
 *           (0000:50 Hz; Others: Refer to Datasheet Table 26, ?Data rate
 * configuration?) LPen    Low power mode enable. Default value: 0 (0: normal
 * mode, 1: low power mode) Zen     Z axis enable. Default value: 1 (0: Z axis
 * disabled; 1: Z axis enabled) Yen     Y axis enable. Default value: 1 (0: Y
 * axis disabled; 1: Y axis enabled) Xen     X axis enable. Default value: 1 (0:
 * X axis disabled; 1: X axis enabled)
 */
#define LIS3DX_REG_CTRL1 0x20
/*!
 *  CTRL_REG2
 *  [HPM1, HPM0, HPCF2, HPCF1, FDS, HPCLICK, HPIS2, HPIS1]
 *   HPM1-0  High pass filter mode selection. Default value: 00
 *           Refer to Table 29, "High pass filter mode configuration"
 *   HPCF2-1 High pass filter cut off frequency selection
 *   FDS     Filtered data selection. Default value: 0
 *					 (0: internal filter bypassed; 1: data from
 *internal filter sent to output register and FIFO) HPCLICK High pass filter
 *enabled for CLICK function. (0: filter bypassed; 1: filter enabled) HPIS2   X
 *axis enable. Default value: 1 (0: X axis disabled; 1: X axis enabled) HPIS1
 *High pass filter enabled for AOI function on interrupt 1, (0: filter bypassed;
 *1: filter enabled)
 */
#define LIS3DX_REG_CTRL2 0x21
/*!
 *  CTRL_REG3
 *  [I1_CLICK, I1_AOI1, I1_AOI2, I1_DRDY1, I1_DRDY2, I1_WTM, I1_OVERRUN, --]
 *   I1_CLICK    CLICK interrupt on INT1. Default value 0.
 *						   (0: Disable; 1: Enable)
 *   I1_AOI1     AOI1 interrupt on INT1. Default value 0.
 *						   (0: Disable; 1: Enable)
 *   I1_AOI2     AOI2 interrupt on INT1. Default value 0.
 *               (0: Disable; 1: Enable)
 *   I1_DRDY1    DRDY1 interrupt on INT1. Default value 0.
 *               (0: Disable; 1: Enable)
 *   I1_DRDY2    DRDY2 interrupt on INT1. Default value 0.
 *               (0: Disable; 1: Enable)
 *   I1_WTM      FIFO Watermark interrupt on INT1. Default value 0.
 *               (0: Disable; 1: Enable)
 *   I1_OVERRUN  FIFO Overrun interrupt on INT1. Default value 0.
 * 							 (0: Disable; 1: Enable)
 */
#define LIS3DX_REG_CTRL3 0x22
/*!
 *  CTRL_REG4
 *  [BDU, BLE, FS1, FS0, HR, ST1, ST0, SIM]
 *   BDU      Block data update. Default value: 0
 *            (0: continuos update; 1: output registers not updated until MSB
 * and LSB reading) BLE      Big/little endian data selection. Default value 0.
 *            (0: Data LSB @ lower address; 1: Data MSB @ lower address)
 *   FS1-FS0  Full scale selection. default value: 00
 *            (00: +/- 2G; 01: +/- 4G; 10: +/- 8G; 11: +/- 16G)
 *   HR       High resolution output mode: Default value: 0
 *            (0: High resolution disable; 1: High resolution Enable)
 *   ST1-ST0  Self test enable. Default value: 00
 *            (00: Self test disabled; Other: See Table 34)
 *   SIM      SPI serial interface mode selection. Default value: 0
 *            (0: 4-wire interface; 1: 3-wire interface).
 */
#define LIS3DX_REG_CTRL4 0x23
/*!
 *  CTRL_REG5
 *  [BOOT, FIFO_EN, --, --, LIR_INT1, D4D_INT1, 0, 0]
 *   BOOT     Reboot memory content. Default value: 0
 *            (0: normal mode; 1: reboot memory content)
 *   FIFO_EN  FIFO enable. Default value: 0
 *            (0: FIFO disable; 1: FIFO Enable)
 *   LIR_INT1 Latch interrupt request on INT1_SRC register, with INT1_SRC
 * register cleared by reading INT1_SRC itself. Default value: 0. (0: interrupt
 * request not latched; 1: interrupt request latched) D4D_INT1 4D enable: 4D
 * detection is enabled on INT1 when 6D bit on INT1_CFG is set to 1.
 */
#define LIS3DX_REG_CTRL5 0x24

/*!
 *  CTRL_REG6
 *  [I2_CLICKen, I2_INT1, 0, BOOT_I1, 0, --, H_L, -]
 */
#define LIS3DX_REG_CTRL6 0x25
#define LIS3DX_REG_REFERENCE 0x26 /**< REFERENCE/DATACAPTURE **/
/*!
 *  STATUS_REG
 *  [ZYXOR, ZOR, YOR, XOR, ZYXDA, ZDA, YDA, XDA]
 *   ZYXOR    X, Y and Z axis data overrun. Default value: 0
 *            (0: no overrun has occurred; 1: a new set of data has overwritten
 * the previous ones) ZOR      Z axis data overrun. Default value: 0 (0: no
 * overrun has occurred; 1: a new data for the Z-axis has overwritten the
 * previous one) YOR      Y axis data overrun. Default value: 0 (0: no overrun
 * has occurred;  1: a new data for the Y-axis has overwritten the previous one)
 *   XOR      X axis data overrun. Default value: 0
 *            (0: no overrun has occurred; 1: a new data for the X-axis has
 * overwritten the previous one) ZYXDA    X, Y and Z axis new data available.
 * Default value: 0 (0: a new set of data is not yet available; 1: a new set of
 * data is available) ZDA      Z axis new data available. Default value: 0 (0: a
 * new data for the Z-axis is not yet available; 1: a new data for the Z-axis is
 * available) YDA      Y axis new data available. Default value: 0 (0: a new
 * data for the Y-axis is not yet available; 1: a new data for the Y-axis is
 * available)
 */
#define LIS3DX_REG_STATUS2 0x27
#define LIS3DX_REG_OUT_X_L 0x28 /**< X-axis acceleration data. Low value */
#define LIS3DX_REG_OUT_X_H 0x29 /**< X-axis acceleration data. High value */
#define LIS3DX_REG_OUT_Y_L 0x2A /**< Y-axis acceleration data. Low value */
#define LIS3DX_REG_OUT_Y_H 0x2B /**< Y-axis acceleration data. High value */
#define LIS3DX_REG_OUT_Z_L 0x2C /**< Z-axis acceleration data. Low value */
#define LIS3DX_REG_OUT_Z_H 0x2D /**< Z-axis acceleration data. High value */
/*!
 *  FIFO_CTRL_REG
 *  [FM1, FM0, TR, FTH4, FTH3, FTH2, FTH1, FTH0]
 *   FM1-FM0  FIFO mode selection. Default value: 00 (see Table 44)
 *   TR       Trigger selection. Default value: 0
 *            0: Trigger event liked to trigger signal on INT1
 *            1: Trigger event liked to trigger signal on INT2
 *   FTH4:0   Default value: 0
 */
#define LIS3DX_REG_FIFOCTRL 0x2E
#define LIS3DX_REG_FIFOSRC                                                                         \
    0x2F /**< FIFO_SRC_REG [WTM, OVRN_FIFO, EMPTY, FSS4, FSS3, FSS2, FSS1, FSS0] \ \                                                                                                 \
          */
/*!
 *  INT1_CFG
 *  [AOI, 6D, ZHIE/ZUPE, ZLIE/ZDOWNE, YHIE/YUPE, XHIE/XUPE, XLIE/XDOWNE]
 *   AOI         And/Or combination of Interrupt events. Default value: 0. Refer
 * to Datasheet Table 48, "Interrupt mode" 6D          6 direction detection
 * function enabled. Default value: 0. Refer to Datasheet Table 48, "Interrupt
 * mode" ZHIE/ZUPE   Enable interrupt generation on Z high event or on Direction
 * recognition. Default value: 0. (0: disable interrupt request; 1: enable
 * interrupt request) ZLIE/ZDOWNE Enable interrupt generation on Z low event or
 * on Direction recognition. Default value: 0. YHIE/YUPE   Enable interrupt
 * generation on Y high event or on Direction recognition. Default value: 0. (0:
 * disable interrupt request; 1: enable interrupt request.) YLIE/YDOWNE Enable
 * interrupt generation on Y low event or on Direction recognition. Default
 * value: 0. (0: disable interrupt request; 1: enable interrupt request.)
 *   XHIE/XUPE   Enable interrupt generation on X high event or on Direction
 * recognition. Default value: 0. (0: disable interrupt request; 1: enable
 * interrupt request.) XLIE/XDOWNE Enable interrupt generation on X low event or
 * on Direction recognition. Default value: 0. (0: disable interrupt request; 1:
 * enable interrupt request.)
 */
#define LIS3DX_REG_INT1CFG 0x30
/*!
 *  INT1_SRC
 *   [0, IA, ZH, ZL, YH, YL, XH, XL]
 *    IA  Interrupt active. Default value: 0
 *        (0: no interrupt has been generated; 1: one or more interrupts have
 * been generated) ZH  Z high. Default value: 0 (0: no interrupt, 1: Z High
 * event has occurred) ZL  Z low. Default value: 0 (0: no interrupt; 1: Z Low
 * event has occurred) YH  Y high. Default value: 0 (0: no interrupt, 1: Y High
 * event has occurred) YL  Y low. Default value: 0 (0: no interrupt, 1: Y Low
 * event has occurred) XH  X high. Default value: 0 (0: no interrupt, 1: X High
 * event has occurred) XL  X low. Default value: 0 (0: no interrupt, 1: X Low
 * event has occurred)
 *
 *    Interrupt 1 source register. Read only register.
 *    Reading at this address clears INT1_SRC IA bit (and the interrupt signal
 * on INT 1 pin) and allows the refreshment of data in the INT1_SRC register if
 * the latched option  was chosen.
 */
#define LIS3DX_REG_INT1SRC 0x31
#define LIS3DX_REG_INT1THS 0x32 /**< INT1_THS register [0, THS6, THS5, THS4, THS3, THS1, THS0] */
#define LIS3DX_REG_INT1DUR 0x33 /**< INT1_DURATION [0, D6, D5, D4, D3, D2, D1, D0] */
/*!
 *  CLICK_CFG
 *   [--, --, ZD, ZS, YD, YS, XD, XS]
 *   ZD  Enable interrupt double CLICK-CLICK on Z axis. Default value: 0
 *       (0: disable interrupt request;
 *        1: enable interrupt request on measured accel. value higher than
 * preset threshold) ZS  Enable interrupt single CLICK-CLICK on Z axis. Default
 * value: 0 (0: disable interrupt request; 1: enable interrupt request on
 * measured accel. value higher than preset threshold) YD  Enable interrupt
 * double CLICK-CLICK on Y axis. Default value: 0 (0: disable interrupt request;
 *        1: enable interrupt request on measured accel. value higher than
 * preset threshold) YS  Enable interrupt single CLICK-CLICK on Y axis. Default
 * value: 0 (0: disable interrupt request; 1: enable interrupt request on
 * measured accel. value higher than preset threshold) XD  Enable interrupt
 * double CLICK-CLICK on X axis. Default value: 0 (0: disable interrupt request;
 * 1: enable interrupt request on measured accel. value higher than preset
 * threshold) XS  Enable interrupt single CLICK-CLICK on X axis. Default value:
 * 0 (0: disable interrupt request; 1: enable interrupt request on measured
 * accel. value higher than preset threshold)
 */
#define LIS3DX_REG_CLICKCFG 0x38
/*!
 *  CLICK_SRC
 *   [-, IA, DCLICK, SCLICK, Sign, Z, Y, X]
 *   IA  Interrupt active. Default value: 0
 *       (0: no interrupt has been generated; 1: one or more interrupts have
 * been generated) DCLICK  Double CLICK-CLICK enable. Default value: 0 (0:double
 * CLICK-CLICK detection disable, 1: double CLICK-CLICK detection enable) SCLICK
 * Single CLICK-CLICK enable. Default value: 0 (0:Single CLICK-CLICK detection
 * disable, 1: single CLICK-CLICK detection enable) Sign    CLICK-CLICK Sign.
 *           (0: positive detection, 1: negative detection)
 *   Z       Z CLICK-CLICK detection. Default value: 0
 *           (0: no interrupt, 1: Z High event has occurred)
 *   Y       Y CLICK-CLICK detection. Default value: 0
 *           (0: no interrupt, 1: Y High event has occurred)
 *   X       X CLICK-CLICK detection. Default value: 0
 *           (0: no interrupt, 1: X High event has occurred)
 */
#define LIS3DX_REG_CLICKSRC 0x39
/*!
 *  CLICK_THS
 *   [-, Ths6, Ths5, Ths4, Ths3, Ths2, Ths1, Ths0]
 *   Ths6-Ths0  CLICK-CLICK threshold. Default value: 000 0000
 */
#define LIS3DX_REG_CLICKTHS 0x3A
/*!
 *  TIME_LIMIT
 *   [-, TLI6, TLI5, TLI4, TLI3, TLI2, TLI1, TLI0]
 *   TLI7-TLI0  CLICK-CLICK Time Limit. Default value: 000 0000
 */
#define LIS3DX_REG_TIMELIMIT 0x3B
/*!
 *  TIME_LATANCY
 *   [-, TLA6, TLIA5, TLA4, TLA3, TLA2, TLA1, TLA0]
 *   TLA7-TLA0  CLICK-CLICK Time Latency. Default value: 000 0000
 */
#define LIS3DX_REG_TIMELATENCY 0x3C
/*!
 *  TIME_WINDOW
 *   [TW7, TW6, TW5, TW4, TW3, TW2, TW1, TW0]
 *   TW7-TW0  CLICK-CLICK Time window
 */
#define LIS3DX_REG_TIMEWINDOW 0x3D

#define WHO_AM_I_CONTENT 0x33

#define LIS3DX_DATARATE_400_HZ 0x70
#define LIS3DX_LOW_POWER_ENABLE 0x08
#define LIS3DX_XYZ_ENABLE 0x07
#define LIS3DX_DATA_RATE_1HZ 0x10
#define LIS3DX_DATA_RATE_25HZ 0x30
#define LIS3DX_POWER_DOWN 0x00

#define LIS3DX_I2_CLICK 0x80
#define LIS3DX_I1_WTM 0x04

#define I1_CFG_6D_Z_AXIS 0x70
#define I1_THS_VAL 0x40
#define I1_DURATION_VAL 0x01

#define CLICK_ENABLE_XYZ_AXIS 0x2A
#define CLICK_THS 0x2F
#define CLICK_TIME_LIMIT_MAX 0xFF
#define CLICK_TIME_LATENCY 0x64
#define CLICK_TIME_WINDOW 0x1F

#define FIFO_ENABLE 0x40
#define STREAM_MODE 0x80
#define FIFO_WATERMARK_31_SAMPLES 0x1E
#define FIFO_WATERMARK_25_SAMPLES 0x19

#define FIFO_SRC_WTM_MASK 0x80
#define FIFO_SRC_OVERRUN_MASK 0x40
#define FIFO_SRC_EMPTY_MASK 0x20
#define FIFO_SRC_FSS_MASK 0x1F

#define DOUBLE_TAP_INT_MASK 0x20

// Activity thresholds
#define INTENSE_THRESHOLD 55
#define MODERATE_THRESHOLD 33
#define LOW_THRESHOLD 10

#define LIS_MAX_FIFO_SIZE 32


/*TODO
 * 1. Saperate double tap and fifo interrupt setup
 * 2. Check for only double tap at startup
 * 3. Setup double tap when going to sleep mode
 */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static char const * activity_level_str[] = {
    "REST",
    "LOW",
    "MODERATE",
    "INTENSE",
};
static activity_level_t current_activity_level;
volatile bool activity_read = false;

/* Private function prototypes -----------------------------------------------*/
void calibrate_lis(uint8_t num_samples, uint8_t sample_delay);
void measure_activity_level(uint8_t samples_available);
void accel_setup_double_tap();
void accel_setup_fifo();
void accel_disable_double_tap();
void accel_disable_fifo();
uint8_t check_fifo_status(uint8_t source);
/* Private functions ---------------------------------------------------------*/
bool accel_init()
{
    uint8_t id;
    ret_code_t err_code;
    lis_gpio_config();
    lis_power_on();
    lis_gpiote_init();
    twi_init();
    nrf_delay_ms(100);
    accel_check_interrupts();
    err_code = lis_read_register(LIS3DX_REG_WHOAMI, &id);
    APP_ERROR_CHECK(err_code);
    msg("LIS Who Am I Read %x Expected %x", id, WHO_AM_I_CONTENT);
    calibrate_lis(50, 10);
    nrf_delay_ms(10);
    accel_disable_fifo();
    nrf_delay_ms(10);
    accel_setup_fifo();
    nrf_delay_ms(10);
    accel_process_fifo_interrupt();
}

uint8_t check_fifo_status(uint8_t source)
{
    bool wtm_status = false;
    bool overrun_status = false;
    bool empty_status = false;
    uint8_t samples_available = 0;

    if (source & FIFO_SRC_WTM_MASK)
        wtm_status = true;
    if (source & FIFO_SRC_EMPTY_MASK)
        empty_status = true;
    if (source & FIFO_SRC_OVERRUN_MASK)
        overrun_status = true;
    samples_available = source & FIFO_SRC_FSS_MASK;

  //  NRF_LOG_INFO("FIFO Src WTM %d Empty %d Overrun %d", wtm_status, empty_status, overrun_status);
  //  NRF_LOG_INFO("Fifo samples %d", samples_available);
    return samples_available;
}

void accel_check_interrupts()
{
    uint8_t source;
    ret_code_t err_code;
    if (nrf_gpio_pin_read(LIS_INT2))
    {
        msg("Tap Interrupt Active");
        err_code = lis_read_register(LIS3DX_REG_CLICKSRC, &source);
        APP_ERROR_CHECK(err_code);
        if (source & DOUBLE_TAP_INT_MASK)
            msg("Double Tap");
        nrf_delay_ms(100);
    }
    if (nrf_gpio_pin_read(LIS_INT1))
    {
    }
}

void calibrate_lis(uint8_t num_samples, uint8_t sample_delay)
{
    ret_code_t err_code;
    int32_t x_sum = 0, y_sum = 0, z_sum = 0;
    int8_t *x, *y, *z;
    uint8_t x_temp, y_temp, z_temp;
    x = &x_temp;
    y = &y_temp;
    z = &z_temp;
    err_code = lis_write_register(
        LIS3DX_REG_CTRL1, LIS3DX_DATARATE_400_HZ | LIS3DX_LOW_POWER_ENABLE | LIS3DX_XYZ_ENABLE);
    APP_ERROR_CHECK(err_code);
    for (int i = 0; i < num_samples; i++)
    {
        //  NRF_WDT->RR[0] = WDT_RR_RR_Reload;

        err_code = lis_read_register(LIS3DX_REG_OUT_X_H, &x_temp);
        APP_ERROR_CHECK(err_code);
        x_sum = x_sum + *x;
        err_code = lis_read_register(LIS3DX_REG_OUT_Y_H, &y_temp);
        APP_ERROR_CHECK(err_code);
        y_sum = y_sum + *y;
        err_code = lis_read_register(LIS3DX_REG_OUT_Z_H, &z_temp);
        APP_ERROR_CHECK(err_code);
        z_sum = z_sum + *z;
        nrf_delay_ms(sample_delay);
        //        NRF_LOG_INFO("LIS Calib Vals %d, %d, %d", x_temp, y_temp,
        //        z_temp);
    }
    lis_calib_vals.x_calib = x_sum / num_samples;
    lis_calib_vals.y_calib = y_sum / num_samples;
    lis_calib_vals.z_calib = z_sum / num_samples;

    msg("LIS Calib Vals %d, %d, %d", lis_calib_vals.x_calib, lis_calib_vals.y_calib,
        lis_calib_vals.z_calib);
}

void measure_activity_level(uint8_t samples_available)
{
    activity_level_t last_activity_level = current_activity_level;
    ret_code_t err_code;
    int8_t *x, *y, *z;
    uint8_t x_temp, y_temp, z_temp;
    x = &x_temp;
    y = &y_temp;
    z = &z_temp;
    int8_t x_comp, y_comp, z_comp;
    int32_t temp;
    int8_t activity_buffer[LIS_MAX_FIFO_SIZE];
    int8_t min, max, difference;
    //    current_activity_level = ACTIVITY_LEVEL_REST;
    for (int i = 0; i < samples_available; i++)
    {
        err_code = lis_read_register(LIS3DX_REG_OUT_X_H, &x_temp);
        APP_ERROR_CHECK(err_code);
        err_code = lis_read_register(LIS3DX_REG_OUT_Y_H, &y_temp);
        APP_ERROR_CHECK(err_code);
        err_code = lis_read_register(LIS3DX_REG_OUT_Z_H, &z_temp);
        APP_ERROR_CHECK(err_code);
        x_comp = *x - lis_calib_vals.x_calib;
        y_comp = *y - lis_calib_vals.y_calib;
        z_comp = *z - lis_calib_vals.z_calib;

        temp = (x_comp * x_comp) + (y_comp * y_comp) + (z_comp * z_comp);
        activity_buffer[i] = sqrt(temp);
    }

    min = activity_buffer[0];
    max = activity_buffer[0];

    for (int i = 0; i < samples_available; i++)
    {
        if (activity_buffer[i] > max)
            max = activity_buffer[i];
        else if (activity_buffer[i] < min)
            min = activity_buffer[i];
        else
            ;
    }
    difference = max - min;
    msg("Min %d, Max %d, Diff %d", min, max, difference);

    if (difference >= INTENSE_THRESHOLD)
        current_activity_level = ACTIVITY_LEVEL_INTENSE;
    else if (difference >= MODERATE_THRESHOLD)
        current_activity_level = ACTIVITY_LEVEL_MODERATE;
    else if (difference >= LOW_THRESHOLD)
        current_activity_level = ACTIVITY_LEVEL_LOW;
    else
        current_activity_level = ACTIVITY_LEVEL_REST;

    msg("Activity Level %s", activity_level_str[current_activity_level]);
}

void accel_setup_fifo()
{
    msg("Enable Accel FIFO");
    ret_code_t err_code;
    accel_disable_double_tap();
    err_code = lis_write_register(
        LIS3DX_REG_CTRL1, LIS3DX_DATA_RATE_25HZ | LIS3DX_LOW_POWER_ENABLE | LIS3DX_XYZ_ENABLE);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_CTRL3, LIS3DX_I1_WTM);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_FIFOCTRL, STREAM_MODE | FIFO_WATERMARK_25_SAMPLES);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_CTRL5, FIFO_ENABLE);
    APP_ERROR_CHECK(err_code);
}

void accel_setup_double_tap()
{
    msg("Setup Double Tap");
    ret_code_t err_code;
    accel_disable_fifo();
    err_code = lis_write_register(
        LIS3DX_REG_CTRL1, LIS3DX_DATARATE_400_HZ | LIS3DX_LOW_POWER_ENABLE | LIS3DX_XYZ_ENABLE);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_CTRL6, LIS3DX_I2_CLICK);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_REFERENCE, 0x01);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_INT1CFG, I1_CFG_6D_Z_AXIS);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_INT1THS, I1_THS_VAL);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_INT1DUR, I1_DURATION_VAL);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_CLICKCFG, CLICK_ENABLE_XYZ_AXIS);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_CLICKTHS, CLICK_THS);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_TIMELIMIT, CLICK_TIME_LIMIT_MAX);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_TIMELATENCY, CLICK_TIME_LATENCY);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_TIMEWINDOW, CLICK_TIME_WINDOW);
    APP_ERROR_CHECK(err_code);
}


void accel_disable_double_tap()
{
    msg("Disable Accel Double Tap");
    ret_code_t err_code;
    err_code = lis_write_register(LIS3DX_REG_CTRL6, 0x00);
    APP_ERROR_CHECK(err_code);
}

void accel_disable_fifo()
{
    msg("Disable Accel FIFO");
    ret_code_t err_code;
    err_code = lis_write_register(LIS3DX_REG_CTRL3, 0x00);
    APP_ERROR_CHECK(err_code);
    err_code = lis_write_register(LIS3DX_REG_CTRL5, 0x00);
    APP_ERROR_CHECK(err_code);
}

activity_level_t get_activity_level() { return current_activity_level; }

void accel_process_fifo_interrupt()
{
    ret_code_t err_code;
    uint8_t source;
    uint8_t available_samples = 0;
  //  NRF_LOG_INFO("FIFO interrupt active");
    err_code = lis_read_register(LIS3DX_REG_FIFOSRC, &source);
    available_samples = check_fifo_status(source);
    measure_activity_level(available_samples);
    activity_read = true;
}

void accel_power_down()
{
    msg("Powering down LIS");
    lis_power_off();
}
/**
 * @brief  Main program
 * @param  None
 * @retval None
 */