/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "battery.h"
#include "nrf.h"
#include "nrf_drv_saadc.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "bio2board.h"
#include "timer_functions.h"
#include "nrf_delay.h"
#include "led_functions.h"
#include "state_machine.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define BATTERY_CHANNEL NRF_SAADC_INPUT_AIN7
#define SAMPLES_IN_BUFFER 2
#define BATTERY_LUT_VALS 16

#define MAX_BAT_LEVEL_INDEX 0
#define MIN_BAT_LEVEL_INDEX 15
/* Private macro -------------------------------------------------------------*/
#define BATTERY_MORE_THAN_90    MINS_TO_SECS(5)
#define BATTERY_MORE_THAN_70    MINS_TO_SECS(3)
#define BATTERY_MORE_THAN_50    MINS_TO_SECS(2)
#define BATTERY_MORE_THAN_20    MINS_TO_SECS(1)
#define BATTERY_MORE_THAN_10    30
#define BATTERY_LESS_THAN_10    10

#define DEFAULT_BATTERY_BLINK_RATE    BATTERY_MORE_THAN_50
/* Private variables ---------------------------------------------------------*/
static volatile uint8_t battery_level;
static uint16_t battery_blink_rate = DEFAULT_BATTERY_BLINK_RATE;
static uint32_t battery_timer_count = 0;

volatile bool battery_level_updated = false;

static const uint8_t battery_lut_soc[BATTERY_LUT_VALS] =
{
  100, 90,80,70,65,60,50,40,30,25,20,15,10,6,3,0
};

//ADC values for 2M 1M Voltage divider, 1/3 Gain and 0.6V Vref
static const uint16_t battery_lut_mv[BATTERY_LUT_VALS] =
{
    3165,3089, 3025,2978,2955,2928, 2881,2863,2850,2840,2820,2800,2772,2742,2684,2501
};

/* Private function prototypes -----------------------------------------------*/
static void update_blink_rate(void);
/* Private functions ---------------------------------------------------------*/

static nrf_saadc_value_t m_buffer[SAMPLES_IN_BUFFER];

void update_battery_level(nrf_saadc_value_t *samples)
{
   uint8_t last_battery_level = battery_level;
   int32_t sample_sum = 0;
   for(uint8_t i = 0; i < SAMPLES_IN_BUFFER ; i++)
      sample_sum += *(samples + i);
   //974 5V  5000*0.758/974
   float float_bat_value = (sample_sum/SAMPLES_IN_BUFFER)*4;
   int16_t avg_bat_value = (int16_t)float_bat_value;
   float voltage_mv = (avg_bat_value / 0.758) ;

   NRF_LOG_INFO("Avg Value %d, Voltage %d mv", avg_bat_value, (int16_t)voltage_mv);

   if(avg_bat_value <= battery_lut_mv[MIN_BAT_LEVEL_INDEX])
   {
        battery_level = battery_lut_soc[MIN_BAT_LEVEL_INDEX];
        current_state.battery_level=battery_level;

   }
   else if (avg_bat_value >= battery_lut_mv[MAX_BAT_LEVEL_INDEX])
   {
        battery_level = battery_lut_soc[MAX_BAT_LEVEL_INDEX];

        current_state.battery_level=battery_level;

        if(battery_level != last_battery_level)
          {
       
            battery_level_updated = true;
            NRF_LOG_INFO("Battery full battery level updated");
        }
   }
   else
   {
      for(uint8_t i = 0; i < BATTERY_LUT_VALS - 1; i++)
      {
          if(avg_bat_value <= battery_lut_mv[i] && avg_bat_value >= battery_lut_mv[i + 1])
          {
              //NRF_LOG_INFO("Found in Range %d tp %d", battery_lut_mv[i],battery_lut_mv[i + 1]);
              float a = battery_lut_mv[i] - battery_lut_mv[i + 1];
              float b = battery_lut_soc[i] - battery_lut_soc[i+1];

              //NRF_LOG_INFO("A is %d b is %d",(int)a,(int)b);

              float slope = a/b;
              //NRF_LOG_INFO("Slope %d",(int)(slope * 100));

              a = slope * battery_lut_soc[i];
              b =  battery_lut_mv[i] - avg_bat_value;
              float bat = (a - b)/slope;
              //NRF_LOG_INFO("A %d , B %d", (int)(a * 10),(int)(b * 10));
              //NRF_LOG_INFO("Bat %d", (int)(bat * 100));
              battery_level = (uint8_t)bat;
              if(battery_level != last_battery_level)
              {
                  battery_level_updated = true;
                  update_blink_rate();
                  NRF_LOG_INFO("Blink Rate %d",battery_blink_rate);
              }
              break;
          }
      }
   }
}

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
    uint16_t adc_val1, adc_val2;
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);
        
        nrf_saadc_value_t *samples =  p_event->data.done.p_buffer;

        update_battery_level(samples);
    }
}

void saadc_init(void)
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config 
        = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(BATTERY_CHANNEL);
    channel_config.acq_time = NRF_SAADC_ACQTIME_40US;
    channel_config.gain = NRF_SAADC_GAIN1_3;

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer, SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
}


void battery_init()
{
    saadc_init();
    battery_level = 0;
}

void battery_task_trigger()
{
    APP_ERROR_CHECK(nrf_drv_saadc_sample());
}

uint8_t get_battery_level()
{
   return battery_level;
}

static void update_blink_rate(void)
{
    if(battery_level > 90)
        battery_blink_rate = BATTERY_MORE_THAN_90;
    else if (battery_level > 70)
        battery_blink_rate = BATTERY_MORE_THAN_70;
    else if (battery_level > 50)
        battery_blink_rate = BATTERY_MORE_THAN_50;
    else if (battery_level > 20)
        battery_blink_rate = BATTERY_MORE_THAN_20;
    else if (battery_level > 10)
        battery_blink_rate = BATTERY_MORE_THAN_10;
    else
        battery_blink_rate = BATTERY_LESS_THAN_10; 
}

void handle_battery_every_second(void)
{
    battery_timer_count++;
    if (battery_timer_count % BATTERY_SAMPLE_TIME_S == 0) {
      NRF_LOG_INFO("Battery timer expired")
      battery_timer_expired = true;
    }
    if(battery_timer_count >= battery_blink_rate )
    {
       battery_timer_count = 0;
       battery_led_handler();
    }
}
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
