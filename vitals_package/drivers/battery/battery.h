/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _BATTERY_H
#define _BATTERY_H

#include <stdint.h>
#include <stdbool.h>
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define BATTERY_SAMPLE_TIME_S 5
/* Exported functions ------------------------------------------------------- */
extern volatile bool battery_level_updated;
extern volatile bool battery_timer_expired;

void battery_init();
void battery_task_trigger();
uint8_t get_battery_level();
void handle_battery_every_second(void);
#endif /* _BATTERY_H */