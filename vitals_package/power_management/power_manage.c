/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "power_manage.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define RESETPIN_MASK 0x00000001
#define DOG_MASK      0x00000002
#define SREQ_MASK     0x00000004
#define LOCKUP_MASK   0x00000008
#define OFF_MASK      0x00010000
#define LPCOMP_MASK   0x00020000
#define DIF_MASK      0x00040000
#define NFC_MASK      0x00080000

#define RESET_REASON_CLR_MASK 0x000F000F

#define POWER_ON_RESET 0x00
/* Private macro -------------------------------------------------------------*/
NRF_PWR_MGMT_HANDLER_REGISTER(app_shutdown_handler, 0);
/* Private variables ---------------------------------------------------------*/
char const * reset_reason_str[] =
{
  "UNKNOWN",
  "RESET PIN",
  "SOFT RESET",
  "DOG",
  "LOCKUP",
  "SYSTEM OFF",
  "LPCOMP",
  "DIF",
  "NFC",
  "PWR ON",
};
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/** @brief Function for placing the application in low power state while waiting for events. */
void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

bool app_shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_DFU:
            NRF_LOG_INFO("Power management wants to reset to DFU mode\r\n"); 
            // Change this code to tailor to your reset strategy.
            // Returning false here means that the device is not ready to jump to DFU mode yet.
            // 
            // Here is an example using a variable to delay resetting the device:
            //
            /* if (!m_ready_for_reset)
               {
                   return false;
               }
            */
            break;
 
        default:
            // Implement any of the other events available from the power management module:
            // -NRF_PWR_MGMT_EVT_PREPARE_SYSOFF
            // -NRF_PWR_MGMT_EVT_PREPARE_WAKEUP
            // -NRF_PWR_MGMT_EVT_PREPARE_RESET
            return true;
    }
    NRF_LOG_INFO("Power management allowed to reset to DFU mode\r\n");
    return true;
}


/**
  * @brief  Main program
  * @param  None
  * @retval None
  */

reset_cause_t get_reset_cause()
{
  reset_cause_t reason = RESET_CAUSE_UNKNOWN;
  uint32_t res;
  sd_power_reset_reason_get(&res);
  if(res == POWER_ON_RESET)
    reason = RESET_CAUSE_POR;
  else if(res & RESETPIN_MASK)
    reason = RESET_CAUSE_RESET_PIN;
  else if(res & DOG_MASK)
    reason = RESET_CAUSE_DOG;
  else if(res & SREQ_MASK)
    reason = RESET_CAUSE_SOFT_RESET;
  else if(res & LOCKUP_MASK)
    reason = RESET_CAUSE_LOCKUP;
  else if(res & OFF_MASK)
    reason = RESET_CAUSE_OFF;
  else if(res & LPCOMP_MASK)
    reason = RESET_CAUSE_LPCOMP;
  else if(res & DIF_MASK)
    reason = RESET_CAUSE_DIF;
  else if(res & NFC_MASK)
    reason = RESET_CAUSE_NFC;
  else;
  
  NRF_LOG_INFO("Reset Reason %s",reset_reason_str[reason]);
  sd_power_reset_reason_clr(RESET_REASON_CLR_MASK);
  return reason;
}