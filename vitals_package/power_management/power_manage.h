/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef POWER_MANAGE_H_
#define POWER_MANAGE_H_

#include "nrf_pwr_mgmt.h"

/* Exported types ------------------------------------------------------------*/
typedef enum
{
  RESET_CAUSE_UNKNOWN,
  RESET_CAUSE_RESET_PIN,
  RESET_CAUSE_SOFT_RESET,
  RESET_CAUSE_DOG,
  RESET_CAUSE_LOCKUP,
  RESET_CAUSE_OFF,
  RESET_CAUSE_LPCOMP,
  RESET_CAUSE_DIF,
  RESET_CAUSE_NFC,
  RESET_CAUSE_POR,
}reset_cause_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void power_manage(void);
bool app_shutdown_handler(nrf_pwr_mgmt_evt_t event);
reset_cause_t get_reset_cause();
#endif /* POWER_MANAGE_H_ */