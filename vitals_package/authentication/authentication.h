/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _AUTH_H_
#define _AUTH_H_

#include "app_error.h"
#include "nrf_drv_rng.h"
#include "nrf_soc.h"
/* Exported types ------------------------------------------------------------*/
typedef enum
{
    APP_KEY,
    CLOUD_ACT_KEY,
    CLOUD_DEACT_KEY,
    USB_AUTH_KEY,
} key_type_t;

/* Exported constants --------------------------------------------------------*/
#define RANDOM_BUFF_LEN 2
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern uint8_t random_buffer[RANDOM_BUFF_LEN];
void get_random_number();
void aes_encrypt(key_type_t key_type, uint8_t * clear_text, uint8_t * cipher_text);
uint16_t crc16(const unsigned char * buffer, uint32_t length);
#endif /* _AUTH_H_ */