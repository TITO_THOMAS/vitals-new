/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "authentication.h"
#include "debug_config.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include <stdlib.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t random_buffer[RANDOM_BUFF_LEN];
static const uint8_t app_key[SOC_ECB_KEY_LENGTH] = {0xf8, 0x1b, 0xe1, 0x34, 0xea, 0x83, 0x89, 0x87,
    0xe0, 0xff, 0x77, 0xca, 0x05, 0xfb, 0xfa, 0x6f}; // dummy key
static const uint8_t cloud_act_key[SOC_ECB_KEY_LENGTH] = {
    0xdd, 0xd1, 0x39, 0xae, 0xda, 0x81, 0x32, 0x51, 0x07, 0x56, 0xc0, 0x2c, 0x8c, 0xbf, 0xbe, 0xed};
static const uint8_t cloud_deact_key[SOC_ECB_KEY_LENGTH] = {
    0xdd, 0xd1, 0x39, 0xae, 0xda, 0x81, 0x32, 0x51, 0x07, 0x56, 0xc0, 0x2c, 0x8c, 0xbf, 0xbe, 0xed};
static const uint8_t usb_key[SOC_ECB_KEY_LENGTH] = {
    0xed, 0xd1, 0x50, 0xae, 0xaa, 0x81, 0x02, 0x51, 0x07, 0x56, 0x20, 0x7c, 0x8c, 0xbf, 0x8e, 0xed};

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
void get_random_number()
{
    uint8_t available = 0;
//    ret_code_t err_code = nrf_drv_rng_init(NULL);
//    APP_ERROR_CHECK(err_code);
//    nrf_drv_rng_bytes_available(&available);
    NRF_LOG_DEBUG("Random bytes available %d", available);
//    err_code = nrf_drv_rng_rand(random_buffer, RANDOM_BUFF_LEN);
//    APP_ERROR_CHECK(err_code);
    NRF_LOG_HEXDUMP_INFO(random_buffer, RANDOM_BUFF_LEN);
 //   nrf_drv_rng_uninit();
}

void aes_encrypt(key_type_t key_type, uint8_t * clear_text, uint8_t * cipher_text)
{
    NRF_LOG_INFO("Aes Encrypt");
    nrf_ecb_hal_data_t m_ecb_data;

    const uint8_t * key;
    switch (key_type)
    {
        case APP_KEY:
            key = app_key;
            break;
        case CLOUD_ACT_KEY:
            key = cloud_act_key;
            break;
        case CLOUD_DEACT_KEY:
            key = cloud_deact_key;
            break;
        case USB_AUTH_KEY:
            key = usb_key;
            break;
        default:
            break; // TODO add return for invalid key type
    }


    memcpy(&m_ecb_data.cleartext[0], clear_text, SOC_ECB_CLEARTEXT_LENGTH);
    memcpy(&m_ecb_data.key[0], key, SOC_ECB_KEY_LENGTH);
    ret_code_t err_code = sd_ecb_block_encrypt(&m_ecb_data);
#ifdef AUTH_DEBUG
    NRF_LOG_DEBUG("In AES Clear Text");
    NRF_LOG_HEXDUMP_DEBUG(m_ecb_data.cleartext, 16);
    NRF_LOG_DEBUG("In AES Cipher Text");
    NRF_LOG_HEXDUMP_DEBUG(m_ecb_data.ciphertext, 16);

    NRF_LOG_DEBUG("Key");
    NRF_LOG_HEXDUMP_DEBUG(key, 16);
#endif
    APP_ERROR_CHECK(err_code);
    memcpy(cipher_text, &m_ecb_data.ciphertext[0], SOC_ECB_CIPHERTEXT_LENGTH);
}

uint16_t crc16(const unsigned char * buffer, uint32_t length)
{
    uint16_t crc = 0xFFFF;

    if (buffer && length)
        while (length--)
        {
            crc = (crc >> 8) | (crc << 8);
            crc ^= *buffer++;
            crc ^= ((unsigned char)crc) >> 4;
            crc ^= crc << 12;
            crc ^= (crc & 0xFF) << 5;
        }

    return crc;
}

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */