/********************************************************************
 *	File:
 *	Author:
 *	Created on :
 *	Last Modified :
 ********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _DFU_FUNCTIONS_H
#define _DFU_FUNCTIONS_H

#include "ble_dfu.h"
#include "nrf_bootloader_info.h"
#include "nrf_dfu_ble_svci_bond_sharing.h"
#include "nrf_svci_async_function.h"
#include "nrf_svci_async_handler.h"


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void ble_dfu_buttonless_evt_handler(ble_dfu_buttonless_evt_type_t event);

#endif /* _DFU_FUNCTIONS_H */