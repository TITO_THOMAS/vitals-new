/********************************************************************
*	File:
*	Author:
*	Created on :
*	Last Modified :
********************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef FDS_FUNCTIONS_H_
#define FDS_FUNCTIONS_H_
/* Includes ------------------------------------------------------------------*/
#include "fds.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/


/* Exported variables ------------------------------------------------------- */
extern char const * fds_err_str[];
extern bool volatile m_fds_initialized;
/* Exported functions ------------------------------------------------------- */
extern void power_manage(void); //TODO move to power optimize
void initialize_fds(void);
void get_fds_stat(void);
#endif /* FDS_FUNCTIONS_H_ */